<?php
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
$email = ($this->session->userdata['logged_in']['email']);
$tipouser = ($this->session->userdata['logged_in']['tipo_usuario']);
} else {
redirect(base_url());
}
?>
  
<?php if ($tipouser == 'Administrador' || $tipouser == 'OPERADOR'){
	
 } else {
    redirect(base_url());
 }?>

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="min-height: 1156px;">
        <br/><br/><br/>
        <!-- Content Header (Page header) -->
        <section class="content-header">
	    <h1>
		Reporte de Tendencias
	    </h1>
	    <ol class="breadcrumb">
	      <li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
	      <li>Reportes</li>
	      <li class="active">Reporte de Tendencias</li>
	    </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
				<form class="form-horizontal" id="form_gra_tendencia">
								<fieldset>
					<legend>Generar Reporte de resultados por tendencias</legend>
					<div class="col-md-2"></div>
					<div class="col-md-4">
						<div class="form-group">
							<label style="font-weight:bold">Eleción</label>
							<select id="eleccion_id" name="eleccion_id" class="form-control select2" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();">
								<option selected="" value="0">Seleccione</option>
								<?php foreach ($listar_elecciones as $elecciones) { ?>
									<option value="<?php echo $elecciones->id ?>"><?php echo $elecciones->nombre ?></option>
								<?php } ?>
							</select>
						</div>
					</div><!-- /.col --><br>
				<div class="col-md-3">
					<button role="button" id="grafico_tendencia" style="font-weight: bold;width: 100%"  class="btn btn-info">
						<span class="glyphicon glyphicon-stats"></span>&nbsp;Génerar Gráficas de Tendencias
					</button>
				</div>
					
				<div id="container2" style="display: inline; width: 90%"></div>
					
					
				</fieldset>
				</form>
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
   <!--       <img  src="<?= base_url() ?>/static/img/footer.png"/> -->
      </footer>
</div><!-- /wrapper -->
<script>
$(document).ready(function(){
      
    $("select").select2();
	$('#grafico_tendencia').click(function(e){
		e.preventDefault();
		var id_eleccion = $('#eleccion_id').find('option').filter(':selected').val();
		var titulo = ($('#eleccion_id').find('option').filter(':selected').text());
		$('#container2').text('');
		// alert(id_eleccion+'-'+titulo);
		var id = $(this).attr('id');
		var bandera = 'candidato';
		if (id_eleccion == 0) {
			bootbox.alert('Disculpe, Debe Seleccionar la Encuesta', function() {	    
				$('#encuestas').parent('div').addClass('has-error'); // colocar class has-error de boostrap
				$('#encuestas').select2('open');
			});
		} else {
			// alert($('#form_gra_tendencia').serialize());
			$.post('<?php echo base_url(); ?>index.php/reportes/CTendencias/cargar_grafica_tendencias', $('#form_gra_tendencia').serialize(), function (response) {
				 if (response == 'vacio') {
				 	bootbox.alert('Disculpe, aún no hay registros para esta Encuesta', function() {	    
				 	$('#container2').html('');
				 	});
				 }
				 else{
				 	$('#container2').highcharts({
				 		chart: {
				 			type: 'spline'
				 		},
				 		title: {
				 			text: 'GRÁFICA DE TENDENCIA'
				 		},
				 		subtitle: {
				 			text: titulo
				 		},
				 		xAxis: {		
				 			categories: ['07:00 AM','08:00 AM','09:00 AM','10:00 AM','11:00 AM','12:00 AM','01:00 PM', '02:00 PM', '03:00 PM', '04:00 PM', '05:00 PM', '06:00 PM', '07:00 PM', '08:00 PM'],
				 			title: {
				 				text: 'Hora'
				 			}
				 		},
				 		yAxis: {
				 			title: {
				 				text: 'Porcentaje'
				 			},
				 			min: 0
				 		},
				 		tooltip: {
				 			headerFormat: '<b>{series.name}</b><br>',
				 		},
				
				 		plotOptions: {
				 			spline: {
				 				marker: {
				 					enabled: true
				 				}
				 			}
				 		},
				 		//series:datos
				 		series: response
				 	});
				 }
			});   
		};
	});

});
</script>
