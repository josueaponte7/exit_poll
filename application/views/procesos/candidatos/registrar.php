<?php
if (isset($this->session->userdata['logged_in'])) {
    $username = ($this->session->userdata['logged_in']['username']);
    $email = ($this->session->userdata['logged_in']['email']);
    $tipouser = ($this->session->userdata['logged_in']['tipo_usuario']);
    $id_user = ($this->session->userdata['logged_in']['id']);
} else {
    redirect(base_url());
}
?>

<div class="wrapper">

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 1156px;">
        <br/>
        <br/>
        <br/>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="color:#3C8DBC">
                Candidatos
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>index.php"  style="color:#3C8DBC"><i class="fa fa-home"></i> Inicio</a></li>
                <li style="color:#3C8DBC">Exitpoll</li>
                <li style="color:#3C8DBC">Candidatos</li>
                <li class="active">Registrar Candidato</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xs-12">

                    <!-- SELECT2 EXAMPLE -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title" style="color:#3C8DBC">Registro de Candidato</h3>
                            <div class="box-tools pull-right">

                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <form id="form_candidatos" method="post" accept-charset="utf-8" class="putImages" enctype='multipart/form-data'>
                                <div class="col-lg-2">

                                    <div class="form-group">
                                        <label class="control-label">Imagen</label>
                                        <input id="foto" name="foto" type="file" multiple class="file-loading" >
                                        <div id="errorBlock"></div>
                                    </div> <!--/.form-group -->

                                </div>
                                <div class="col-lg-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="font-weight:bold">Nombre</label>
                                            <input type="text" placeholder="Introduzca su Nombre" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" id="nombre" maxlength="70" name="nombre" class="form-control">

                                        </div> <!--/.form-group -->
                                    </div><!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="font-weight:bold">Apellido</label>
                                            <input type="text" placeholder="Introduzca su Apellido" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" id="apellido" maxlength="70"  name="apellido" class="form-control">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.col -->
                                
								<div class="form-group">
                                    <div class="col-md-12" style="text-align: center">
										<br><br>
                                        <input class="form-control"  type='hidden' placeholder="user" id="user_create_id" name="user_create_id" value="<?php echo"$id_user" ?>"/>
                                        <input class="form-control"  type='hidden' id="id" name="id" value="<?php echo $ultimo_id + 1 ?>"/>
                                        <a class="btn btn-app " data-toggle="tab" id="volver">
                                            <i class="glyphicon glyphicon-chevron-left text-orange"></i>Volver
                                        </a>
                                        <a class="btn btn-app " type="reset" id="limpiar" data-toggle="tab" >
                                            <i class="glyphicon glyphicon-retweet text-blue"></i>Limpiar
                                        </a>
                                        <a class="btn btn-app " type="submit" id="registrar" data-toggle="tab" >
                                            <i class="glyphicon glyphicon-floppy-disk text-green"></i>Registrar
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div><!-- /.box-body -->
                    </div><!-- /.box-body-primary -->

                </div><!-- /.col -->

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
    </footer>


    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->


<script>
    //~ $('#codigo').numeric();
    $('#nombre').alphanumeric({allow: " "}); //Solo permite texto
    $('#apellido').alphanumeric({allow: " "}); //Solo permite texto
    
    $('input').on({
        keypress: function () {
            $(this).parent('div').removeClass('has-error');
        }
    });

    $('#volver').click(function () {
        url = '<?php echo base_url() ?>index.php/procesos/CCandidatos/'
        window.location = url
    })
    
    $("#foto").fileinput({
        initialPreview: [
            '<img src="<?= base_url() ?>static/img/usuario.jpg" class="file-preview-image" alt="Usuario" title="Usuario">',
        ],
        browseClass: "btn btn-primary btn-block",
        browseLabel: "Buscar Imagen",
        showCaption: false,
        showRemove: false,
        maxFileSize: 300,
        showUpload: false,
        allowedFileExtensions: ["jpg", "png","jpeg"],
        elErrorContainer: "#errorBlock",
        msgSizeTooLarge: 'Archivo muy pesado "{name}". (<b>{size} KB</b>) excede el tamaño máximo que es de <b>{maxSize} KB',
        msgInvalidFileExtension: 'Extensión invalida "{name}". Solo admite archivos"{extensions}".'

    });
    
    // Validamos que el tipo de archivo sea .jpg, png o jpeg
	//~ $("#foto").change(function (e) {
		//~ e.preventDefault();  // Para evitar que se envíe por defecto
		//~ var foto = $(this).val();
		//~ 
		//~ var ext = foto.split(".");
		//~ ext = ext[1];
		//~ 
		//~ if (ext != 'jpg' && ext != 'png' && ext != 'jpeg'){
			//~ bootbox.alert("Formato de archivo no soportado", function () {
			//~ }).on('hidden.bs.modal', function (event) {
				//~ $("#foto").val('');
			//~ });
		//~ }
	//~ });

    $("#registrar").click(function (e) {
        e.preventDefault();  // Para evitar que se envíe por defecto
        if (($('#nombre').val().trim() == '')) {
            bootbox.alert('Disculpe, Debe colocar el nombre del candidato', function () {
                $('#nombre').parent('div').addClass('has-error');
            });
        } else if (($('#apellido').val().trim() == '')) {
            bootbox.alert('Disculpe, Debe colocar el apellido del candidato', function () {
                $('#apellido').parent('div').addClass('has-error');
            });
        } else {
            //~ $('#codigo').prop('disabled',false);
            var formData = new FormData(document.getElementById("form_candidatos"));
            
            $.ajax({
				url: '<?php echo base_url(); ?>index.php/procesos/CCandidatos/guardar/',
				type: "post",
				dataType: "html",
				data: formData,
				cache: false,
				contentType: false,
				processData: false
			}).done(function(res){
				var respuesta = res;
				//~ alert(respuesta);
				
				bootbox.alert(respuesta, function () {
				}).on('hidden.bs.modal', function (event) {
					url = '<?php echo base_url() ?>index.php/procesos/CCandidatos/';
					window.location = url;
				});
				
			});
        }
    });



</script>
