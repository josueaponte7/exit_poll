<?php
if (isset($this->session->userdata['logged_in'])) {
    $username = ($this->session->userdata['logged_in']['username']);
    $email = ($this->session->userdata['logged_in']['email']);
    $tipouser = ($this->session->userdata['logged_in']['tipo_usuario']);
    $id_user = ($this->session->userdata['logged_in']['id']);
} else {
    redirect(base_url());
}
?>

<div class="wrapper">

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 1156px;">
        <br/>
        <br/>
        <br/>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="color:#3C8DBC">
                Elecciones
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>index.php"  style="color:#3C8DBC"><i class="fa fa-home"></i> Inicio</a></li>
                <li style="color:#3C8DBC">Exitpoll</li>
                <li style="color:#3C8DBC">Elecciones</li>
                <li class="active">Registrar Elección</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xs-12">

                    <!-- SELECT2 EXAMPLE -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title" style="color:#3C8DBC">Registro de Elección</h3>
                            <div class="box-tools pull-right">

                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <form id="form_eleccion" method="post" accept-charset="utf-8">
                                <div class="col-lg-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="font-weight:bold">Nombre</label>
                                            <input type="text" placeholder="Introduzca su Nombre" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" id="nombre" maxlength="70" name="nombre" class="form-control">

                                        </div> <!--/.form-group -->
                                    </div><!-- /.col -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="font-weight:bold">Estado</label>
                                            <select id="estado_id" name="estado_id" class="form-control">
												<option value="0">Seleccione</option>
												<?php echo $listar_estados ?>
												<?php foreach ($listar_estados as $estados) { ?>
													<option value="<?php echo $estados->codigo ?>"><?php echo $estados->descripcion ?></option>
												<?php } ?>
                                            </select>
                                        </div>
                                    </div><!-- /.col -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="font-weight:bold">Municipio</label>
                                            <select id="municipio_id" name="municipio_id" class="form-control">
                                                <option value="0">Seleccione</option>
												<?php foreach ($listar_municipios as $municipios) { ?>
													<option value="<?php echo $municipios->codigo ?>"><?php echo $municipios->descripcion ?></option>
												<?php } ?>
                                            </select>
                                        </div>
                                    </div><!-- /.col -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="font-weight:bold">Candidatos</label>
                                            <select id="candidatos_ids" class="form-control" multiple="multiple">
                                                <?php foreach ($list_candidatos as $candidato) { ?>
                                                    <option value="<?php echo $candidato->id ?>"><?php echo $candidato->nombre." ".$candidato->apellido ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.col -->
                                
								<div class="form-group">
                                    <div class="col-md-12" style="text-align: center">
										<br><br>
                                        <input class="form-control"  type='hidden' placeholder="user" id="user_create_id" name="user_create_id" value="<?php echo"$id_user" ?>"/>
                                        <input class="form-control"  type='hidden' id="id" name="id" value="<?php echo $ultimo_id + 1 ?>"/>
                                        <a class="btn btn-app " data-toggle="tab" id="volver">
                                            <i class="glyphicon glyphicon-chevron-left text-orange"></i>Volver
                                        </a>
                                        <a class="btn btn-app " type="reset" id="limpiar" data-toggle="tab" >
                                            <i class="glyphicon glyphicon-retweet text-blue"></i>Limpiar
                                        </a>
                                        <a class="btn btn-app " type="submit" id="registrar" data-toggle="tab" >
                                            <i class="glyphicon glyphicon-floppy-disk text-green"></i>Registrar
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div><!-- /.box-body -->
                    </div><!-- /.box-body-primary -->

                </div><!-- /.col -->

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
    </footer>


    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->


<script>
    //~ $('#codigo').numeric();
    $('#nombre').alphanumeric({allow: " "}); //Solo permite texto
    $('#apellido').alphanumeric({allow: " "}); //Solo permite texto
    
	$('#estado_id').change(function () {
        var estado_id = $('#estado_id').val();
        $('#municipio_id').find('option:gt(0)').remove().end().select2('val', '0');
 
        $.get('<?php echo base_url(); ?>index.php/busquedas_ajax/ControllersBusqueda/search_topo_municipios/' + estado_id + '', function (data) {
            var option = "";
            $.each(data, function (i) {
                option += "<option value=" + data[i]['codigo'] + ">" +data[i]['descripcion'] + "</option>";
            });
            $('#municipio_id').append(option);
        }, 'json');
    });
	
    $('select').on({
		change: function () {
			$(this).parent('div').removeClass('has-error');
		}
	});
    $('input').on({
        keypress: function () {
            $(this).parent('div').removeClass('has-error');
        }
    });

    $('#volver').click(function () {
        url = '<?php echo base_url() ?>index.php/procesos/CEleccion/'
        window.location = url
    })

    $("#registrar").click(function (e) {
        e.preventDefault();  // Para evitar que se envíe por defecto
        if (($('#nombre').val().trim() == '')) {
            bootbox.alert('Disculpe, debe colocar el nombre de la elección', function () {
                $('#nombre').parent('div').addClass('has-error');
                $('#nombre').focus();
            });
        } else if (($('#estado_id').val().trim() == '0')) {
            bootbox.alert('Disculpe, debe seleccionar el estado de la elección', function () {
                $('#estado_id').parent('div').addClass('has-error');
                $('#estado_id').focus();
            });
        } else if (($('#municipio_id').val().trim() == '0')) {
            bootbox.alert('Disculpe, debe seleccionar el municipio de la elección', function () {
                $('#municipio_id').parent('div').addClass('has-error');
                $('#municipio_id').focus();
            });
        } else {
            //~ alert($('#candidatos_ids').val());
            
            $.post('<?php echo base_url(); ?>index.php/procesos/CEleccion/guardar', $('#form_eleccion').serialize()+'&'+$.param({'candidatos_ids':$('#candidatos_ids').val()}), function (response) {
				//~ alert(response);
				if (response == 'existe') {
                    bootbox.alert('Disculpe, este nombre de elección se encuentra registrado', function () {
						$('#nombre').parent('div').addClass('has-error');
					});
                }else{
					bootbox.alert("Guardado con exito", function () {
					}).on('hidden.bs.modal', function (event) {
						url = '<?php echo base_url() ?>index.php/procesos/CEleccion/';
						window.location = url;
					});
				}
            });
        }
    });



</script>
