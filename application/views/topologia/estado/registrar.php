<?php
if (isset($this->session->userdata['logged_in'])) {
    $username = ($this->session->userdata['logged_in']['username']);
    $email = ($this->session->userdata['logged_in']['email']);
    $tipouser = ($this->session->userdata['logged_in']['tipo_usuario']);
    $id_user = ($this->session->userdata['logged_in']['id']);
} else {
    redirect(base_url());
}
?>

<?php
if ($tipouser == 'Administrador') {
    
} else {
    redirect(base_url());
}
?>  

<div class="wrapper">



    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 1156px;">
        <br/>
        <br/>
        <br/>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Estados
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
                <li>Configuraciones</li>
                <li>Topologia</li>
                <li class="active">Registrar Estado</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xs-12">

                    <!-- SELECT2 EXAMPLE -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Registrar Estado</h3>
                            <div class="box-tools pull-right">

                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <form id="form_estados">


                                <!--<div class="col-md-4">-->
                                <!--    <div class="form-group">-->
                                <!--        <label style="font-weight:bold">Abreviatura</label>-->
                                <!--        <input class="form-control" autofocus="" type='text' placeholder="Ej: VE" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" id="abreviatura" maxlength="3" name="abreviatura"/>-->
                                <!--    </div><!-- /.form-group -->
                                <!---->
                                <!---->
                                <!--</div><!-- /.form-group -->

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label  style="font-weight:bold">Estado</label>
                                        <input class="form-control"  type='text'  style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" placeholder="Nombre del Estado" maxlength="20" id="descripcion" name="descripcion"/>
                                        <!-- /.form-group -->

                                    </div>



                                </div><!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-weight:bold">País</label>
                                        <select id="pais_id" name="pais_id" class="form-control select2" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();">
                                            <option selected="" value="0">Seleccione</option>
                                            <?php foreach ($listar_paises as $pais) { ?>
                                                <option value="<?php echo $pais->codigo ?>"><?php echo $pais->descripcion ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>



                                </div><!-- /.col -->
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input class="form-control"  type='hidden' id="activo" name="activo" value="true"/>
                                        <input class="form-control"  type='hidden' id="id" name="id" value="<?php echo $ultimo_id +1 ?>"/>
                                        <input class="form-control"  type='hidden' id="codigo" name="codigo" value="<?php echo $ultimo_id +1 ?>"/>
                                        <button type="button" id="volver" style="font-weight: bold;font-size: 13px" class="btn btn-danger " >
                                            &nbsp;<span class="glyphicon glyphicon-chevron-left"></span>&nbsp;Volver
                                        </button>
                                        <button type="reset" id="limpiar" style="font-weight: bold;font-size: 13px; color: white " class="btn btn-info"/>
                                        &nbsp;<span class="glyphicon glyphicon-retweet"></span>&nbsp;&nbsp;Limpiar
                                        </button>
                                        <button type="submit" id="registrar" style="font-weight: bold;font-size: 13px" class="btn btn-success"/>
                                        &nbsp;<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Registrar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div><!-- /.box-body -->
                    </div><!-- /.box-body-primary -->

                </div><!-- /.col -->

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
    </footer>


    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->


<script>

    //Initialize Select2 Elements
    $(".select2").select2();
    $("#cod_estado").numeric();
    $('select').on({
        change: function () {
            $(this).parent('div').removeClass('has-error');
        }
    });
    $('input').on({
        keypress: function () {
            $(this).parent('div').removeClass('has-error');
        }
    });

    $('#volver').click(function () {
        url = '<?php echo base_url() ?>index.php/topologia/CEstados/'
        window.location = url
    });

    


    $("#registrar").click(function (e) {
        e.preventDefault();  // Para evitar que se envíe por defecto

        //if (($('#abreviatura').val().trim() == '')) {
        //    bootbox.alert('Disculpe, Debe Colocar la abreviatura del Estado', function () {
        //        $('#abreviatura').parent('div').addClass('has-error');
        //    });
        //} else
        if (($('#descripcion').val().trim() == '')) {
            bootbox.alert('Disculpe, Debe Colocar el Nombre del Estado', function () {
                $('#descripcion').parent('div').addClass('has-error');
            });
        } else if ($('#descripcion').val().length < 4) {
            bootbox.alert('Disculpe, Nombre de estado invalido (Muy Corto)', function () {
                $('#descripcion').parent('div').addClass('has-error');
            });
        } else if ($('#pais_id').val() == 0) {
            bootbox.alert('Disculpe, Debe seleccionar el pais', function () {
                $('#pais_id').parent('div').addClass('has-error');
            });
        }else {

            $.post('<?php echo base_url(); ?>index.php/topologia/CEstados/guardar', $('#form_estados').serialize(), function (response) {

                if (response[0] == '1') {
                    bootbox.alert("Disculpe, El Estado ya se encuentra registrado", function () {
                    }).on('hidden.bs.modal', function (event) {
                        $("#descripcion").parent('div').addClass('has-error');
                        $('#descripcion').val('');
                        $("#descripcion").focus();

                    });

                //} else if (response[0] == '2') {
                //    bootbox.alert("Disculpe, La abreviatura ingresada ya se encuentra registrado", function () {
                //    }).on('hidden.bs.modal', function (event) {
                //        $("#abreviatura").parent('div').addClass('has-error')
                //        $('#abreviatura').val('')
                //        $("#abreviatura").focus();
                //
                //    });

                } else {
                    bootbox.alert("Se registro con exito", function () {
                    }).on('hidden.bs.modal', function (event) {
                        url = '<?php echo base_url(); ?>index.php/topologia/CEstados'
                        window.location = url;
                    });
                }

            });
        }
    });





</script>

