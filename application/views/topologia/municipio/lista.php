<?php
if (isset($this->session->userdata['logged_in'])) {
    $username = ($this->session->userdata['logged_in']['username']);
    $email = ($this->session->userdata['logged_in']['email']);
    $tipouser = ($this->session->userdata['logged_in']['tipo_usuario']);
} else {
    redirect(base_url());
}
?>

<?php
if ($tipouser == 'Administrador') {
    
} else {
    redirect(base_url());
}
?>   
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 1156px;">
        <br/>
        <br/>
        <br/>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Municipios
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
                <li>Configuraciones</li>
                <li>Topologia</li>
                <li class="active">Municipios</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">


                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Listado de Municipios</h3>
                        </div><!-- /.box-header -->
                        <button role="button" class="btn btn-primary" style="font-weight: bold;font-size: 13px; color: white " id="enviar"  >

                            &nbsp;<span class="glyphicon glyphicon-plus"></span>&nbsp;Nuevo Municipio
                        </button>
                        <br/>
                        <div class="box-body">
                            <table id="tab_municipio" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style='text-align: center'>Item</th>
                                        <th style='text-align: center'>País</th>
                                        <th style='text-align: center'>Estado</th>
                                        <th style='text-align: center'>Municipio</th>
                                        <th style='text-align: center'>Activar/Desactivar</th>
                                        <th style='text-align: center'>Editar</th>
                                        <th style='text-align: center'>Borrar</th>
                                    </tr>
                                </thead>
                                <tbody >    
                                    <?php $i = 1; ?>

                                    <?php foreach ($listar as $municipio) { ?>
                                        <tr style="font-size: 16px;text-align: center" class="{% cycle 'impar' 'par' %}" >
                                            <td>
                                                <?php echo $i; ?>
                                            </td>
                                            <td>
                                                <?php foreach ($listar_paises as $paises) {
                                                    if ($municipio->pais_id == $paises->codigo)
                                                    {
                                                        echo $paises->descripcion;
                                                    }
                                                }?>
                                            </td>
                                        <td>
                                            <?php foreach ($listar_estados as $estado) { ?>
                                                <?php if ($estado->codigo == $municipio->estado_id): ?>
                                                    <option value="<?php echo $estado->codigo ?>"><?php echo $estado->descripcion ?></option>
                                                <?php endif; ?>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php echo $municipio->descripcion; ?>
                                        </td>
                                        <td style='text-align: center'>
                                            <?php if ($municipio->activo == 't') { ?>
                                                <input class='activar_desactivar' id='<?php echo $municipio->id; ?>' type="checkbox" title='Desactivar el usuario <?php echo $municipio->id; ?>' checked="checked"/>
                                            <?php } else if ($municipio->activo == 'f') { ?>
                                                <input class='activar_desactivar' id='<?php echo $municipio->id; ?>' type="checkbox" title='Activar el usuario <?php echo $municipio->id; ?>'/>
                                            <?php } ?>
                                        </td>
                                        <td style='text-align: center'>
                                            <a href="<?php echo base_url() ?>index.php/topologia/CMunicipios/editar/<?= $municipio->id; ?>"><i class="fa fa-pencil"></i></a>
                                        </td>
                                        <td style='text-align: center'>
    
                                            <a class='borrar' id='<?php echo $municipio->id; ?>'><i class="fa fa-trash"></i></a>
                                        </td>
                                        </tr>
                                        <?php $i++ ?>
                                    <?php } ?>

                                </tbody>

                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <img  src="<?= base_url() ?>/static/img/footer.png"/>
    </footer>
</div><!-- /wrapper -->



<script>


    var TMun = $('#tab_municipio').dataTable({
        "paging": true,
        "lengthChange": false,
        "autoWidth": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "iDisplayLength": 5,
        "iDisplayStart": 0,
        "sPaginationType": "full_numbers",
        "aLengthMenu": [5, 10, 15],
        "oLanguage": {"sUrl": "<?= base_url() ?>/static/js/es.txt"},
        "aoColumns": [
            {"sClass": "registro center", "sWidth": "1%"},
            {"sClass": "registro center", "sWidth": "10%"},
            {"sClass": "registro center", "sWidth": "10%"},
            {"sClass": "registro center", "sWidth": "15%"},
            {"sClass": "registro center", "sWidth": "1%"},
            {"sWidth": "1%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false},
            {"sWidth": "1%", "bSortable": false, "sClass": "center sorting_false", "bSearchable": false}
        ]
    });

    // Validacion para borrar
    $("table#tab_municipio").on('click', 'a.borrar', function (e) {
        e.preventDefault();
        var id = this.getAttribute('id');
        //alert(id)

        bootbox.dialog({
            message: "¿Está seguro de borrar el Municipio?",
            title: "Borrar registro Municipio",
            buttons: {
                success: {
                    label: "Descartar",
                    className: "btn-primary",
                    callback: function () {

                    }
                },
                danger: {
                    label: "Procesar",
                    className: "btn-success",
                    callback: function () {
                        //alert(id)
                        $.post('<?php echo base_url(); ?>index.php/topologia/CMunicipios/eliminar/' + id + '', function (response) {

                            if (response[0] == "e") {

                                bootbox.alert("Disculpe, Se encuentra asociado a una Parroquia", function () {
                                }).on('hidden.bs.modal', function (event) {
                                });

                            } else {
                                bootbox.alert("Se elimino con exito", function () {
                                }).on('hidden.bs.modal', function (event) {
                                    url = '<?php echo base_url(); ?>index.php/topologia/CMunicipios';
                                    window.location = url;
                                });
                            }
                        });
                    }
                }
            }
        });
    });


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].activar_desactivar, input[type="radio"].activar_desactivar').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    // Función para activar/desactivar un usuario
    $("table#tab_municipio").on('ifChanged', 'input.activar_desactivar', function (e) {
        e.preventDefault();

        var id = this.getAttribute('id');
        //alert(id)

        var check = $(this);

        //~ alert(check.prop('checked'));

        var accion = '';
        if (check.is(':checked')) {
            accion = 'activar';
        } else {
            accion = 'desactivar';
        }

        //~ var padre = $(this).closest('tr');
        //~ var nRow  = padre[0];
        bootbox.confirm("¿Desea " + accion + " el Municipio?", function (result) {
            if (result) {
                $("#motivo_anulacion").val('');
                $("#accion").val(accion);

                var mensaje = "";
                if (accion == 'desactivar') {
                    mensaje = "desactivado";
                } else {
                    mensaje = "activado";
                }

                //~ alert("código de la factura: "+$("#codfactura").val());
                //~ alert("motivo de la anulación: "+$("#motivo_anulacion").val());

                $.post('<?php echo base_url(); ?>index.php/topologia/CMunicipios/activar_desactivar/' + id, {'accion': accion}, function (response) {
                    bootbox.alert("El Municipio fue " + mensaje + " exitosamente", function () {
                    }).on('hidden.bs.modal', function (event) {
                        location.reload();
                    });
                })

            }
        });


    });


    $('#enviar').click(function () {
        url = '<?php echo base_url() ?>index.php/topologia/CMunicipios/registrar';
        window.location = url;
    });





</script>
