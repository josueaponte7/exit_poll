<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControllersEstados
 *
 * @author Ing. Marcel Arcuri
 */
class CEstados extends CI_Controller
{

    public function __construct()
    {


        parent::__construct();

// Load form helper library
        $this->load->helper('form');

        $this->load->helper(array('url'));

        $this->load->view('base');

// Load form validation library
        $this->load->library('form_validation');

// Load session library
        $this->load->library('session');

// Load database
        $this->load->model('topologia/MEstados');
        $this->load->model('topologia/MPaises');
        $this->load->model('busquedas_ajax/ModelsBusqueda');
        $this->load->model('administracion/MAuditoria');
        
    }

    function index()
    {
        //Validacion de configuracion de primer uso
        //Al no haber registros por defecto carga algunos genericos
        $data['listar'] = '';
        $ultimo_id = $this->ModelsBusqueda->count_all_table('top_estados');
        if ($ultimo_id > 0){
            $data['listar'] = $this->MEstados->obtenerEstados();
            $data['listar_paises'] = $this->MPaises->obtenerPais();
        }
        else{
            $this->MEstados->cargarCSV();
            redirect('topologia/CEstados');
        }
        $this->load->view('topologia/estado/lista', $data);
    }

    function registrar()
    {
        $data['ultimo_id']   = $this->ModelsBusqueda->count_all_table('top_estados');
        $data['listar_paises'] = $this->MPaises->obtenerPais();
        $this->load->view('topologia/estado/registrar', $data);
    }
    
    //metodo para guardar un nuevo registro
    public function guardar()
    {

        $result = $this->MEstados->insertarEstados($this->input->post());
        if ($result) {
            
            $param = array(
                'tabla' => 'Estados',
                'codigo' => $this->input->post('codigo'),
                'accion' => 'Registro de nuevo País: '.$this->input->post('descripcion'),
                'fecha' => date('Y-m-d'),
                'hora' => date("h:i:s a"),
                'usuario' => $this->session->userdata['logged_in']['id'],
            );
            $this->MAuditoria->add($param);
            //redirect('configuracion/CEstados');
        }
    }

    //metodo para editar
    function editar()
    {
        $data['id']     = $this->uri->segment(4);
        $data['editar'] = $this->MEstados->obtenerEstado($data['id']);
        $data['listar_paises'] = $this->MPaises->obtenerPais();
        $this->load->view('topologia/estado/editar', $data);
    }
    
    //metodo para eliminar
    function eliminar($id)
    {
        $data = $this->MEstados->obtenerEstado($id);
        //echo $data[0]->descripcion;
        $result = $this->MEstados->eliminarEstados($id);
        $param = array(
            'tabla' => 'Estados',
            'codigo' => $data[0]->codigo,
            'accion' => 'Eliminación de País: '.$data[0]->descripcion,
            'fecha' => date('Y-m-d'),
            'hora' => date("h:i:s a"),
            'usuario' => $this->session->userdata['logged_in']['id'],
        );
        $this->MAuditoria->add($param);
    }

    //Metodo para actualizar
    function actualizar()
    {
        $result = $this->MEstados->actualizarEstados($this->input->post());
        if ($result) {
            
            $param = array(
                'tabla' => 'Estados',
                'codigo' => $this->input->post('codigo'),
                'accion' => 'Edición de País: '.$this->input->post('descripcion'),
                'fecha' => date('Y-m-d'),
                'hora' => date("h:i:s a"),
                'usuario' => $this->session->userdata['logged_in']['id'],
            );
            $this->MAuditoria->add($param);
            //redirect('configuracion/CEstados');
        }
    }
    
     // Método para anular o activar 
    function activar_desactivar($id){
        $accion = $this->input->post('accion');
        $activo = true;
        if ($accion == 'desactivar'){
            $activo = false;
        }
        // Armamos la data a actualizar
        $data = array(
            'id' => $id,
            'activo' => $activo,
        );
        // Actualizamos el usuario con los datos armados
        $result = $this->MEstados->actualizarEstados($data);
    }
}
