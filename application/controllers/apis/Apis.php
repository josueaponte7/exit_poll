<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Apis extends REST_Controller {


    // http://localhost/exitpoll/apis/apis/estados
    public function estados_get()
    {
        $this->load->model('topologia/MEstados');
        $this->response($this->MEstados->obtenerEstados());
    }

    // http://localhost/exitpoll/apis/apis/municipios?estado_id
    public function municipios_get()
    {

        $estado_id = $this->input->get('estado_id');
        $this->load->model('topologia/MMunicipios');
        $this->response($this->MMunicipios->getMunicipio($estado_id));
    }

    // http://localhost/exitpoll/apis/apis/candidatos
    public function candidatos_get()
    {
        $this->load->model('procesos/MCandidatos');
        $this->response($this->MCandidatos->obtenerCandidatos());
    }

    // http://localhost/exitpoll/apis/apis/eleccion
    public function eleccion_get()
    {
        $this->load->model('procesos/MEleccion');
        $this->response($this->MEleccion->obtenerElecciones());
    }

    // http://localhost/exitpoll/apis/apis/votos    * esta es un post
    function votos_post()
    {
        print_r($this->input->get('id'));
    }
}
?>