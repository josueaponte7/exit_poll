<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControllersGEtareos
 *
 * @author Ing. Marcel Arcuri
 */
class CGEtareos extends CI_Controller
{

    public function __construct()
    {


        parent::__construct();

// Load form helper library
        $this->load->helper('form');

        $this->load->helper(array('url'));

        $this->load->view('base2');

// Load form validation library
        $this->load->library('form_validation');

// Load session library
        $this->load->library('session');

// Load database
        $this->load->model('configuracion/MGEtareos');
        $this->load->model('busquedas_ajax/ModelsBusqueda');
        $this->load->model('administracion/MAuditoria');
        
    }

    function index()
    {
        //Validacion de configuracion de primer uso
        //Al no haber registros por defecto carga algunos genericos
        $data['listar'] = '';
        $ultimo_id = $this->ModelsBusqueda->count_all_table('conf_grupos_etareos');
        if ($ultimo_id > 0){
            $data['listar'] = $this->MGEtareos->obtenerGEtareo();
        }
        else{
            $this->MGEtareos->cargarCSV();
            redirect('configuracion/CGEtareos');
        }
        $this->load->view('configuracion/grupos_etareos/lista', $data);
    }

    function registrar()
    {
        $data['ultimo_id']   = $this->ModelsBusqueda->count_all_table('conf_grupos_etareos');
        $this->load->view('configuracion/grupos_etareos/registrar', $data);
    }
    
    //metodo para guardar un nuevo registro
    public function guardar()
    {

        $result = $this->MGEtareos->insertarGEtareos($this->input->post());
        if ($result) {
            
            $param = array(
                'tabla' => 'Grupos Etareos',
                'codigo' => $this->input->post('codigo'),
                'accion' => 'Registro de nuevo Grupo Etareo: '.$this->input->post('descripcion'),
                'fecha' => date('Y-m-d'),
                'hora' => date("h:i:s a"),
                'usuario' => $this->session->userdata['logged_in']['id'],
            );
            $this->MAuditoria->add($param);
            //redirect('configuracion/CGEtareos');
        }
    }

    //metodo para editar
    function editar()
    {
        $data['id']     = $this->uri->segment(4);
        $data['editar'] = $this->MGEtareos->obtenerGEtareos($data['id']);
        $this->load->view('configuracion/grupos_etareos/editar', $data);
    }
    
    //metodo para eliminar
    function eliminar($id)
    {
        $data = $this->MGEtareos->obtenerGEtareos($id);
        $result = $this->MGEtareos->eliminarGEtareos($id);
        $param = array(
            'tabla' => 'Grupos Etareos',
            'codigo' => $data[0]->codigo,
            'accion' => 'Eliminación de Grupo Etareo: '.$data[0]->descripcion,
            'fecha' => date('Y-m-d'),
            'hora' => date("h:i:s a"),
            'usuario' => $this->session->userdata['logged_in']['id'],
        );
        $this->MAuditoria->add($param);
    }

    //Metodo para actualizar
    function actualizar()
    {
        $result = $this->MGEtareos->actualizarGEtareos($this->input->post());
        if ($result) {
            
            $param = array(
                'tabla' => 'Grupos Etareos',
                'codigo' => $this->input->post('codigo'),
                'accion' => 'Edición de Grupo Etareos: '.$this->input->post('descripcion'),
                'fecha' => date('Y-m-d'),
                'hora' => date("h:i:s a"),
                'usuario' => $this->session->userdata['logged_in']['id'],
            );
            $this->MAuditoria->add($param);
            //redirect('configuracion/CGEtareos');
        }
    }
    
     // Método para anular o activar 
    function activar_desactivar($id){
        $accion = $this->input->post('accion');
        $activo = true;
        if ($accion == 'desactivar'){
            $activo = false;
        }
        // Armamos la data a actualizar
        $data = array(
            'id' => $id,
            'activo' => $activo,
        );
        // Actualizamos el usuario con los datos armados
        $result = $this->MGEtareos->actualizarGEtareos($data);
    }
}
