<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControllersEleccion
 *
 * @author Ing. Marcel Arcuri
 */
class CEleccion extends CI_Controller
{

    public function __construct()
    {


        parent::__construct();

// Load form helper library
        $this->load->helper('form');

        $this->load->helper(array('url'));

// Load form validation library
        $this->load->library('form_validation');

// Load session library
        $this->load->library('session');

// Load database
        $this->load->model('procesos/MEleccion');
        $this->load->model('procesos/MCandidatos');
		$this->load->model('topologia/MMunicipios');
        $this->load->model('topologia/MEstados');
        $this->load->model('busquedas_ajax/ModelsBusqueda');
        $this->load->model('administracion/MAuditoria');
        
    }

    function index()
    {
        $this->load->view('base');
		//~ $this->session->sess_destroy();
        $data['listar'] = $this->MEleccion->obtenerElecciones();
        $data['candidatos_elecciones'] = $this->MEleccion->obtenerEleccionesCandidatos();
		$data['listar_municipios'] = $this->MMunicipios->obtenerMunicipios();
        $data['listar_estados'] = $this->MEstados->obtenerEstados();
        $data['candidatos'] = $this->MCandidatos->obtenerCandidatos();
        
        $this->load->view('procesos/eleccion/lista', $data);
    }

    function registrar()
    {
		$this->load->view('base');
        $data['ultimo_id']   = $this->ModelsBusqueda->count_all_table('reg_eleccion');
        
        // Construcción de una lista de candidatos que no estén en otra elección
        $candidatos_libres = array();
        $candidatos = $this->MCandidatos->obtenerCandidatos();
        if(count($candidatos) > 0){
			foreach($candidatos as $candidato){
				$candidato_libre = $this->MCandidatos->obtenerCandidatosOcupados($candidato->id);
				if(count($candidato_libre) == 0){
					$candidatos_libres[] = (object)$candidato;
				}
			}
		}
        $data['listar_municipios'] = $this->MMunicipios->obtenerMunicipios();
        $data['listar_estados'] = $this->MEstados->obtenerEstados();
        $data['list_candidatos']   = $candidatos_libres;
        $this->load->view('procesos/eleccion/registrar', $data);
    }
    
    // Método para guardar un nuevo registro
    public function guardar()
    {
		// Armamos los datos de la elección
		$data = array(
			'id' => $this->input->post('id'),
			'nombre' => $this->input->post('nombre'),
			'estado_id' => $this->input->post('estado_id'),
			'municipio_id' => $this->input->post('municipio_id'),
			'user_create' => $this->input->post('user_create_id'),
			'date_create' => date('Y-m-d')." ".date("H:i:s"),
		);
		
        $result = $this->MEleccion->insertarEleccion($data);        
        
        if ($result) {
            
            // Registro de candidatos para la elección
			foreach($this->input->post('candidatos_ids') as $candidato_id){
				$cand_elec = array(
					'eleccion_id' => $this->input->post('id'),
					'candidato_id' => $candidato_id,
					'user_create' => $this->input->post('user_create_id'),
					'date_create' => date('Y-m-d')." ".date("H:i:s")
				);
				$this->MEleccion->insertarCandidatoEleccion($cand_elec);
			}
            
            $param = array(
                'tabla' => 'Eleccion',
                'codigo' => $this->input->post('codigo'),
                'accion' => 'Registro de nueva Elección: '.$this->input->post('nombre'),
                'fecha' => date('Y-m-d'),
                'hora' => date("h:i:s a"),
                'usuario' => $this->session->userdata['logged_in']['id'],
            );
            $this->MAuditoria->add($param);
        }
    }

    // Método para editar
    function editar()
    {
		$this->load->view('base');
        $data['id']     = $this->uri->segment(4);
        $data['editar'] = $this->MEleccion->obtenerEleccion($data['id']);
        
        $ids_candidatos = "";
        
        $candidatos = $this->MCandidatos->obtenerCandidatos();
        
        $candidatos_asociados = $this->MEleccion->obtenerCandidatosAsociados($data['id']);
        
        if(count($candidatos_asociados) > 0){
			foreach($candidatos_asociados as $candidato){
				$ids_candidatos .= $candidato->candidato_id.",";
			}
			$ids_candidatos = substr($ids_candidatos, 0, -1);
		}
        
        $data['ids_candidatos'] = $ids_candidatos;
        
        $data['list_candidatos']   = $candidatos;
        $data['listar_municipios'] = $this->MMunicipios->obtenerMunicipios();
        $data['listar_estados'] = $this->MEstados->obtenerEstados();
        $this->load->view('procesos/eleccion/editar', $data);
    }
    
    // Método para eliminar
    function eliminar($id)
    {
        $data = $this->MEleccion->obtenerEleccion($id);
        $result = $this->MEleccion->eliminarEleccion($id);
        $param = array(
            'tabla' => 'Eleccion',
            'codigo' => $data[0]->codigo,
            'accion' => 'Eliminación de Elección: '.$data[0]->nombre,
            'fecha' => date('Y-m-d'),
            'hora' => date("h:i:s a"),
            'usuario' => $this->session->userdata['logged_in']['id'],
        );
        $this->MAuditoria->add($param);
    }

    // Método para actualizar
    function actualizar()
    {
		// Armamos los datos de la elección
		$data = array(
			'id' => $this->input->post('id'),
			'nombre' => $this->input->post('nombre'),
			'estado_id' => $this->input->post('estado_id'),
			'municipio_id' => $this->input->post('municipio_id'),
			'user_update' => $this->session->userdata['logged_in']['id'],
			'date_update' => date('Y-m-d')." ".date("H:i:s"),
		);
		
        $result = $this->MEleccion->actualizarEleccion($data);
        
        if ($result) {
            
            // Proceso de registro de candidatos asociados a la elección
			$ids_candidatos = array(); // Aquí almacenaremos los ids de los candidatos a asociar
			// Asociamos las nuevas acciones seleccionadas del combo select
			foreach($this->input->post('candidatos_ids') as $candidato_id){
				// Primero verificamos si ya está asociado cada acción, si no lo está, lo insertamos
				$check_associated = $this->MEleccion->obtener_candidato_ids($data['id'], $candidato_id);
				//~ echo count($check_associated);
				if(count($check_associated) == 0){
					$data_candidato = array(
						'eleccion_id'=>$data['id'],
						'candidato_id'=>$candidato_id,
						'user_create' => $this->input->post('user_create_id'),
						'date_create' => date('Y-m-d')." ".date("H:i:s")
					);
					$this->MEleccion->insertarCandidatoEleccion($data_candidato);
				}
				// Vamos colectando los ids recorridos
				$ids_candidatos[] = $candidato_id;
			}
			
			// Validamos que candidatos han sido quitadas del combo select para proceder a borrar las relaciones
			// Primero buscamos todos los candidatos asociados a la elección
			$query_associated = $this->MEleccion->obtener_candidatos_id($data['id']);
			if(count($query_associated) > 0){
				// Verificamos cuales de los candidatos no están en la nueva lista
				foreach($query_associated as $association){
					if(!in_array($association->candidato_id, $ids_candidatos)){
						// Eliminamos la asociacion de la tabla candidato_eleccion
						$this->MEleccion->delete_candidato_eleccion($data['id'], $association->candidato_id);
					}
				}
			}
            
            $param = array(
                'tabla' => 'Elección',
                'codigo' => $this->input->post('codigo'),
                'accion' => 'Edición de la Elección: '.$this->input->post('nombre'),
                'fecha' => date('Y-m-d'),
                'hora' => date("h:i:s a"),
                'usuario' => $this->session->userdata['logged_in']['id'],
            );
            $this->MAuditoria->add($param);
            //redirect('configuracion/CEleccion');
        }
    }
    
}
