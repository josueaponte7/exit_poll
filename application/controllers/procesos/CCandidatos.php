<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControllersCandidatos
 *
 * @author Ing. Marcel Arcuri
 */
class CCandidatos extends CI_Controller
{

    public function __construct()
    {


        parent::__construct();

// Load form helper library
        $this->load->helper('form');

        $this->load->helper(array('url'));

// Load form validation library
        $this->load->library('form_validation');

// Load session library
        $this->load->library('session');

// Load database
		$this->load->library('image_lib');
        $this->load->model('procesos/MCandidatos');
        $this->load->model('busquedas_ajax/ModelsBusqueda');
        $this->load->model('administracion/MAuditoria');
        
    }

    function index()
    {
		$this->load->view('base');
		//~ $this->session->sess_destroy();
        $data['listar'] = $this->MCandidatos->obtenerCandidatos();
        
        $this->load->view('procesos/candidatos/lista', $data);
    }

    function registrar()
    {
		$this->load->view('base');
        $data['ultimo_id']   = $this->ModelsBusqueda->count_all_table('reg_candidato');
        $this->load->view('procesos/candidatos/registrar', $data);
    }
    
    // Método para guardar un nuevo registro
    public function guardar()
    {
		// Asignamos un nombre genérico si no viene vacío
		if($_FILES['foto']['name'] != ''){
			$ext = explode(".", $_FILES['foto']['name']);
			$ext = $ext[1];  // extensión de la foto
			$nombre_foto = "foto1_".$_POST['id'].".".$ext;
			
		}else{
			$nombre_foto = "";
		}

		// Armamos los datos del candidato
		$data = array(
			'id' => $_POST['id'],
			'nombre' => $_POST['nombre'],
			'apellido' => $_POST['apellido'],
			'foto' => $nombre_foto,
			'user_create' => $_POST['user_create_id'],
			'date_create' => date('Y-m-d')." ".date("H:i:s"),
		);

        $result = $this->MCandidatos->insertarCandidatos($data);
        
        if ($result) {
			
			echo "Candidato registrado con éxito.<br>\n";
            
            // Guardamos la foto en el servidor
            if($_FILES['foto']['name'] != ''){
				$ruta = getcwd();  // Obtiene el directorio actual en donde se esta trabajando
				if (move_uploaded_file($_FILES['foto']['tmp_name'], $ruta."/static/img/candidatos/".$nombre_foto)) {				
					// Localizo la imagen en sistema
					$im = file_get_contents($ruta."/static/img/candidatos/".$nombre_foto);
					// La vuelvo binaria
					$imdata = base64_encode($im);
					$data2 = array(
						'id' => $_POST['id'],
						'foto_binario' => $imdata,
					);
					//print_r($data2);
					$result = $this->MCandidatos->actualizarBinarioCandidatos($data2);
					echo "El fichero es válido y se subió con éxito.";
				} else {
					echo "¡Posible ataque de subida de ficheros!";
				}
			}
            
            // Registro en la bitácora
            $param = array(
                'tabla' => 'Candidatos',
                'codigo' => $this->input->post('codigo'),
                'accion' => 'Registro de nuevo Candidato: '.$this->input->post('nombre')." ".$this->input->post('apellido'),
                'fecha' => date('Y-m-d'),
                'hora' => date("h:i:s a"),
                'usuario' => $this->session->userdata['logged_in']['id'],
            );
            
            $this->MAuditoria->add($param);
        
        }else{
			
			echo "Error al registrar candidato";
			
		}
    }

    // Método para editar
    function editar()
    {
		$this->load->view('base');
        $data['id']     = $this->uri->segment(4);
        $data['editar'] = $this->MCandidatos->obtenerCandidato($data['id']);
        $this->load->view('procesos/candidatos/editar', $data);
    }
    
    // Método para eliminar
    function eliminar($id)
    {
        $data = $this->MCandidatos->obtenerCandidato($id);
        $result = $this->MCandidatos->eliminarCandidato($id);
        $param = array(
            'tabla' => 'Candidatos',
            'codigo' => $data[0]->codigo,
            'accion' => 'Eliminación de Candidato: '.$data[0]->nombre." ".$data[0]->apellido,
            'fecha' => date('Y-m-d'),
            'hora' => date("h:i:s a"),
            'usuario' => $this->session->userdata['logged_in']['id'],
        );
        $this->MAuditoria->add($param);
    }

    // Método para actualizar
    function actualizar()
    {
		// Asignamos un nombre genérico si no viene vacío
		if($_FILES['foto']['name'] != ''){
			$ext = explode(".", $_FILES['foto']['name']);
			$ext = $ext[1];  // extensión de la foto
			$nombre_foto = "foto1_".$_POST['id'].".".$ext;
		}else{
			$nombre_foto = "";
		}
		
		// Armamos los datos del candidato
		$data = array(
			'id' => $_POST['id'],
			'nombre' => $_POST['nombre'],
			'apellido' => $_POST['apellido'],
			'foto' => $nombre_foto,
			'user_update' => $_POST['user_update_id'],
			'date_update' => date('Y-m-d')." ".date("H:i:s"),
		);
		
        $result = $this->MCandidatos->actualizarCandidatos($data);
        
        if ($result) {
			
			echo "Candidato actualizado con éxito.<br>\n";
			
			// Guardamos la foto en el servidor
            if($_FILES['foto']['name'] != ''){
				$ruta = getcwd();  // Obtiene el directorio actual en donde se esta trabajando
				
				
				
				if (move_uploaded_file($_FILES['foto']['tmp_name'], $ruta."/static/img/candidatos/".$nombre_foto)) {

					// Localizo la imagen en sistema
					$im = file_get_contents($ruta."/static/img/candidatos/".$nombre_foto);
					// La vuelvo binaria
					$imdata = base64_encode($im);
					$data2 = array(
						'id' => $_POST['id'],
						'foto_binario' => $imdata,
					);
					//print_r($data2);
					$result = $this->MCandidatos->actualizarBinarioCandidatos($data2);
					echo "El fichero es válido y se subió con éxito.";
				} else {
					echo "¡Posible ataque de subida de ficheros!";
				}
			}
            
            $param = array(
                'tabla' => 'Candidatos',
                'codigo' => $this->input->post('codigo'),
                'accion' => 'Edición de Candidato: '.$this->input->post('nombre')." ".$this->input->post('apellido'),
                'fecha' => date('Y-m-d'),
                'hora' => date("h:i:s a"),
                'usuario' => $this->session->userdata['logged_in']['id'],
            );
            $this->MAuditoria->add($param);
            //redirect('configuracion/CCandidatos');
        }else{
			
			echo "Error al registrar candidato";
			
		}
    }
    
    // Método para anular o activar 
    function activar_desactivar($id){
        $accion = $this->input->post('accion');
        $activo = true;
        if ($accion == 'desactivar'){
            $activo = false;
        }
        // Armamos la data a actualizar
        $data = array(
            'id' => $id,
            'activo' => $activo,
        );
        // Actualizamos el usuario con los datos armados
        $result = $this->MCandidatos->actualizarCandidatos($data);
    }
}
