<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CTendencias
 *
 * @author jsolorzano
 */
class CTendencias extends CI_Controller
{

    public function __construct()
    {


        parent::__construct();

// Load form helper library
        $this->load->helper('form');

        $this->load->helper(array('url'));

        

// Load form validation library
        $this->load->library('form_validation');

// Load session library
        $this->load->library('session');

// Load database
        $this->load->model('reportes/MTendencias');
        $this->load->model('procesos/MEleccion');
        $this->load->model('busquedas_ajax/ModelsBusqueda');
        $this->load->model('usuarios/Usuarios_model');
        
    }

    function index()
    {
		$this->load->view('base');
		$data['listar_elecciones'] = $this->MEleccion->obtenerElecciones();
        $this->load->view('reportes/tendencias', $data);
    }
    function cargar_grafica_tendencias(){
        $eleccion_id = $this->input->post('eleccion_id');
        $candidatos = $this->MTendencias->search_candidatos($eleccion_id);

		// Alternativa que también funciona
		foreach($candidatos as $r){
			$id_cand = $r->candidato_id;
			$tendencias = $this->MTendencias->search_tendencia($eleccion_id,$id_cand);
			$reg = array();
			$completo = array();
			foreach($tendencias as $t){
				$hora = $t->hora;
				$porcentaje = $t->porcentaje;
				$nombre= $t->nombre;
				$reg['hora'] = trim($hora);
				$reg['porcentaje'] = $porcentaje;
				$reg['nombre'] = $nombre;
				
				// Listamos el registro
				$lista_regs[] = $reg;
			}
		
		}	
		echo json_encode($lista_regs);
		//echo json_encode($lista_cant_pagos);
    }
    // Consulta de auditorías
    //function obtenerPagos($estatus,$desde,$hasta)
    //{
    //    $data = $this->MLPagos->obtenerPagosEspBit($estatus,$desde,$hasta);  // Datos generales
    //    return $data;
    //}
    
    
    // Generación de reporte de pagos
    //~ function pdf_pagos($cuenta,$estatus,$desde,$hasta)
//    function pdf_pagos($estatus,$desde,$hasta)
//    {
//        $data['pagos'] = $this->MLPagos->obtenerPagosEspBit($estatus,$desde,$hasta);  // Datos generales del los pagos
//
//		$data['usuarios'] = $this->Usuarios_model->obtenerUsuarios();  // Usuarios
//		$data['usuario'] = $this->ModelsBusqueda->obtenerRegistro('usuarios', 'id', $this->session->userdata['logged_in']['id']); // Usuario en sesión
//        $data['desde'] = $desde;  // Fecha de inicio
//        $data['hasta'] = $hasta;  // Fecha final
//        
//        $this->load->view('reportes/pdf/reporte_pagos', $data);
//    }    
    
}
