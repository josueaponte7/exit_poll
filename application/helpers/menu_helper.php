<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
if (!function_exists('menu')) {

    function menu($datos_sesion) {
        $ci = & get_instance();

        $user_id = $ci->session->userdata('user_id');
        $modulo_ids = array();
        //$menu = $ci->libreria->menu($modulo_ids);
        //foreach ($menu as $value) {
        //    $submenu = $ci->libreria->submenu($value->id);
        //    ?>
        //    <!--<li class="active treeview">
        //        <a href="#">
        //            <i class="fa fa-wrench"></i> <span><?php echo $value->modulo; ?></span> <i class="fa fa-angle-left pull-right"></i>
        //        </a>
        //        <ul class="treeview-menu">
        //    <?php
        //    foreach ($submenu as $values) {
        //        ?>
        //                <li><a href="<?php echo base_url(); ?>index.php/<?php echo $values->route; ?>"><i class="fa fa-circle-o"></i><?php echo $values->modulo; ?></a></li>
        //
        //        <?php
        //    }
        //    ?>
        //
        //        </ul>
        //
        //    </li>-->
        //    <?php
        //}
        ?>
        <?php if ($datos_sesion['tipouser'] == 'BÁSICO' ): ?>
        
		<li class="treeview">
            <a href="#">
                <i class="fa fa-puzzle-piece text-teal"></i><span>Exitpoll</span><i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
		        <li><a href="<?php echo base_url(); ?>index.php/procesos/CCandidatos"><i class="fa fa-money text-teal"></i>Candidato</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/procesos/CEleccion"><i class="fa fa-cc-discover text-teal"></i>Elección</a></li>
            </ul>
        </li>
        <?php endif; ?>
        
        <?php if ($datos_sesion['tipouser'] == 'Administrador' || $datos_sesion['tipouser'] == 'OPERADOR'): ?>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-file-text text-maroon"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>index.php/reportes/CTendencias"><i class="fa fa-area-chart text-maroon"></i>Gráfica de Tendencias</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-puzzle-piece text-primary"></i><span>Registrar</span><i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
		        <li><a href="<?php echo base_url(); ?>index.php/procesos/CCandidatos"><i class="fa fa-address-card-o text-primary"></i>Candidatos</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/procesos/CEleccion"><i class="fa fa-list text-primary"></i>Exit Polls</a></li>
            </ul>
        </li>
<!-- 	   <li class="treeview">
            <a href="#">
                <i class="fa fa-cogs text-maroon"></i><span>Configuraciones</span><i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>index.php/configuracion/CGEtareos"><i class="fa fa-users text-teal"></i>Grupos Etareos</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/configuracion/CTipoElecciones"><i class="fa fa-list-ul text-teal"></i>Tipo de Elección</a></li>
            </ul>
        </li> -->
        <li class="treeview">
            <a href="#">
                <i class="fa fa-globe text-green"></i><span>Topología</span><i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>index.php/topologia/CPaises"><i class="fa fa-globe text-green"></i>Paises</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/topologia/CEstados"><i class="fa fa-globe text-green"></i>Estados</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/topologia/CMunicipios"><i class="fa fa-globe text-green"></i>Municipios</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/topologia/CParroquias"><i class="fa fa-globe text-green"></i>Parroquias</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/topologia/CCiudades"><i class="fa fa-globe text-green"></i>Ciudades</a></li>
            </ul>
        </li>
		<?php endif; ?>
		
	    <?php if ($datos_sesion['tipouser'] == 'Administrador'): ?>
	    <li class="treeview">
            <a href="#">
                <i class="fa fa-shield text-orange"></i><span>Administración</span><i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
        		<li><a href="<?php echo base_url(); ?>index.php/administracion/CAuditoria"><i class="fa fa-book text-orange"></i>Bitacora</a></li>
        		<!--<li><a href="<?php echo base_url(); ?>index.php/administracion/CEmpresa/"><i class="fa fa-industry text-orange"></i>Empresa</a></li>-->
        		<li><a href="<?php echo base_url(); ?>index.php/configuracion/grupos_usuarios/ControllersGrupoUsuarios"><i class="fa fa-object-group text-orange"></i> Gestión de Grupos</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/configuracion/usuarios/usuarios"><i class="fa fa-user text-orange"></i>Gestión de Usuarios</a></li>
                <li><a href="#" id="rec_password"><i class="fa fa-shield text-orange"></i>Rec: Clave de Acceso</a></li>
            </ul>
        </li>
        <?php endif; ?>


        <?php
    }

}
