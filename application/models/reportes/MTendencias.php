<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModelStandard
 *
 * @author jesus
 */
class MTendencias extends CI_Model
{
    //put your code here}
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	public function search_candidatos($id_ele){
		$result = $this->db->where('eleccion_id =', $id_ele);
        $result = $this->db->get('rel_candidato_eleccion');
		return $result->result();
    }
	public function search_tendencia($id_ele, $id_cand){
		$sql_select = "SELECT  (date_part('hour',  hora::time)||':00') as hora,  ROUND (COUNT(candidatos_id)*100/(SELECT COUNT (candidatos_id) ";
		$sql_select .= " FROM rep_resultados WHERE  eleccion_id=".$id_ele."), 2):: text  porcentaje, CONCAT(c.nombre,' ',c.apellido) nombre   ";
		$sql_select .= " FROM rep_resultados AS v inner join reg_candidato as c on v.candidatos_id=c.id ";
		$sql_select .= " WHERE candidatos_id =".$id_cand." AND hora::time <= '22:30'   ";
		$sql_select .= " GROUP BY date_part('hour', hora::time),candidatos_id,c.nombre, c.apellido order by date_part('hour', hora::time) ASC  ";
		//echo $sql_select;
		$query3 = $this->db->query($sql_select);
		return $query3->result();
    }
}
