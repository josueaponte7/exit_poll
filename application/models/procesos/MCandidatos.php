<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModelsCandidatos
 *
 * @author Ing. Marcel Arcuri
 */
class MCandidatos extends CI_Model {
    //put your code here


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //Método público para obterner la unidad de medida
    public function obtenerCandidatos() {
        $query = $this->db->get('reg_candidato');
        
        if($query->num_rows()>0) return $query->result();
         else return $query->result();
    }
    
    //Método público para obterner la unidad de medida
    public function obtenerCandidatosOcupados($candidato_id) {
        $this->db->where('candidato_id',$candidato_id);    
        $query = $this->db->get('rel_candidato_eleccion');        
        if($query->num_rows()>0) return $query->result();
        else return $query->result();
    }
    
    // Método público, forma de insertar los datos
    public function insertarCandidatos($datos){
        
        $result = $this->db->where('nombre =', $datos['nombre']);
        $result = $this->db->get('reg_candidato');

        if ($result->num_rows() > 0) {
            echo '1';
        }else{
            $result = $this->db->insert("reg_candidato", $datos);
            return $result;
        }
    }
    
    // Método público, para obterner la unidad de medida por id
    public function obtenerCandidato($id){
        $this->db->where('id',$id);    
        $query = $this->db->get('reg_candidato');        
        if($query->num_rows()>0) return $query->result();
        else return $query->result();
    }
    // Método público, para actualizar un registro 
    public function actualizarBinarioCandidatos($datos) {
        
        $sql_select = "UPDATE reg_candidato SET foto_binario = '".$datos['foto_binario']."' WHERE id =".$datos['id'];
		$query3 = $this->db->query($sql_select);
    }
    
    // Método público, para actualizar un registro 
    public function actualizarCandidatos($datos) {
        
        $result = $this->db->where('nombre =', $datos['nombre']);
        $result = $this->db->where('id !=', $datos['id']);
        $result = $this->db->get('reg_candidato');

        
        if ($result->num_rows() > 0) {
            echo '1';
        }else {
            $result = $this->db->where('id', $datos['id']);
            $result = $this->db->update('reg_candidato', $datos);
            return $result;
        }
    }

    // Método público, para eliminar un registro 
    public function eliminarCandidato($id) {
        $result = $this->db->where('candidato_id', $id);
        $result = $this->db->get('rel_candidato_eleccion');
        
        if ($result->num_rows() > 0) {
            echo 'e';
        }else {
            $result = $this->db->delete('reg_candidato', array('id'=>$id));
            return $result; 
        }
            
    }
}
