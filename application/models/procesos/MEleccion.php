<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModelsEleccion
 *
 * @author Ing. Marcel Arcuri
 */
class MEleccion extends CI_Model {
    //put your code here


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //Método público para obterner la unidad de medida
    public function obtenerElecciones() {
        $query = $this->db->get('reg_eleccion');
        
        if($query->num_rows()>0) return $query->result();
         else return $query->result();
    }
    
    //Método público para obterner la unidad de medida
    public function obtenerCandidatosAsociados($eleccion_id) {
        $this->db->where('eleccion_id',$eleccion_id);    
        $query = $this->db->get('rel_candidato_eleccion');        
        if($query->num_rows()>0) return $query->result();
        else return $query->result();
    }
    
    //Public method to obtain the actions asociated
    public function obtenerEleccionesCandidatos() {
        $query = $this->db->get('rel_candidato_eleccion');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return $query->result();
    }
    
    //Public method to obtain the candidatos asociated by id_eleccion
    public function obtener_candidatos_id($id_eleccion) {
		$this->db->where('eleccion_id =', $id_eleccion);
        $query = $this->db->get('rel_candidato_eleccion');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return $query->result();
    }
    
    //Public method to obtain the candidatos asociated by eleccion_id and candidato_id
    public function obtener_candidato_ids($id_eleccion, $id_candidato) {
		$this->db->where('eleccion_id =', $id_eleccion);
		$this->db->where('candidato_id =', $id_candidato);
        $query = $this->db->get('rel_candidato_eleccion');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return $query->result();
    }
    
    // Método público, forma de insertar los datos
    public function insertarEleccion($datos){
        
        $result = $this->db->where('nombre =', $datos['nombre']);
        $result = $this->db->get('reg_eleccion');

        if ($result->num_rows() > 0) {
            return 'existe';
        }else{
            $result = $this->db->insert("reg_eleccion", $datos);
            return $result;
        }
    }
    
    // Método público, forma de insertar los datos
    public function insertarCandidatoEleccion($datos){
        
        $result = $this->db->where('eleccion_id =', $datos['eleccion_id']);
        $result = $this->db->where('candidato_id =', $datos['candidato_id']);
        $result = $this->db->get('rel_candidato_eleccion');

        if ($result->num_rows() == 0) {
            $result = $this->db->insert("rel_candidato_eleccion", $datos);
        }
    }
    
    // Método público, para obterner la unidad de medida por id
    public function obtenerEleccion($id){
        $this->db->where('id',$id);    
        $query = $this->db->get('reg_eleccion');        
        if($query->num_rows()>0) return $query->result();
        else return $query->result();
    }

    
   // Método público, para actualizar un registro 
    public function actualizarEleccion($datos) {
        
        $result = $this->db->where('nombre =', $datos['nombre']);
        $result = $this->db->where('id !=', $datos['id']);
        $result = $this->db->get('reg_eleccion');

        
        if ($result->num_rows() > 0) {
            echo '1';
        }else {
            $result = $this->db->where('id', $datos['id']);
            $result = $this->db->update('reg_eleccion', $datos);
            return $result;
        }
    }

    // Método público, para eliminar un registro 
    public function eliminarEleccion($id) {
        $result = $this->db->where('eleccion_id', $id);
        $result = $this->db->get('rel_candidato_eleccion');
        
        if ($result->num_rows() > 0) {
            $result = $this->db->delete('rel_candidato_eleccion', array('eleccion_id'=>$id));
            $result2 = $this->db->delete('reg_eleccion', array('id'=>$id));
            return $result2;
        }else {
            $result = $this->db->delete('reg_eleccion', array('id'=>$id));
            return $result; 
        }
            
    }
    
    // Public method to delete the candidatos asociated 
    public function delete_rel_candidato_eleccion($id_eleccion, $id_candidato) {
		$result = $this->db->delete('rel_candidato_eleccion', array('eleccion_id' => $id_eleccion, 'candidato_id' => $id_candidato));
    }
}
