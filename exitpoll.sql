--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.15
-- Dumped by pg_dump version 9.4.15
-- Started on 2017-11-27 23:21:57 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1 (class 3079 OID 11861)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2142 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 1151723)
-- Name: adm_empresa; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE adm_empresa (
    id integer NOT NULL,
    codigo integer,
    nombre_empresa character varying(50),
    rif character varying(20),
    cedula integer,
    nombre character varying(25),
    apellido character varying(25),
    telefono1 character varying(20),
    telefono2 character varying(20),
    correo character varying(40),
    direccion character varying(150),
    logo character varying(500)
);


--
-- TOC entry 174 (class 1259 OID 1151729)
-- Name: adm_empresa_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE adm_empresa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2143 (class 0 OID 0)
-- Dependencies: 174
-- Name: adm_empresa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE adm_empresa_id_seq OWNED BY adm_empresa.id;


--
-- TOC entry 175 (class 1259 OID 1151731)
-- Name: auditoria; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auditoria (
    id integer NOT NULL,
    tabla character varying(200),
    codigo character varying(200),
    accion character varying(200),
    fecha character varying(10) NOT NULL,
    hora character varying(12) NOT NULL,
    usuario integer
);


--
-- TOC entry 176 (class 1259 OID 1151737)
-- Name: auditoria_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auditoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2144 (class 0 OID 0)
-- Dependencies: 176
-- Name: auditoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auditoria_id_seq OWNED BY auditoria.id;


--
-- TOC entry 177 (class 1259 OID 1151739)
-- Name: rel_candidato_eleccion; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE rel_candidato_eleccion (
    id integer NOT NULL,
    eleccion_id integer,
    candidato_id integer,
    user_create integer,
    date_create timestamp without time zone,
    user_update integer,
    date_update timestamp without time zone
);


--
-- TOC entry 178 (class 1259 OID 1151742)
-- Name: candidato_eleccion_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE candidato_eleccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2145 (class 0 OID 0)
-- Dependencies: 178
-- Name: candidato_eleccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE candidato_eleccion_id_seq OWNED BY rel_candidato_eleccion.id;


--
-- TOC entry 179 (class 1259 OID 1151744)
-- Name: reg_candidato; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE reg_candidato (
    id integer NOT NULL,
    codigo integer,
    nombre character varying(50) NOT NULL,
    apellido character varying(50) NOT NULL,
    foto character varying(50),
    activo boolean,
    user_create integer,
    date_create timestamp without time zone,
    user_update integer,
    date_update timestamp without time zone,
    foto_binario text
);


--
-- TOC entry 180 (class 1259 OID 1151747)
-- Name: candidato_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE candidato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2146 (class 0 OID 0)
-- Dependencies: 180
-- Name: candidato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE candidato_id_seq OWNED BY reg_candidato.id;


--
-- TOC entry 181 (class 1259 OID 1151749)
-- Name: claves_sistema; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE claves_sistema (
    id integer NOT NULL,
    clave character varying(128),
    fecha character varying(10),
    hora character varying(12),
    user_create integer
);


--
-- TOC entry 182 (class 1259 OID 1151752)
-- Name: claves_sistema_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE claves_sistema_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2147 (class 0 OID 0)
-- Dependencies: 182
-- Name: claves_sistema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE claves_sistema_id_seq OWNED BY claves_sistema.id;


--
-- TOC entry 183 (class 1259 OID 1151754)
-- Name: conf_grupo_user; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE conf_grupo_user (
    id integer NOT NULL,
    codigo integer,
    name character varying(50) NOT NULL,
    activo boolean,
    user_create integer,
    date_create timestamp without time zone,
    user_update integer,
    date_update timestamp without time zone
);


--
-- TOC entry 184 (class 1259 OID 1151757)
-- Name: conf_grupo_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE conf_grupo_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2148 (class 0 OID 0)
-- Dependencies: 184
-- Name: conf_grupo_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE conf_grupo_user_id_seq OWNED BY conf_grupo_user.id;


--
-- TOC entry 185 (class 1259 OID 1151759)
-- Name: reg_eleccion; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE reg_eleccion (
    id integer NOT NULL,
    codigo integer,
    nombre character varying(50) NOT NULL,
    estado_id integer,
    municipio_id integer,
    user_create integer,
    date_create timestamp without time zone,
    user_update integer,
    date_update timestamp without time zone
);


--
-- TOC entry 186 (class 1259 OID 1151762)
-- Name: eleccion_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE eleccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2149 (class 0 OID 0)
-- Dependencies: 186
-- Name: eleccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE eleccion_id_seq OWNED BY reg_eleccion.id;


--
-- TOC entry 187 (class 1259 OID 1151764)
-- Name: rep_resultados; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE rep_resultados (
    id integer NOT NULL,
    candidatos_id integer NOT NULL,
    eleccion_id integer,
    hora character varying(15)
);


--
-- TOC entry 188 (class 1259 OID 1151767)
-- Name: rep_resultados_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE rep_resultados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2150 (class 0 OID 0)
-- Dependencies: 188
-- Name: rep_resultados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE rep_resultados_id_seq OWNED BY rep_resultados.id;


--
-- TOC entry 189 (class 1259 OID 1151769)
-- Name: top_ciudades; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE top_ciudades (
    id integer NOT NULL,
    codigo integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    estado_id integer NOT NULL,
    municipio_id integer NOT NULL,
    pais_id integer NOT NULL,
    activo boolean
);


--
-- TOC entry 190 (class 1259 OID 1151772)
-- Name: top_ciudades_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE top_ciudades_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2151 (class 0 OID 0)
-- Dependencies: 190
-- Name: top_ciudades_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE top_ciudades_id_seq OWNED BY top_ciudades.id;


--
-- TOC entry 191 (class 1259 OID 1151774)
-- Name: top_estados; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE top_estados (
    id integer NOT NULL,
    codigo integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    pais_id integer NOT NULL,
    activo boolean
);


--
-- TOC entry 192 (class 1259 OID 1151777)
-- Name: top_estados_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE top_estados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2152 (class 0 OID 0)
-- Dependencies: 192
-- Name: top_estados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE top_estados_id_seq OWNED BY top_estados.id;


--
-- TOC entry 193 (class 1259 OID 1151779)
-- Name: top_municipios; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE top_municipios (
    id integer NOT NULL,
    codigo integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    estado_id integer NOT NULL,
    pais_id integer NOT NULL,
    activo boolean
);


--
-- TOC entry 194 (class 1259 OID 1151782)
-- Name: top_municipios_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE top_municipios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2153 (class 0 OID 0)
-- Dependencies: 194
-- Name: top_municipios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE top_municipios_id_seq OWNED BY top_municipios.id;


--
-- TOC entry 195 (class 1259 OID 1151784)
-- Name: top_paises; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE top_paises (
    id integer NOT NULL,
    codigo integer,
    descripcion character varying(50),
    activo boolean
);


--
-- TOC entry 196 (class 1259 OID 1151787)
-- Name: top_paises_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE top_paises_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2154 (class 0 OID 0)
-- Dependencies: 196
-- Name: top_paises_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE top_paises_id_seq OWNED BY top_paises.id;


--
-- TOC entry 197 (class 1259 OID 1151789)
-- Name: top_parroquias; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE top_parroquias (
    id integer NOT NULL,
    codigo integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    estado_id integer NOT NULL,
    municipio_id integer NOT NULL,
    pais_id integer NOT NULL,
    activo boolean
);


--
-- TOC entry 198 (class 1259 OID 1151792)
-- Name: top_parroquias_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE top_parroquias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2155 (class 0 OID 0)
-- Dependencies: 198
-- Name: top_parroquias_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE top_parroquias_id_seq OWNED BY top_parroquias.id;


--
-- TOC entry 199 (class 1259 OID 1151794)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE usuarios (
    id integer NOT NULL,
    codigo integer,
    username character varying(30),
    password character varying(128),
    cedula integer,
    first_name character varying(70),
    last_name character varying(70),
    email character varying(75),
    tipo_usuario character varying(100),
    cargo character varying(100),
    telefono character varying(20),
    estatus boolean,
    picture character varying(500),
    fecha_create timestamp with time zone,
    fecha_update timestamp with time zone,
    user_create_id integer
);


--
-- TOC entry 200 (class 1259 OID 1151800)
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE usuarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2156 (class 0 OID 0)
-- Dependencies: 200
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE usuarios_id_seq OWNED BY usuarios.id;


--
-- TOC entry 1967 (class 2604 OID 1151802)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY adm_empresa ALTER COLUMN id SET DEFAULT nextval('adm_empresa_id_seq'::regclass);


--
-- TOC entry 1968 (class 2604 OID 1151803)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auditoria ALTER COLUMN id SET DEFAULT nextval('auditoria_id_seq'::regclass);


--
-- TOC entry 1971 (class 2604 OID 1151804)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY claves_sistema ALTER COLUMN id SET DEFAULT nextval('claves_sistema_id_seq'::regclass);


--
-- TOC entry 1972 (class 2604 OID 1151805)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY conf_grupo_user ALTER COLUMN id SET DEFAULT nextval('conf_grupo_user_id_seq'::regclass);


--
-- TOC entry 1970 (class 2604 OID 1151806)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY reg_candidato ALTER COLUMN id SET DEFAULT nextval('candidato_id_seq'::regclass);


--
-- TOC entry 1973 (class 2604 OID 1151807)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY reg_eleccion ALTER COLUMN id SET DEFAULT nextval('eleccion_id_seq'::regclass);


--
-- TOC entry 1969 (class 2604 OID 1151808)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY rel_candidato_eleccion ALTER COLUMN id SET DEFAULT nextval('candidato_eleccion_id_seq'::regclass);


--
-- TOC entry 1974 (class 2604 OID 1151809)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY rep_resultados ALTER COLUMN id SET DEFAULT nextval('rep_resultados_id_seq'::regclass);


--
-- TOC entry 1975 (class 2604 OID 1151810)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY top_ciudades ALTER COLUMN id SET DEFAULT nextval('top_ciudades_id_seq'::regclass);


--
-- TOC entry 1976 (class 2604 OID 1151811)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY top_estados ALTER COLUMN id SET DEFAULT nextval('top_estados_id_seq'::regclass);


--
-- TOC entry 1977 (class 2604 OID 1151812)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY top_municipios ALTER COLUMN id SET DEFAULT nextval('top_municipios_id_seq'::regclass);


--
-- TOC entry 1978 (class 2604 OID 1151813)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY top_paises ALTER COLUMN id SET DEFAULT nextval('top_paises_id_seq'::regclass);


--
-- TOC entry 1979 (class 2604 OID 1151814)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY top_parroquias ALTER COLUMN id SET DEFAULT nextval('top_parroquias_id_seq'::regclass);


--
-- TOC entry 1980 (class 2604 OID 1151815)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY usuarios ALTER COLUMN id SET DEFAULT nextval('usuarios_id_seq'::regclass);


--
-- TOC entry 2108 (class 0 OID 1151723)
-- Dependencies: 173
-- Data for Name: adm_empresa; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO adm_empresa (id, codigo, nombre_empresa, rif, cedula, nombre, apellido, telefono1, telefono2, correo, direccion, logo) VALUES (1, 1, 'CLUB AHORRO', '', 19363480, 'MARCEL ROBERT', 'ARCURI GOMEZ', '(0424) 384-4484', '(1351) 361-6511', 'exitpoll@gmail.com', 'Maracay, Edo Aragua', 'logo.jpg');


--
-- TOC entry 2157 (class 0 OID 0)
-- Dependencies: 174
-- Name: adm_empresa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('adm_empresa_id_seq', 1, false);


--
-- TOC entry 2110 (class 0 OID 1151731)
-- Dependencies: 175
-- Data for Name: auditoria; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (1, '', '', 'Inicio de Sesion', '2016-08-30', '12:50:21 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (2, '', '', 'Inicio de Sesion', '2016-08-30', '12:54:48 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (3, 'Empresa', '1', 'Editar Empresa', '2016-08-30', '01:04:40 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (4, 'Bancos', '1', 'Nueva Bancos', '2016-08-30', '01:05:04 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (5, 'Bancos', '2', 'Nueva Bancos', '2016-08-30', '01:05:28 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (6, 'Bancos', '3', 'Nueva Bancos', '2016-08-30', '01:07:27 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (7, 'Bancos', '4', 'Nueva Bancos', '2016-08-30', '01:07:39 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (8, 'Bancos', '5', 'Nueva Bancos', '2016-08-30', '01:08:26 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (9, 'Bancos', '6', 'Nueva Bancos', '2016-08-30', '01:08:44 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (10, 'Bancos', '7', 'Nueva Bancos', '2016-08-30', '01:09:00 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (11, 'Bancos', '8', 'Nueva Bancos', '2016-08-30', '01:09:09 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (12, '', '', 'Inicio de Sesion', '2016-09-05', '10:35:14 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (13, '', '', 'Inicio de Sesion', '2016-09-06', '02:45:03 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (14, 'Tipos de Inmueble', '1', 'Nueva Tipo Inmueble', '2016-09-06', '02:45:19 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (15, 'Tipos de Inmueble', '2', 'Nueva Tipo Inmueble', '2016-09-06', '02:45:29 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (16, 'Inmuebles', '1', 'Nueva Inmuebles', '2016-09-06', '02:54:35 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (17, 'Inmuebles', '1', 'Editar Inmuebles', '2016-09-06', '02:56:50 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (18, 'RelBancos', NULL, 'Nueva RelBancos', '2016-09-06', '02:57:59 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (19, 'Copropietarios', '1', 'Nueva Copropietarios', '2016-09-06', '03:03:39 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (20, 'Copropietarios', '2', 'Nueva Copropietarios', '2016-09-06', '03:04:46 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (21, 'Copropietarios', '3', 'Nueva Copropietarios', '2016-09-06', '03:06:59 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (22, 'Copropietarios', '4', 'Nueva Copropietarios', '2016-09-06', '03:07:45 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (23, 'Copropietarios', '5', 'Nueva Copropietarios', '2016-09-06', '03:09:12 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (24, 'Copropietarios', '6', 'Nueva Copropietarios', '2016-09-06', '03:15:58 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (25, 'Bancos', '9', 'Nueva Bancos', '2016-09-06', '03:21:32 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (26, 'Inmuebles', '2', 'Nueva Inmuebles', '2016-09-06', '03:47:56 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (27, 'RelBancos', NULL, 'Nueva RelBancos', '2016-09-06', '03:48:22 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (28, 'RelBancos', NULL, 'Nueva RelBancos', '2016-09-06', '03:48:50 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (29, '', '', 'Inicio de Sesion', '2016-09-06', '04:59:34 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (30, 'Recibos', '1', 'Nueva Recibo', '2016-09-06', '05:00:39 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (31, 'RelRecibos', NULL, 'Nueva RelRecibos', '2016-09-06', '05:01:15 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (32, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '07:40:37 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (33, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '07:40:59 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (34, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '07:41:19 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (35, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '07:41:59 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (36, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '08:02:07 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (37, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '08:07:16 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (38, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '08:23:09 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (39, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '08:33:04 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (40, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '08:33:48 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (41, 'RelRecibos', NULL, 'Nueva RelRecibos', '2016-09-06', '08:38:24 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (42, 'RelRecibos', NULL, 'Nueva RelRecibos', '2016-09-06', '08:38:59 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (43, 'RelRecibos', NULL, 'Nueva RelRecibos', '2016-09-06', '08:39:33 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (44, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-06', '10:16:14 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (45, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-07', '02:18:12 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (46, 'Recibos Rel Ingresos', NULL, 'Nueva Rel Ingresos', '2016-09-07', '02:19:14 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (47, 'Recibos Rel Ingresos', NULL, 'Eliminar Rel Ingresos', '2016-09-07', '02:21:14 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (48, 'Recibos Rel Ingresos', NULL, 'Eliminar Rel Ingresos', '2016-09-07', '02:21:21 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (49, 'Recibos Rel Cargos', NULL, 'Nueva Rel Recibos Cargos', '2016-09-07', '02:22:02 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (50, 'Recibos Rel Cargos', NULL, 'Nueva Rel Recibos Cargos', '2016-09-07', '02:24:30 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (51, 'RelRecibos', NULL, 'Nueva RelRecibos', '2016-09-07', '02:32:39 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (52, 'Recibos Rel Ingresos', NULL, 'Eliminar RelBancos', '2016-09-07', '02:32:46 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (53, 'Recibos Rel Cargos', NULL, 'Eliminar Rel Recibos Cargos', '2016-09-07', '02:51:40 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (54, 'Recibos Rel Cargos', NULL, 'Nueva Rel Recibos Cargos', '2016-09-07', '03:42:52 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (55, '', '', 'Inicio de Sesion', '2016-09-07', '07:22:38 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (56, 'Inmuebles', '1', 'Editar Inmuebles', '2016-09-08', '12:39:57 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (57, '', '', 'Inicio de Sesion', '2016-09-08', '04:03:05 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (58, '', '', 'Inicio de Sesion', '2016-09-08', '04:32:59 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (59, 'Inmuebles', '1', 'Editar Inmuebles', '2016-09-08', '04:36:40 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (60, 'RelBancos', NULL, 'Nueva RelBancos', '2016-09-08', '01:28:02 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (61, '', '', 'Inicio de Sesion', '2016-09-09', '09:10:00 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (62, '', '', 'Inicio de Sesion', '2016-09-12', '08:55:09 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (63, '', '', 'Inicio de Sesion', '2016-09-13', '08:43:06 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (64, '', '', 'Inicio de Sesion', '2016-09-13', '08:45:08 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (65, '', '', 'Inicio de Sesion', '2016-09-13', '01:42:43 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (66, '', '', 'Inicio de Sesion', '2016-09-15', '10:30:29 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (67, '', '', 'Inicio de Sesion', '2016-09-22', '09:06:29 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (68, '', '', 'Inicio de Sesion', '2016-09-27', '10:38:43 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (69, '', '', 'Inicio de Sesion', '2016-09-27', '10:40:50 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (70, 'Egresos', NULL, 'Nueva Recibo', '2016-09-27', '10:41:13 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (71, 'Rel Egresos', NULL, 'Nueva Rel Egresos', '2016-09-27', '10:43:10 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (72, 'Rel Egresos', NULL, 'Nueva Rel Egresos', '2016-09-27', '10:48:09 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (73, 'Rel Egresos', NULL, 'Nueva Rel Egresos', '2016-09-27', '10:49:15 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (74, '', '', 'Inicio de Sesion', '2016-09-27', '02:57:53 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (75, 'RelBancos', '5', 'Actualizacion RelBancos', '2016-09-27', '03:00:33 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (76, 'RelBancos', '5', 'Eliminar RelBancos', '2016-09-27', '03:01:22 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (77, 'RelBancos', '1', 'Actualizacion RelBancos', '2016-09-27', '03:03:05 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (78, 'RelBancos', '6', 'Eliminar RelBancos', '2016-09-27', '03:03:31 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (79, 'RelBancos', '1', 'Actualizacion RelBancos', '2016-09-27', '03:05:44 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (80, '', '', 'Inicio de Sesion', '2016-09-28', '11:09:17 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (81, '', '', 'Inicio de Sesion', '2016-10-19', '10:37:42 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (82, '', '', 'Cerrada la Sesión', '2016-10-19', '10:49:57 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (83, '', '', 'Inicio de Sesion', '2016-10-19', '10:57:16 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (84, '', '', 'Cerrada la Sesión', '2016-10-19', '10:58:35 am', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (85, '', '', 'Inicio de Sesion', '2016-10-24', '08:06:38 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (86, '', '', 'Inicio de Sesion', '2016-10-24', '08:06:49 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (87, '', '', 'Cerrada la Sesión', '2016-10-24', '08:10:20 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (88, '', '', 'Inicio de Sesion', '2016-10-26', '10:17:06 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (89, 'Tipos de Cuenta', '1', 'Nuevo Tipo de Cuenta', '2016-10-26', '11:48:47 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (90, '', '', 'Inicio de Sesion', '2016-10-27', '04:17:57 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (91, 'Tipos de Cuenta', '1', 'Editar Tipo de Cuenta', '2016-10-27', '04:18:23 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (92, '', '', 'Cerrada la Sesión', '2016-10-27', '05:00:21 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (93, '', '', 'Inicio de Sesion', '2016-10-27', '08:08:37 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (94, 'Tipos de Cuenta', '1', 'Eliminar Tipo de Cuenta', '2016-10-27', '08:45:52 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (95, 'Tipos de Cuenta', '1', 'Nuevo Tipo de Cuenta', '2016-10-27', '09:12:48 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (96, 'Tipos de Cuenta', '1', 'Nueva Cuenta', '2016-10-27', '09:22:04 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (97, 'Tipos de Cuenta', '2', 'Nueva Cuenta', '2016-10-27', '09:25:18 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (98, 'Tipos de Cuenta', '2', 'Eliminar Cuenta', '2016-10-27', '09:26:37 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (99, '', '', 'Inicio de Sesion', '2016-10-29', '03:57:28 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (100, 'Grupos Usuarios', '2', 'Nueva Grupos Usuarios', '2016-10-29', '04:46:57 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (101, 'Grupos Usuarios', '2', 'Nueva Grupos Usuarios', '2016-10-29', '05:30:17 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (102, 'Grupos Usuarios', '2', 'Nueva Grupos Usuarios', '2016-10-29', '05:55:59 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (103, '', '', 'Cerrada la Sesión', '2016-10-29', '06:00:01 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (104, '', '', 'Inicio de Sesion', '2016-10-29', '06:00:18 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (105, '', '', 'Inicio de Sesion', '2016-10-29', '06:09:11 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (106, 'Usuarios', '1', 'Se desactivo el usuario', '2016-10-29', '06:11:56 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (107, '', '', 'Cerrada la Sesión', '2016-10-29', '06:12:04 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (108, '', '', 'Inicio de Sesion', '2016-10-29', '06:12:20 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (109, '', '', 'Cerrada la Sesión', '2016-10-29', '06:20:10 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (110, '', '', 'Inicio de Sesion', '2016-10-29', '06:20:41 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (111, 'Usuarios', '1', 'Se desactivo el usuario', '2016-10-29', '06:22:42 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (112, '', '', 'Cerrada la Sesión', '2016-10-29', '06:22:54 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (113, '', '', 'Inicio de Sesion', '2016-10-29', '06:23:06 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (114, '', '', 'Cerrada la Sesión', '2016-10-29', '06:23:30 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (115, '', '', 'Inicio de Sesion', '2016-10-29', '06:24:15 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (116, '', '', 'Cerrada la Sesión', '2016-10-29', '06:25:33 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (117, '', '', 'Cerrada la Sesión', '2016-10-29', '06:39:49 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (118, '', '', 'Cerrada la Sesión', '2016-10-29', '06:47:02 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (119, '', '', 'Cerrada la Sesión', '2016-10-29', '06:50:52 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (120, '', '', 'Cerrada la Sesión', '2016-10-29', '07:11:23 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (121, '', '', 'Inicio de Sesion', '2016-10-29', '07:11:36 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (122, '', '', 'Cerrada la Sesión', '2016-10-29', '07:11:40 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (123, '', '', 'Cerrada la Sesión', '2016-10-29', '07:12:36 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (124, '', '', 'Inicio de Sesion', '2016-10-29', '07:21:20 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (125, '', '', 'Cerrada la Sesión', '2016-10-29', '07:21:27 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (126, '', '', 'Inicio de Sesion', '2016-10-29', '07:21:42 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (127, '', '', 'Cerrada la Sesión', '2016-10-29', '07:21:47 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (128, '', '', 'Inicio de Sesion', '2016-10-29', '07:35:25 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (129, '', '', 'Cerrada la Sesión', '2016-10-29', '07:35:29 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (130, '', '', 'Inicio de Sesion', '2016-10-29', '07:36:16 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (131, '', '', 'Cerrada la Sesión', '2016-10-29', '07:36:20 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (132, '', '', 'Inicio de Sesion', '2016-10-29', '07:38:28 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (133, 'Usuarios', '2', 'Se desactivo el usuario', '2016-10-29', '08:16:16 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (134, 'Usuarios', '2', 'Se desactivo el usuario', '2016-10-29', '08:16:44 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (135, '', '', 'Cerrada la Sesión', '2016-10-29', '08:16:58 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (136, '', '', 'Inicio de Sesion', '2016-10-29', '08:17:51 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (137, '', '', 'Cerrada la Sesión', '2016-10-29', '08:34:48 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (138, '', '', 'Inicio de Sesion', '2016-10-31', '03:29:34 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (139, '', '', 'Cerrada la Sesión', '2016-10-31', '03:30:04 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (140, '', '', 'Inicio de Sesion', '2016-11-03', '03:36:51 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (141, '', '', 'Cerrada la Sesión', '2016-11-03', '03:37:07 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (142, '', '', 'Inicio de Sesion', '2016-11-03', '08:13:58 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (143, '', '', 'Cerrada la Sesión', '2016-11-03', '08:14:16 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (144, '', '', 'Inicio de Sesion', '2016-11-03', '08:03:25 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (145, '', '', 'Cerrada la Sesión', '2016-11-03', '08:04:50 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (146, '', '', 'Inicio de Sesion', '2016-11-03', '08:05:38 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (147, '', '', 'Cerrada la Sesión', '2016-11-03', '08:16:04 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (148, '', '', 'Cerrada la Sesión', '2016-11-03', '08:19:54 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (149, '', '', 'Cerrada la Sesión', '2016-11-03', '08:20:14 pm', 1);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (150, '', '', 'Inicio de Sesion', '2016-11-03', '09:20:18 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (151, '', '', 'Cerrada la Sesión', '2016-11-03', '09:21:09 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (152, '', '', 'Cerrada la Sesión', '2016-11-03', '09:27:53 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (153, '', '', 'Inicio de Sesion', '2016-11-03', '09:59:14 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (154, '', '', 'Cerrada la Sesión', '2016-11-03', '09:59:46 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (155, '', '', 'Inicio de Sesion', '2016-11-04', '01:11:10 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (156, '', '', 'Cerrada la Sesión', '2016-11-04', '01:12:14 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (157, '', '', 'Inicio de Sesion', '2016-11-04', '03:52:15 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (158, '', '', 'Cerrada la Sesión', '2016-11-04', '04:04:09 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (159, '', '', 'Inicio de Sesion', '2016-11-04', '07:48:32 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (160, '', '', 'Cerrada la Sesión', '2016-11-04', '08:02:52 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (161, '', '', 'Inicio de Sesion', '2016-11-05', '09:46:22 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (162, 'Grupos Usuarios', '2', 'Nueva Grupos Usuarios', '2016-11-05', '11:07:25 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (163, 'Grupos Usuarios', '3', 'Nueva Grupos Usuarios', '2016-11-05', '11:07:54 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (164, '', '', 'Cerrada la Sesión', '2016-11-05', '11:22:28 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (165, '', '', 'Inicio de Sesion', '2016-11-05', '11:22:39 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (166, '', '', 'Cerrada la Sesión', '2016-11-05', '11:23:35 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (167, '', '', 'Inicio de Sesion', '2016-11-05', '11:23:46 am', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (168, '', '', 'Inicio de Sesion', '2016-11-05', '11:25:57 am', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (169, '', '', 'Cerrada la Sesión', '2016-11-05', '11:26:50 am', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (170, '', '', 'Inicio de Sesion', '2016-11-05', '11:26:59 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (171, '', '', 'Cerrada la Sesión', '2016-11-05', '11:27:09 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (172, '', '', 'Inicio de Sesion', '2016-11-05', '11:27:23 am', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (173, '', '', 'Cerrada la Sesión', '2016-11-05', '11:27:29 am', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (174, '', '', 'Inicio de Sesion', '2016-11-05', '11:27:48 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (175, '', '', 'Cerrada la Sesión', '2016-11-05', '11:28:22 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (176, '', '', 'Inicio de Sesion', '2016-11-05', '11:29:39 am', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (177, '', '', 'Inicio de Sesion', '2016-11-05', '01:28:37 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (178, '', '', 'Inicio de Sesion', '2016-11-05', '01:30:51 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (179, '', '', 'Cerrada la Sesión', '2016-11-05', '02:23:24 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (180, '', '', 'Inicio de Sesion', '2016-11-05', '02:23:40 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (181, '', '', 'Cerrada la Sesión', '2016-11-05', '02:33:44 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (182, '', '', 'Inicio de Sesion', '2016-11-05', '02:33:53 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (183, '', '', 'Cerrada la Sesión', '2016-11-05', '02:48:09 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (184, '', '', 'Inicio de Sesion', '2016-11-05', '02:48:17 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (185, '', '', 'Cerrada la Sesión', '2016-11-05', '04:41:29 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (186, '', '', 'Inicio de Sesion', '2016-11-07', '03:14:48 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (187, '', '', 'Cerrada la Sesión', '2016-11-07', '03:21:34 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (188, '', '', 'Inicio de Sesion', '2016-11-07', '03:21:37 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (189, '', '', 'Cerrada la Sesión', '2016-11-07', '03:22:48 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (190, '', '', 'Inicio de Sesion', '2016-11-07', '03:22:53 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (191, '', '', 'Cerrada la Sesión', '2016-11-07', '03:23:29 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (192, '', '', 'Inicio de Sesion', '2016-11-07', '03:23:34 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (193, '', '', 'Cerrada la Sesión', '2016-11-07', '03:26:33 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (194, '', '', 'Inicio de Sesion', '2016-11-07', '03:28:10 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (195, '', '', 'Cerrada la Sesión', '2016-11-07', '03:31:07 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (196, '', '', 'Inicio de Sesion', '2016-11-07', '03:31:12 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (197, '', '', 'Inicio de Sesion', '2016-11-08', '03:09:46 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (198, '', '', 'Cerrada la Sesión', '2016-11-08', '03:10:33 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (199, '', '', 'Inicio de Sesion', '2016-11-08', '03:10:41 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (200, 'RelPagos', NULL, 'Registro de Pago', '2016-11-08', '03:22:18 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (201, '', '', 'Cerrada la Sesión', '2016-11-08', '03:25:37 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (202, '', '', 'Inicio de Sesion', '2016-11-08', '03:25:42 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (203, '', '', 'Cerrada la Sesión', '2016-11-08', '03:31:46 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (204, '', '', 'Inicio de Sesion', '2016-11-08', '03:31:52 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (205, '', '', 'Cerrada la Sesión', '2016-11-08', '03:33:27 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (206, '', '', 'Inicio de Sesion', '2016-11-08', '03:33:31 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (207, '', '', 'Cerrada la Sesión', '2016-11-08', '03:34:09 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (208, '', '', 'Inicio de Sesion', '2016-11-08', '03:34:14 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (209, 'RelPagos', NULL, 'Registro de Pago', '2016-11-08', '03:34:26 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (210, '', '', 'Cerrada la Sesión', '2016-11-08', '03:36:18 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (211, '', '', 'Inicio de Sesion', '2016-11-08', '03:36:24 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (212, '', '', 'Cerrada la Sesión', '2016-11-08', '03:39:43 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (213, '', '', 'Inicio de Sesion', '2016-11-08', '03:39:49 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (214, 'RelPagos', NULL, 'Registro de Pago', '2016-11-08', '03:40:06 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (215, '', '', 'Cerrada la Sesión', '2016-11-08', '03:40:23 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (216, '', '', 'Inicio de Sesion', '2016-11-08', '03:40:27 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (217, '', '', 'Cerrada la Sesión', '2016-11-08', '03:40:52 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (218, '', '', 'Inicio de Sesion', '2016-11-08', '03:40:57 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (219, '', '', 'Cerrada la Sesión', '2016-11-08', '03:43:11 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (220, '', '', 'Inicio de Sesion', '2016-11-08', '03:43:17 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (221, '', '', 'Cerrada la Sesión', '2016-11-08', '03:56:22 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (222, '', '', 'Inicio de Sesion', '2016-11-08', '03:56:27 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (223, 'RelPagos', NULL, 'Registro de Pago', '2016-11-08', '03:56:39 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (224, 'RelPagos', NULL, 'Registro de Pago', '2016-11-08', '03:58:06 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (225, 'RelPagos', NULL, 'Registro de Pago', '2016-11-08', '03:58:14 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (226, 'RelPagos', NULL, 'Registro de Pago', '2016-11-08', '04:00:27 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (227, '', '', 'Cerrada la Sesión', '2016-11-08', '04:00:33 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (228, '', '', 'Inicio de Sesion', '2016-11-08', '04:00:40 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (229, '', '', 'Cerrada la Sesión', '2016-11-08', '04:06:37 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (230, '', '', 'Inicio de Sesion', '2016-11-08', '04:06:43 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (231, '', '', 'Cerrada la Sesión', '2016-11-08', '04:07:56 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (232, '', '', 'Inicio de Sesion', '2016-11-08', '04:08:15 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (233, '', '', 'Cerrada la Sesión', '2016-11-08', '04:08:49 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (234, '', '', 'Inicio de Sesion', '2016-11-08', '04:10:37 am', 10);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (235, 'RelPagos', NULL, 'Registro de Pago', '2016-11-08', '04:11:06 am', 10);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (236, '', '', 'Cerrada la Sesión', '2016-11-08', '04:11:36 am', 10);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (237, '', '', 'Inicio de Sesion', '2016-11-08', '04:11:46 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (238, '', '', 'Cerrada la Sesión', '2016-11-08', '04:12:10 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (239, '', '', 'Inicio de Sesion', '2016-11-08', '04:12:18 am', 10);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (240, 'Perfiles', '19124123', 'Editar perfil de usuario', '2016-11-08', '04:13:01 am', 10);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (241, '', '', 'Cerrada la Sesión', '2016-11-08', '04:24:40 am', 10);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (242, '', '', 'Inicio de Sesion', '2016-11-08', '04:24:48 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (243, '', '', 'Inicio de Sesion', '2016-11-08', '04:27:16 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (244, 'Empresa', '1', 'Editar Empresa', '2016-11-08', '04:37:25 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (245, '', '', 'Cerrada la Sesión', '2016-11-08', '04:42:16 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (246, '', '', 'Inicio de Sesion', '2016-11-08', '04:42:22 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (247, '', '', 'Cerrada la Sesión', '2016-11-08', '04:44:48 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (248, '', '', 'Inicio de Sesion', '2016-11-08', '08:50:21 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (249, '', '', 'Cerrada la Sesión', '2016-11-08', '08:52:02 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (250, '', '', 'Inicio de Sesion', '2016-11-08', '08:53:08 am', 11);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (251, 'RelPagos', NULL, 'Registro de Pago', '2016-11-08', '08:55:53 am', 11);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (252, '', '', 'Cerrada la Sesión', '2016-11-08', '08:56:43 am', 11);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (253, '', '', 'Inicio de Sesion', '2016-11-08', '08:56:50 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (254, '', '', 'Cerrada la Sesión', '2016-11-08', '08:57:26 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (255, '', '', 'Inicio de Sesion', '2016-11-08', '08:57:35 am', 11);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (256, 'Perfiles', '19144489', 'Editar perfil de usuario', '2016-11-08', '08:58:38 am', 11);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (257, '', '', 'Cerrada la Sesión', '2016-11-08', '09:04:31 am', 11);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (258, '', '', 'Inicio de Sesion', '2016-11-08', '09:17:53 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (259, '', '', 'Cerrada la Sesión', '2016-11-08', '09:18:27 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (260, '', '', 'Inicio de Sesion', '2016-11-08', '09:19:28 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (261, '', '', 'Cerrada la Sesión', '2016-11-08', '09:30:30 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (262, '', '', 'Inicio de Sesion', '2016-11-08', '10:11:04 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (263, 'RelLinks', '9', 'Nueva Links de invitación', '2016-11-08', '01:15:24 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (264, 'RelLinks', '9', 'Nueva Links de invitación', '2016-11-08', '01:24:07 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (265, 'Perfiles', '19363480', 'Editar perfil de usuario', '2016-11-08', '01:28:58 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (266, 'RelLinks', '9', 'Nueva Links de invitación', '2016-11-08', '01:30:42 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (267, 'Perfiles', '19363480', 'Editar perfil de usuario', '2016-11-08', '01:35:38 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (268, '', '', 'Cerrada la Sesión', '2016-11-08', '01:36:47 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (269, '', '', 'Inicio de Sesion', '2016-11-08', '01:36:57 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (270, '', '', 'Cerrada la Sesión', '2016-11-08', '01:41:35 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (271, '', '', 'Inicio de Sesion', '2016-11-08', '01:41:41 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (272, 'Perfiles', '18993867', 'Editar perfil de usuario', '2016-11-08', '01:42:27 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (273, 'RelLinks', '3', 'Nueva Links de invitación', '2016-11-08', '01:42:36 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (274, '', '', 'Cerrada la Sesión', '2016-11-08', '01:58:08 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (275, '', '', 'Inicio de Sesion', '2016-11-08', '01:58:14 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (276, '', '', 'Cerrada la Sesión', '2016-11-08', '02:26:57 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (277, '', '', 'Inicio de Sesion', '2016-11-08', '02:27:03 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (278, '', '', 'Cerrada la Sesión', '2016-11-08', '02:38:09 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (279, '', '', 'Inicio de Sesion', '2016-11-08', '02:38:18 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (280, 'TiposMonedas', '4', 'Nueva TiposMonedas', '2016-11-08', '02:38:35 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (281, 'TiposMonedas', '2', 'Editar TiposMonedas', '2016-11-08', '02:39:31 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (282, 'TiposMonedas', '4', 'Eliminar TiposMonedas', '2016-11-08', '02:40:21 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (283, 'TiposMonedas', '3', 'Editar TiposMonedas', '2016-11-08', '02:40:47 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (284, '', '', 'Cerrada la Sesión', '2016-11-08', '02:44:01 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (285, '', '', 'Inicio de Sesion', '2016-11-08', '02:44:07 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (286, 'RelLinks', '9', 'Nueva Links de invitación', '2016-11-08', '02:44:14 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (287, '', '', 'Cerrada la Sesión', '2016-11-08', '03:15:12 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (288, '', '', 'Cerrada la Sesión', '2016-11-08', '03:16:49 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (289, '', '', 'Inicio de Sesion', '2016-11-08', '03:16:54 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (290, '', '', 'Inicio de Sesion', '2016-11-09', '08:48:04 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (291, '', '', 'Cerrada la Sesión', '2016-11-09', '08:53:47 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (292, '', '', 'Inicio de Sesion', '2016-11-09', '08:53:55 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (293, '', '', 'Cerrada la Sesión', '2016-11-09', '08:54:07 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (294, '', '', 'Inicio de Sesion', '2016-11-09', '08:54:12 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (295, 'RelLinks', '3', 'Nueva Links de invitación', '2016-11-09', '09:13:20 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (296, '', '', 'Cerrada la Sesión', '2016-11-09', '11:30:29 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (297, '', '', 'Inicio de Sesion', '2016-11-09', '11:30:46 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (298, '', '', 'Cerrada la Sesión', '2016-11-09', '11:32:32 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (299, '', '', 'Inicio de Sesion', '2016-11-09', '11:33:02 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (300, 'RelPagos', NULL, 'Registro de Pago', '2016-11-09', '11:39:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (301, '', '', 'Cerrada la Sesión', '2016-11-09', '11:39:41 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (302, '', '', 'Inicio de Sesion', '2016-11-09', '11:39:47 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (303, '', '', 'Cerrada la Sesión', '2016-11-09', '11:41:07 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (304, '', '', 'Inicio de Sesion', '2016-11-09', '11:41:15 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (305, 'Perfiles', '19404568', 'Editar perfil de usuario', '2016-11-09', '01:09:24 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (306, '', '', 'Cerrada la Sesión', '2016-11-09', '01:09:48 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (307, '', '', 'Inicio de Sesion', '2016-11-09', '01:10:08 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (308, '', '', 'Cerrada la Sesión', '2016-11-09', '01:10:17 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (309, '', '', 'Inicio de Sesion', '2016-11-09', '01:12:14 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (310, '', '', 'Inicio de Sesion', '2016-11-09', '01:12:53 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (311, 'RelLinks', '12', 'Nueva Links de invitación', '2016-11-09', '01:19:09 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (312, '', '', 'Cerrada la Sesión', '2016-11-09', '01:24:51 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (313, '', '', 'Inicio de Sesion', '2016-11-09', '01:25:18 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (314, '', '', 'Cerrada la Sesión', '2016-11-09', '01:25:34 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (315, '', '', 'Inicio de Sesion', '2016-11-09', '01:25:58 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (316, 'RelPagos', NULL, 'Registro de Pago', '2016-11-09', '01:27:06 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (317, '', '', 'Cerrada la Sesión', '2016-11-09', '01:32:04 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (318, '', '', 'Inicio de Sesion', '2016-11-09', '01:32:17 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (319, '', '', 'Cerrada la Sesión', '2016-11-09', '01:32:41 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (320, '', '', 'Inicio de Sesion', '2016-11-09', '01:32:46 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (321, '', '', 'Cerrada la Sesión', '2016-11-09', '01:49:31 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (322, '', '', 'Inicio de Sesion', '2016-11-09', '01:49:37 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (323, 'Perfiles', '18456684', 'Editar perfil de usuario', '2016-11-09', '01:51:08 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (324, 'RelLinks', '13', 'Nueva Links de invitación', '2016-11-09', '01:51:14 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (325, '', '', 'Cerrada la Sesión', '2016-11-09', '01:51:18 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (326, '', '', 'Inicio de Sesion', '2016-11-09', '01:51:23 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (327, '', '', 'Cerrada la Sesión', '2016-11-09', '02:19:19 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (328, '', '', 'Inicio de Sesion', '2016-11-09', '02:19:25 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (329, '', '', 'Cerrada la Sesión', '2016-11-09', '03:27:26 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (330, '', '', 'Inicio de Sesion', '2016-11-09', '03:27:32 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (331, '', '', 'Inicio de Sesion', '2016-11-10', '08:14:56 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (332, '', '', 'Cerrada la Sesión', '2016-11-10', '08:18:17 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (333, '', '', 'Cerrada la Sesión', '2016-11-10', '08:18:33 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (334, '', '', 'Inicio de Sesion', '2016-11-10', '08:18:40 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (335, '', '', 'Cerrada la Sesión', '2016-11-10', '10:21:36 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (336, '', '', 'Inicio de Sesion', '2016-11-10', '10:22:01 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (337, '', '', 'Inicio de Sesion', '2016-11-10', '10:22:10 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (338, '', '', 'Cerrada la Sesión', '2016-11-10', '10:22:13 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (339, '', '', 'Inicio de Sesion', '2016-11-10', '10:22:32 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (340, '', '', 'Cerrada la Sesión', '2016-11-10', '10:32:45 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (341, '', '', 'Inicio de Sesion', '2016-11-10', '10:32:49 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (342, '', '', 'Cerrada la Sesión', '2016-11-10', '10:34:16 am', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (343, '', '', 'Inicio de Sesion', '2016-11-10', '10:34:25 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (344, '', '', 'Cerrada la Sesión', '2016-11-10', '10:34:42 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (345, '', '', 'Inicio de Sesion', '2016-11-10', '10:34:46 am', 11);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (346, '', '', 'Cerrada la Sesión', '2016-11-10', '11:16:27 am', 11);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (347, '', '', 'Inicio de Sesion', '2016-11-10', '01:33:06 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (348, '', '', 'Cerrada la Sesión', '2016-11-10', '02:00:59 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (349, '', '', 'Inicio de Sesion', '2016-11-10', '02:01:04 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (350, '', '', 'Cerrada la Sesión', '2016-11-10', '02:06:04 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (351, '', '', 'Inicio de Sesion', '2016-11-10', '02:06:16 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (352, '', '', 'Cerrada la Sesión', '2016-11-10', '02:25:40 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (353, '', '', 'Inicio de Sesion', '2016-11-10', '02:25:44 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (354, '', '', 'Cerrada la Sesión', '2016-11-10', '02:52:56 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (355, '', '', 'Inicio de Sesion', '2016-11-21', '09:40:44 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (356, '', '', 'Inicio de Sesion', '2016-11-21', '09:42:35 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (357, '', '', 'Cerrada la Sesión', '2016-11-21', '09:48:51 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (358, '', '', 'Inicio de Sesion', '2016-11-21', '09:48:58 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (359, 'Rel Distribución', '2', 'Nueva Pago de distribucion de capital', '2016-11-21', '10:53:36 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (360, 'Rel Distribución', '3', 'Nueva Pago de distribucion de capital', '2016-11-21', '11:12:13 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (361, 'Rel Distribución', '4', 'Nueva Pago de distribucion de capital', '2016-11-21', '11:20:48 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (362, 'Rel Distribución', '5', 'Nueva Pago de distribucion de capital', '2016-11-21', '11:21:49 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (363, 'Rel Distribución', '6', 'Nueva Pago de distribucion de capital', '2016-11-21', '11:22:02 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (364, 'Rel Distribución', '7', 'Nueva Pago de distribucion de capital', '2016-11-21', '11:52:10 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (365, '', '', 'Cerrada la Sesión', '2016-11-21', '11:54:53 am', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (366, '', '', 'Inicio de Sesion', '2016-11-21', '01:12:49 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (367, '', '', 'Cerrada la Sesión', '2016-11-21', '01:13:00 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (368, '', '', 'Inicio de Sesion', '2016-11-21', '01:13:05 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (369, '', '', 'Inicio de Sesion', '2016-11-21', '01:45:46 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (370, 'Cargo Mora', '1', 'Editar Cargo Mora', '2016-11-21', '01:57:46 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (371, 'Monto Pago', '1', 'Editar Monto Pago', '2016-11-21', '01:58:36 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (372, '', '', 'Cerrada la Sesión', '2016-11-21', '02:08:39 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (373, '', '', 'Inicio de Sesion', '2016-11-21', '02:08:45 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (374, '', '', 'Cerrada la Sesión', '2016-11-21', '03:30:18 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (375, '', '', 'Cerrada la Sesión', '2016-11-23', '01:31:13 pm', 4);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (376, '', '', 'Inicio de Sesion', '2016-11-23', '01:31:30 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (377, '', '', 'Cerrada la Sesión', '2016-11-23', '03:29:11 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (378, '', '', 'Inicio de Sesion', '2016-11-23', '03:29:15 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (379, '', '', 'Inicio de Sesion', '2016-11-23', '09:36:45 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (380, 'Rel Distribución', '8', 'Nueva Pago de distribucion de capital', '2016-11-23', '10:13:32 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (381, '', '', 'Cerrada la Sesión', '2016-11-23', '10:13:40 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (382, '', '', 'Inicio de Sesion', '2016-11-23', '10:13:49 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (383, '', '', 'Cerrada la Sesión', '2016-11-23', '10:14:05 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (384, '', '', 'Inicio de Sesion', '2016-11-23', '10:14:12 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (385, 'Rel Distribución', '9', 'Nueva Pago de distribucion de capital', '2016-11-23', '10:14:54 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (386, 'Rel Distribución', '10', 'Nueva Pago de distribucion de capital', '2016-11-23', '10:18:52 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (387, 'Rel Distribución', '11', 'Nueva Pago de distribucion de capital', '2016-11-23', '10:18:56 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (388, '', '', 'Cerrada la Sesión', '2016-11-23', '10:25:49 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (389, '', '', 'Inicio de Sesion', '2016-11-23', '10:25:56 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (390, '', '', 'Cerrada la Sesión', '2016-11-23', '11:27:24 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (391, '', '', 'Inicio de Sesion', '2016-11-23', '11:27:29 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (392, '', '', 'Cerrada la Sesión', '2016-11-23', '11:27:48 pm', 9);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (393, '', '', 'Inicio de Sesion', '2016-11-23', '11:28:00 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (394, '', '', 'Cerrada la Sesión', '2016-11-23', '11:29:40 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (395, '', '', 'Inicio de Sesion', '2016-11-23', '11:30:12 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (396, 'RelPagos', NULL, 'Registro de Pago', '2016-11-23', '11:39:24 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (397, '', '', 'Cerrada la Sesión', '2016-11-23', '11:41:34 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (398, '', '', 'Inicio de Sesion', '2016-11-23', '11:41:38 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (399, '', '', 'Cerrada la Sesión', '2016-11-23', '11:41:54 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (400, '', '', 'Inicio de Sesion', '2016-11-23', '11:42:07 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (401, 'Perfiles', '18784848', 'Editar perfil de usuario', '2016-11-23', '11:43:00 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (402, 'Rel Distribución', '12', 'Nueva Pago de distribucion de capital', '2016-11-23', '11:47:25 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (403, 'Rel Distribución', '13', 'Nueva Pago de distribucion de capital', '2016-11-23', '11:47:30 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (404, 'Rel Distribución', '14', 'Nueva Pago de distribucion de capital', '2016-11-23', '11:47:34 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (405, 'Rel Distribución', '15', 'Nueva Pago de distribucion de capital', '2016-11-23', '11:47:39 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (406, 'Rel Distribución', '16', 'Nueva Pago de distribucion de capital', '2016-11-23', '11:47:43 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (407, '', '', 'Cerrada la Sesión', '2016-11-23', '11:59:50 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (408, '', '', 'Inicio de Sesion', '2016-11-23', '11:59:57 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (409, '', '', 'Cerrada la Sesión', '2016-11-24', '02:52:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (410, '', '', 'Inicio de Sesion', '2016-11-24', '02:52:42 am', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (411, 'RelLinks', '14', 'Nueva Links de invitación', '2016-11-24', '02:53:46 am', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (412, '', '', 'Cerrada la Sesión', '2016-11-24', '02:55:31 am', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (413, '', '', 'Inicio de Sesion', '2016-11-24', '02:55:58 am', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (414, 'RelPagos', NULL, 'Registro de Pago', '2016-11-24', '02:57:06 am', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (415, '', '', 'Cerrada la Sesión', '2016-11-24', '02:57:11 am', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (416, '', '', 'Inicio de Sesion', '2016-11-24', '02:57:15 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (417, '', '', 'Cerrada la Sesión', '2016-11-24', '02:57:26 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (418, '', '', 'Inicio de Sesion', '2016-11-24', '02:57:31 am', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (419, 'Perfiles', '17884984', 'Editar perfil de usuario', '2016-11-24', '03:00:33 am', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (420, 'Rel Distribución', '17', 'Nueva Pago de distribucion de capital', '2016-11-24', '03:06:57 am', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (421, 'Rel Distribución', '18', 'Nueva Pago de distribucion de capital', '2016-11-24', '03:07:03 am', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (422, 'Rel Distribución', '19', 'Nueva Pago de distribucion de capital', '2016-11-24', '03:07:14 am', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (423, '', '', 'Cerrada la Sesión', '2016-11-24', '03:07:20 am', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (424, '', '', 'Inicio de Sesion', '2016-11-24', '03:07:24 am', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (425, '', '', 'Cerrada la Sesión', '2016-11-24', '03:15:25 am', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (426, '', '', 'Inicio de Sesion', '2016-11-24', '03:15:30 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (427, '', '', 'Cerrada la Sesión', '2016-11-24', '03:15:52 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (428, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:31 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (429, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (430, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (431, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (432, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (433, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (434, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (435, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (436, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (437, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (438, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (439, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (440, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (441, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (442, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (443, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (444, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (445, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (446, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (447, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (448, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (449, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (450, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (451, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (452, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (453, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:32 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (454, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (455, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (456, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (457, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (458, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (459, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (460, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (461, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (462, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (463, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (464, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (465, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (466, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (467, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (468, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (469, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (470, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (471, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (472, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (473, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (474, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (475, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (476, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (477, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (478, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (479, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (480, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (481, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (482, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (483, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:33 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (484, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (485, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (486, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (487, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (488, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (489, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (490, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (491, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (492, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (493, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (494, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (495, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (496, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (497, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (498, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (499, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (500, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (501, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (502, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (503, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (504, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (505, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (506, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (507, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (508, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (509, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (510, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (511, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (512, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (513, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:34 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (514, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:35 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (515, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:35 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (516, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:35 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (517, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:35 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (518, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:35 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (519, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:35 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (520, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:35 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (521, '', '', 'Cerrada la Sesión', '2016-11-24', '03:16:35 am', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (522, '', '', 'Inicio de Sesion', '2016-11-24', '10:10:21 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (523, 'Monto de Retiro Minimo', '1', 'Editar Monto de Retiro Minimo', '2016-11-24', '10:28:59 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (524, '', '', 'Cerrada la Sesión', '2016-11-24', '10:35:09 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (525, '', '', 'Inicio de Sesion', '2016-11-24', '10:35:17 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (526, '', '', 'Cerrada la Sesión', '2016-11-24', '10:59:07 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (527, '', '', 'Inicio de Sesion', '2016-11-24', '10:59:11 pm', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (528, '', '', 'Cerrada la Sesión', '2016-11-24', '11:07:14 pm', 15);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (529, '', '', 'Inicio de Sesion', '2016-11-24', '11:07:21 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (530, '', '', 'Cerrada la Sesión', '2016-11-24', '11:07:30 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (531, '', '', 'Inicio de Sesion', '2016-11-24', '11:07:34 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (532, 'RelPagos', NULL, 'Nueva RelPagos', '2016-11-24', '11:07:54 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (533, '', '', 'Cerrada la Sesión', '2016-11-24', '11:08:05 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (534, '', '', 'Inicio de Sesion', '2016-11-24', '11:08:09 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (535, '', '', 'Cerrada la Sesión', '2016-11-24', '11:08:37 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (536, '', '', 'Inicio de Sesion', '2016-11-24', '11:08:50 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (537, '', '', 'Cerrada la Sesión', '2016-11-24', '11:22:43 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (538, '', '', 'Inicio de Sesion', '2016-11-24', '11:22:50 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (539, '', '', 'Cerrada la Sesión', '2016-11-24', '11:22:56 pm', 14);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (540, '', '', 'Inicio de Sesion', '2016-11-24', '11:23:04 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (541, '', '', 'Cerrada la Sesión', '2016-11-24', '11:26:08 pm', 12);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (542, '', '', 'Inicio de Sesion', '2016-11-24', '11:26:29 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (543, '', '', 'Inicio de Sesion', '2016-11-25', '05:47:53 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (544, '', '', 'Cerrada la Sesión', '2016-11-25', '06:01:06 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (545, '', '', 'Inicio de Sesion', '2016-11-25', '06:01:14 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (546, 'RelPagos', NULL, 'Nueva RelPagos', '2016-11-25', '06:09:55 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (547, 'RelPagos', NULL, 'Nueva RelPagos', '2016-11-25', '06:15:40 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (548, '', '', 'Cerrada la Sesión', '2016-11-25', '06:15:56 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (549, 'Cargo Mora', '1', 'Editar Cargo Mora', '2016-11-25', '05:43:42 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (550, 'Comision Retiro', '1', 'Editar Comision Retiro', '2016-11-25', '05:43:58 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (551, 'ref_perfil', '1', 'Actualizar Perfil', '2016-11-25', '05:57:21 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (552, 'ref_rel_retiros', '1', 'Aprobar Retiro', '2016-11-25', '05:59:31 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (553, '', '', 'Cerrada la Sesión', '2016-11-25', '06:51:34 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (554, '', '', 'Inicio de Sesion', '2016-11-25', '06:51:48 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (555, '', '', 'Cerrada la Sesión', '2016-11-25', '06:54:26 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (556, '', '', 'Inicio de Sesion', '2016-11-25', '06:54:33 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (557, '', '', 'Cerrada la Sesión', '2016-11-25', '06:54:53 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (558, '', '', 'Inicio de Sesion', '2016-11-25', '06:54:59 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (559, '', '', 'Cerrada la Sesión', '2016-11-25', '07:03:01 pm', 13);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (560, '', '', 'Inicio de Sesion', '2016-11-25', '07:03:09 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (561, '', '', 'Inicio de Sesion', '2016-11-29', '08:41:05 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (562, '', '', 'Cerrada la Sesión', '2016-11-29', '10:47:05 pm', 6);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (563, '', '', 'Inicio de Sesion', '2017-06-07', '07:32:34 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (564, '', '', 'Cerrada la Sesión', '2017-06-07', '07:33:40 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (565, '', '', 'Inicio de Sesion', '2017-06-07', '07:33:49 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (566, '', '', 'Cerrada la Sesión', '2017-06-07', '07:36:12 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (567, '', '', 'Inicio de Sesion', '2017-06-07', '07:43:54 pm', 16);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (568, '', '', 'Cerrada la Sesión', '2017-06-07', '07:55:00 pm', 16);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (569, '', '', 'Inicio de Sesion', '2017-11-25', '04:15:14 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (570, '', '', 'Cerrada la Sesión', '2017-11-25', '04:23:17 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (571, '', '', 'Inicio de Sesion', '2017-11-25', '04:23:27 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (572, '', '', 'Cerrada la Sesión', '2017-11-25', '04:26:50 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (573, '', '', 'Inicio de Sesion', '2017-11-25', '04:27:04 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (574, '', '', 'Inicio de Sesion', '2017-11-25', '04:31:18 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (575, '', '', 'Cerrada la Sesión', '2017-11-25', '04:44:08 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (576, '', '', 'Cerrada la Sesión', '2017-11-25', '04:45:01 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (577, '', '', 'Inicio de Sesion', '2017-11-25', '04:45:08 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (578, '', '', 'Cerrada la Sesión', '2017-11-25', '04:51:35 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (579, '', '', 'Inicio de Sesion', '2017-11-25', '05:49:39 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (580, '', '', 'Cerrada la Sesión', '2017-11-25', '05:51:08 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (581, '', '', 'Inicio de Sesion', '2017-11-25', '05:51:18 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (582, '', '', 'Inicio de Sesion', '2017-11-25', '06:00:32 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (583, '', '', 'Cerrada la Sesión', '2017-11-25', '06:02:02 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (584, '', '', 'Inicio de Sesion', '2017-11-25', '06:02:14 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (585, '', '', 'Inicio de Sesion', '2017-11-25', '07:04:00 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (586, '', '', 'Cerrada la Sesión', '2017-11-25', '07:12:38 pm', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (587, '', '', 'Inicio de Sesion', '2017-11-25', '07:12:49 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (588, '', '', 'Inicio de Sesion', '2017-11-25', '09:08:08 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (589, 'Usuarios', '1', 'Registro de nuevo Usuario', '2017-11-25', '09:28:51 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (590, 'Candidatos', NULL, 'Registro de nuevo Candidato: DIANA DIANA', '2017-11-25', '09:55:30 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (591, 'Candidatos', NULL, 'Registro de nuevo Candidato: DIANA LUNA', '2017-11-25', '10:04:24 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (592, 'Candidatos', NULL, 'Registro de nuevo Candidato: GOKU KAKAROTO', '2017-11-25', '10:06:54 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (593, 'Candidatos', NULL, 'Eliminación de Candidato: ', '2017-11-25', '10:38:10 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (594, 'Candidatos', NULL, 'Eliminación de Candidato: AYAME ASAKURA', '2017-11-25', '10:47:17 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (595, 'Candidatos', NULL, 'Registro de nuevo Candidato: AYAME ASAKURA', '2017-11-25', '10:48:04 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (596, 'Candidatos', NULL, 'Eliminación de Candidato: AYAME ASAKURA', '2017-11-25', '10:49:16 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (597, 'Candidatos', NULL, 'Registro de nuevo Candidato: AYAME ASAKURA', '2017-11-25', '10:50:07 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (598, 'Candidatos', NULL, 'Eliminación de Candidato: AYAME ASAKURA', '2017-11-25', '10:51:05 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (599, 'Candidatos', NULL, 'Registro de nuevo Candidato: AYAME ASAKURA', '2017-11-25', '10:53:22 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (600, 'Candidatos', NULL, 'Eliminación de Candidato: AYAME ASAKURA', '2017-11-25', '10:54:31 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (601, 'Candidatos', NULL, 'Registro de nuevo Candidato: AYAME ASAKURA', '2017-11-25', '10:55:11 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (602, 'Candidatos', NULL, 'Edición de Candidato: AYAME ASAKURA', '2017-11-25', '11:28:26 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (603, 'Candidatos', NULL, 'Edición de Candidato: AYAME ASAKURA', '2017-11-25', '11:30:11 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (604, 'Candidatos', NULL, 'Edición de Candidato: AYAME ASAKURA', '2017-11-25', '11:33:54 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (605, 'Candidatos', NULL, 'Edición de Candidato: AYAME ASAKURA', '2017-11-25', '11:35:09 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (606, 'Candidatos', NULL, 'Edición de Candidato: AYAME ASAKURA', '2017-11-25', '11:36:14 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (607, '', '', 'Cerrada la Sesión', '2017-11-26', '02:46:47 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (608, '', '', 'Inicio de Sesion', '2017-11-26', '02:47:01 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (609, 'Eleccion', NULL, 'Registro de nueva Elección: ELECCIÓN 1', '2017-11-26', '02:52:27 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (610, 'Elección', NULL, 'Edición de la Elección: ELECCIÓN 1', '2017-11-26', '03:58:13 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (611, 'Elección', NULL, 'Edición de la Elección: ELECCIÓN 1', '2017-11-26', '04:00:28 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (612, 'Elección', NULL, 'Edición de la Elección: ELECCIÓN 1', '2017-11-26', '04:01:20 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (613, 'Elección', NULL, 'Edición de la Elección: ELECCIÓN 1', '2017-11-26', '04:01:45 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (614, 'Eleccion', NULL, 'Eliminación de Elección: ELECCIÓN 1', '2017-11-26', '04:02:20 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (615, 'Eleccion', NULL, 'Registro de nueva Elección: ELECCIÓN 1', '2017-11-26', '04:03:24 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (616, 'Eleccion', NULL, 'Eliminación de Elección: ELECCIÓN 1', '2017-11-26', '04:03:41 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (617, 'Eleccion', NULL, 'Registro de nueva Elección: ELECCIÓN 1', '2017-11-26', '04:08:10 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (618, 'Elección', NULL, 'Edición de la Elección: ELECCIÓN 1', '2017-11-26', '04:15:03 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (619, 'Eleccion', NULL, 'Registro de nueva Elección: FDGDGD', '2017-11-26', '04:19:06 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (620, 'Eleccion', NULL, 'Registro de nueva Elección: ELECCIÓN 1', '2017-11-26', '04:20:33 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (621, 'Elección', NULL, 'Edición de la Elección: ELECCIÓN 1', '2017-11-26', '04:21:20 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (622, 'Elección', NULL, 'Edición de la Elección: ELECCIÓN 1', '2017-11-26', '04:22:18 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (623, 'Elección', NULL, 'Edición de la Elección: ELECCIÓN 1', '2017-11-26', '04:22:59 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (624, 'Eleccion', NULL, 'Registro de nueva Elección: ELECCIÓN 1', '2017-11-26', '04:23:43 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (625, 'Eleccion', NULL, 'Eliminación de Elección: ELECCIÓN 1', '2017-11-26', '04:24:06 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (626, 'Eleccion', NULL, 'Registro de nueva Elección: ELECCIONES DE ALCALDES', '2017-11-26', '04:24:48 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (627, '', '', 'Cerrada la Sesión', '2017-11-26', '04:39:48 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (628, '', '', 'Inicio de Sesion', '2017-11-26', '04:39:57 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (629, '', '', 'Inicio de Sesion', '2017-11-26', '07:54:57 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (630, 'Elección', NULL, 'Edición de la Elección: ELECCIONES DE ALCALDES', '2017-11-26', '07:59:55 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (631, 'Elección', NULL, 'Edición de la Elección: ELECCIONES DE ALCALDES', '2017-11-26', '08:00:18 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (632, 'Elección', NULL, 'Edición de la Elección: ELECCIONES DE ALCALDES', '2017-11-26', '08:10:22 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (633, 'Elección', NULL, 'Edición de la Elección: ELECCIONES DE ALCALDES', '2017-11-26', '08:11:06 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (634, 'Elección', NULL, 'Edición de la Elección: ELECCIONES DE ALCALDES', '2017-11-26', '08:24:20 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (635, 'Elección', NULL, 'Edición de la Elección: ELECCIONES DE ALCALDES', '2017-11-26', '08:24:47 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (636, 'Elección', NULL, 'Edición de la Elección: ELECCIONES DE ALCALDES', '2017-11-26', '08:26:06 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (637, 'Elección', NULL, 'Edición de la Elección: ELECCIONES DE ALCALDES', '2017-11-26', '08:26:49 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (638, '', '', 'Cerrada la Sesión', '2017-11-26', '08:27:13 am', 3);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (639, '', '', 'Inicio de Sesion', '2017-11-27', '03:23:28 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (640, '', '', 'Cerrada la Sesión', '2017-11-27', '04:09:40 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (641, '', '', 'Inicio de Sesion', '2017-11-27', '04:09:43 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (642, '', '', 'Inicio de Sesion', '2017-11-27', '04:12:59 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (643, 'Candidatos', NULL, 'Registro de nuevo Candidato: MARCEL ARCURI', '2017-11-27', '04:44:25 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (644, 'Eleccion', NULL, 'Registro de nueva Elección: PRUEBA', '2017-11-27', '05:10:39 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (645, 'Elección', NULL, 'Edición de la Elección: PRUEBAS', '2017-11-27', '05:18:42 pm', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (646, '', '', 'Inicio de Sesion', '2017-11-28', '01:58:25 am', 2);
INSERT INTO auditoria (id, tabla, codigo, accion, fecha, hora, usuario) VALUES (647, '', '', 'Inicio de Sesion', '2017-11-28', '02:03:05 am', 2);


--
-- TOC entry 2158 (class 0 OID 0)
-- Dependencies: 176
-- Name: auditoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auditoria_id_seq', 647, true);


--
-- TOC entry 2159 (class 0 OID 0)
-- Dependencies: 178
-- Name: candidato_eleccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('candidato_eleccion_id_seq', 11, true);


--
-- TOC entry 2160 (class 0 OID 0)
-- Dependencies: 180
-- Name: candidato_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('candidato_id_seq', 1, false);


--
-- TOC entry 2116 (class 0 OID 1151749)
-- Dependencies: 181
-- Data for Name: claves_sistema; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO claves_sistema (id, clave, fecha, hora, user_create) VALUES (1, 'pbkdf2_sha256$12000$ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', '2016-10-29', '18:45:00 pm', 4);


--
-- TOC entry 2161 (class 0 OID 0)
-- Dependencies: 182
-- Name: claves_sistema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('claves_sistema_id_seq', 1, true);


--
-- TOC entry 2118 (class 0 OID 1151754)
-- Dependencies: 183
-- Data for Name: conf_grupo_user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO conf_grupo_user (id, codigo, name, activo, user_create, date_create, user_update, date_update) VALUES (1, 1, 'Administrador', true, NULL, NULL, NULL, NULL);
INSERT INTO conf_grupo_user (id, codigo, name, activo, user_create, date_create, user_update, date_update) VALUES (2, 2, 'OPERADOR', true, 2, NULL, NULL, NULL);
INSERT INTO conf_grupo_user (id, codigo, name, activo, user_create, date_create, user_update, date_update) VALUES (3, 3, 'BÁSICO', true, 2, NULL, NULL, NULL);


--
-- TOC entry 2162 (class 0 OID 0)
-- Dependencies: 184
-- Name: conf_grupo_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('conf_grupo_user_id_seq', 1, true);


--
-- TOC entry 2163 (class 0 OID 0)
-- Dependencies: 186
-- Name: eleccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('eleccion_id_seq', 1, false);


--
-- TOC entry 2114 (class 0 OID 1151744)
-- Dependencies: 179
-- Data for Name: reg_candidato; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO reg_candidato (id, codigo, nombre, apellido, foto, activo, user_create, date_create, user_update, date_update, foto_binario) VALUES (1, NULL, 'DIANA', 'LUNA', 'foto1_1jpg', NULL, 2, '2017-11-25 22:04:23', NULL, NULL, NULL);
INSERT INTO reg_candidato (id, codigo, nombre, apellido, foto, activo, user_create, date_create, user_update, date_update, foto_binario) VALUES (2, NULL, 'AXEL', 'KAISER', 'foto1_2jpg', NULL, 2, '2017-11-25 22:06:54', NULL, NULL, NULL);
INSERT INTO reg_candidato (id, codigo, nombre, apellido, foto, activo, user_create, date_create, user_update, date_update, foto_binario) VALUES (3, NULL, 'AYAME', 'KOBAYASHI', 'foto1_3jpg', NULL, 2, '2017-11-25 22:55:11', 2, '2017-11-25 23:36:14', NULL);
INSERT INTO reg_candidato (id, codigo, nombre, apellido, foto, activo, user_create, date_create, user_update, date_update, foto_binario) VALUES (4, NULL, 'MARCEL', 'ARCURI', 'foto1_4jpg', NULL, 2, '2017-11-27 16:44:25', NULL, NULL, NULL);


--
-- TOC entry 2120 (class 0 OID 1151759)
-- Dependencies: 185
-- Data for Name: reg_eleccion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO reg_eleccion (id, codigo, nombre, estado_id, municipio_id, user_create, date_create, user_update, date_update) VALUES (1, NULL, 'ELECCIONES DE ALCALDES', 3, 1, 3, '2017-11-26 04:24:48', 3, '2017-11-26 08:26:49');
INSERT INTO reg_eleccion (id, codigo, nombre, estado_id, municipio_id, user_create, date_create, user_update, date_update) VALUES (2, NULL, 'PRUEBAS', 5, 51500, 2, '2017-11-27 17:10:39', 2, '2017-11-27 17:18:42');


--
-- TOC entry 2112 (class 0 OID 1151739)
-- Dependencies: 177
-- Data for Name: rel_candidato_eleccion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO rel_candidato_eleccion (id, eleccion_id, candidato_id, user_create, date_create, user_update, date_update) VALUES (5, 1, 2, 3, '2017-11-26 04:24:48', NULL, NULL);
INSERT INTO rel_candidato_eleccion (id, eleccion_id, candidato_id, user_create, date_create, user_update, date_update) VALUES (6, 1, 3, 3, '2017-11-26 04:24:48', NULL, NULL);
INSERT INTO rel_candidato_eleccion (id, eleccion_id, candidato_id, user_create, date_create, user_update, date_update) VALUES (10, 1, 1, 3, '2017-11-26 08:26:49', NULL, NULL);
INSERT INTO rel_candidato_eleccion (id, eleccion_id, candidato_id, user_create, date_create, user_update, date_update) VALUES (11, 2, 4, 2, '2017-11-27 17:10:39', NULL, NULL);


--
-- TOC entry 2122 (class 0 OID 1151764)
-- Dependencies: 187
-- Data for Name: rep_resultados; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO rep_resultados (id, candidatos_id, eleccion_id, hora) VALUES (1, 1, 1, '8:00 AM');
INSERT INTO rep_resultados (id, candidatos_id, eleccion_id, hora) VALUES (2, 1, 1, '8:10 AM');
INSERT INTO rep_resultados (id, candidatos_id, eleccion_id, hora) VALUES (3, 2, 1, '8:45 AM');
INSERT INTO rep_resultados (id, candidatos_id, eleccion_id, hora) VALUES (4, 3, 1, '9:00 AM');
INSERT INTO rep_resultados (id, candidatos_id, eleccion_id, hora) VALUES (5, 1, 1, '9:15 AM');
INSERT INTO rep_resultados (id, candidatos_id, eleccion_id, hora) VALUES (6, 2, 1, '9:20 AM');


--
-- TOC entry 2164 (class 0 OID 0)
-- Dependencies: 188
-- Name: rep_resultados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('rep_resultados_id_seq', 1, false);


--
-- TOC entry 2124 (class 0 OID 1151769)
-- Dependencies: 189
-- Data for Name: top_ciudades; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (43, 507001, 'PALO NEGRO', 5, 50700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1, 101001, 'CARACAS', 1, 10100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (2, 201001, 'LA ESMERALDA', 2, 20100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (3, 202001, 'SAN FERNANDO DE ATABAPO', 2, 20200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (4, 203001, 'PUERTO AYACUCHO', 2, 20300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (5, 204001, 'ISLA RATÓN', 2, 20400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (6, 205001, 'MAROA', 2, 20500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (7, 206001, 'SAN JUAN DE MANAPIARE', 2, 20600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (8, 207001, 'SAN CARLOS DE RÍO NEGRO', 2, 20700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (9, 301001, 'ANACO', 3, 30100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (10, 302001, 'ARAGUA DE BARCELONA', 3, 30200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (11, 303001, 'PUERTO PÍRITU', 3, 30300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (12, 304001, 'VALLE DE GUANAPE', 3, 30400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (13, 305001, 'PARIAGUÁN', 3, 30500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (14, 306001, 'GUANTA', 3, 30600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (15, 307001, 'SOLEDAD', 3, 30700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (16, 308001, 'PUERTO LA CRUZ', 3, 30800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (17, 309001, 'ONOTO', 3, 30900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (18, 310001, 'MAPIRE', 3, 31000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (19, 311001, 'SAN MATEO', 3, 31100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (20, 312001, 'CLARINES', 3, 31200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (21, 313001, 'CANTAURA', 3, 31300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (22, 314001, 'PÍRITU', 3, 31400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (23, 315001, 'SAN JOSE DE GUANIPA', 3, 31500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (24, 316001, 'BOCA DE UCHIRE', 3, 31600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (25, 317001, 'SANTA ANA', 3, 31700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (26, 318001, 'BARCELONA', 3, 31800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (27, 319001, 'EL TIGRE', 3, 31900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (28, 320001, 'EL CHAPARRO', 3, 32000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (29, 321001, 'LECHERÍAS', 3, 32100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (30, 401001, 'ACHAGUAS', 4, 40100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (31, 402001, 'BIRUACA', 4, 40200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (32, 403001, 'BRUZUAL', 4, 40300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (33, 404001, 'GUASDUALITO', 4, 40400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (34, 405001, 'SAN JUAN DE PAYARA', 4, 40500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (35, 406001, 'ELORZA', 4, 40600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (36, 407001, 'SAN FERNANDO DE APURE', 4, 40700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (37, 501001, 'SAN MATEO', 5, 50100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (38, 502001, 'CAMATAGUA', 5, 50200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (39, 503001, 'MARACAY', 5, 50300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (40, 504001, 'JOSÉ ÁNGE LAMAS', 5, 50400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (41, 505001, 'LA VICTORIA', 5, 50500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (42, 506001, 'EL CONSEJO', 5, 50600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (44, 508001, 'EL LIMÓN', 5, 50800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (45, 509001, 'SAN CASIMIRO', 5, 50900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (46, 510001, 'SAN SEBASTIÁN', 5, 51000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (47, 511001, 'TURMERO', 5, 51100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (48, 512001, 'LAS TEJERÍAS', 5, 51200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (49, 513001, 'CAGUA', 5, 51300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (50, 514001, 'COLONIA TOVAR', 5, 51400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (51, 515001, 'BARBACOAS', 5, 51500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (52, 516001, 'VILLA DE CURA', 5, 51600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (53, 517001, 'SANTA RITA', 5, 51700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (54, 518001, 'OCUMARE DE LA COSTA', 5, 51800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (55, 601001, 'SABANETA', 6, 60100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (56, 602001, 'SOCOPÓ', 6, 60200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (57, 603001, 'ARISMENDI', 6, 60300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (58, 604001, 'BARINAS', 6, 60400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (59, 605001, 'BARINITAS', 6, 60500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (60, 606001, 'BARRANCAS', 6, 60600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (61, 607001, 'SANTA BÁRBARA', 6, 60700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (62, 608001, 'OBISPOS', 6, 60800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (63, 609001, 'CIUDAD BOLIVIA', 6, 60900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (64, 610001, 'LIBERTAD', 6, 61000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (65, 611001, 'CIUDAD DE NUTRIAS', 6, 61100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (66, 612001, 'EL CANTÓN', 6, 61200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (67, 701001, 'CIUDAD GUAYANA', 7, 70100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (68, 702001, 'CAICARA DEL ORINOCO', 7, 70200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (69, 703001, 'EL CALLAO', 7, 70300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (70, 704001, 'SANTA ELENA DE UAIRÉN', 7, 70400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (71, 705001, 'CIUDAD BOLÍVAR', 7, 70500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (72, 706001, 'UPATA', 7, 70600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (73, 707001, 'CIUDAD PIAR', 7, 70700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (74, 708001, 'GUASIPATI', 7, 70800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (75, 709001, 'TUMEREMO', 7, 70900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (76, 710001, 'MARIPA', 7, 71000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (77, 711001, 'EL PALMAR', 7, 71100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (78, 801001, 'BEJUMA', 8, 80100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (79, 802001, 'GÜIEGÜIE', 8, 80200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (80, 803001, 'MARIARA', 8, 80300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (81, 804001, 'GUACARA', 8, 80400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (82, 805001, 'MORÓN', 8, 80500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (83, 806001, 'TOCUYITO', 8, 80600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (84, 807001, 'LOS GUAYOS', 8, 80700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (85, 808001, 'MIRANDA', 8, 80800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (86, 809001, 'MONTALBÁN', 8, 80900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (87, 810001, 'NAGUANAGUA', 8, 81000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (88, 811001, 'PUERTO CABELLO', 8, 81100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (89, 812001, 'SAN DIEGO', 8, 81200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (90, 813001, 'SAN JOAQUÍN', 8, 81300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (91, 814001, 'VALENCIA', 8, 81400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (92, 901001, 'COJEDES', 9, 90100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (93, 902001, 'TRANQUILO', 9, 90200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (94, 903001, 'EL BAÚL', 9, 90300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (95, 904001, 'MACAPO', 9, 90400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (96, 905001, 'EL PAO', 9, 90500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (97, 906001, 'LIBERTAD', 9, 90600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (98, 907001, 'LAS VEGAS', 9, 90700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (99, 908001, 'SAN CARLOS DE RÍO NEGRO', 9, 90800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (100, 909001, 'TINACO', 9, 90900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (101, 1001001, 'CUARIAPO', 10, 100100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (102, 1002001, 'SIERRA IMATACA', 10, 100200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (103, 1003001, 'PEDERNALES', 10, 100300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (104, 1004001, 'TUCUPITA', 10, 100400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (105, 1101001, 'SAN JUÁN DE LOS CAYOS', 11, 110100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (106, 1102001, 'SAN LUIS', 11, 110200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (107, 1103001, 'CATAPÁRIDA', 11, 110300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (108, 1104001, 'YARACAL', 11, 110400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (109, 1105001, 'PUNTO FIJO', 11, 110500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (110, 1106001, 'LA VELA DE CORO', 11, 110600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (111, 1107001, 'DABAJURO', 11, 110700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (112, 1108001, 'PEDREGAL', 11, 110800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (113, 1109001, 'PUEBLO NUEVO', 11, 110900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (114, 1110001, 'CHURUGUARA', 11, 111000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (115, 1111001, 'JACURA', 11, 111100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (116, 1112001, 'SANTA CRUZ DE LOS TAQUES', 11, 111200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (117, 1113001, 'MENE DE MAUROA', 11, 111300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (118, 1114001, 'SANTA ANA DE CORO', 11, 111400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (119, 1115001, 'CHICHIRIVICHE', 11, 111500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (120, 1116001, 'PALMASOLA', 11, 111600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (121, 1117001, 'CABURE', 11, 111700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (122, 1118001, 'PÍRITU', 11, 111800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (123, 1119001, 'MIRIMIRE', 11, 111900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (124, 1120001, 'TUCACAS', 11, 112000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (125, 1121001, 'LA CRUZ DE TARATARA', 11, 112100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (126, 1122001, 'TOCÓPERO', 11, 112200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (127, 1123001, 'SANTA CRUZ DE BURACAL', 11, 112300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (128, 1124001, 'URUMACO', 11, 112400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (129, 1125001, 'PUERTO CUMAREBO', 11, 112500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (130, 1201001, 'CAMAGUÁN', 12, 120100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (131, 1202001, 'CHAGUARAMAS', 12, 120200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (132, 1203001, 'EL SOCORRO', 12, 120300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (133, 1204001, 'GUAYABAL', 12, 120400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (134, 1205001, 'VALLE DE LA PASCUA', 12, 120500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (135, 1206001, 'LAS MERCEDES', 12, 120600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (136, 1207001, 'EL SOMBRERO', 12, 120700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (137, 1208001, 'CALABOZO', 12, 120800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (138, 1209001, 'ALTAGRACIA DE ORITUCO', 12, 120900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (139, 1210001, 'ORTIZ', 12, 121000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (140, 1211001, 'TUCUPIDO', 12, 121100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (141, 1212001, 'SAN JUAN DE LOS MORROS', 12, 121200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (142, 1213001, 'SAN JOSÉ DE GUARIBE', 12, 121300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (143, 1214001, 'SANTA MARÍA DE IPIRE', 12, 121400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (144, 1215001, 'ZARAZA', 12, 121500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (145, 1301001, 'SANARE', 13, 130100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (146, 1302001, 'DUACA', 13, 130200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (147, 1303001, 'BARQUISIMETO', 13, 130300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (148, 1304001, 'QUÍBOR', 13, 130400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (149, 1305001, 'EL TOCUYO', 13, 130500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (150, 1306001, 'CABUDARE', 13, 130600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (151, 1307001, 'SARARE', 13, 130700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (152, 1308001, 'CARORA', 13, 130800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (153, 1309001, 'SIQUISQUE', 13, 130900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (154, 1401001, 'EL VIGÍA', 14, 140100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (155, 1402001, 'LA AZULITA', 14, 140200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (156, 1403001, 'SANTA CRUZ DE MORA', 14, 140300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (157, 1404001, 'ARICAGUA', 14, 140400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (158, 1405001, 'CANAGUA', 14, 140500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (159, 1406001, 'EJIDO', 14, 140600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (160, 1407001, 'TUCANI', 14, 140700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (161, 1408001, 'SANTO DOMINGO', 14, 140800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (162, 1409001, 'GUARAQUE', 14, 140900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (163, 1410001, 'ARAPUEY', 14, 141000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (164, 1411001, 'TORONDOY', 14, 141100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (165, 1412001, 'MÉRIDA', 14, 141200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (166, 1413001, 'TIMOTES', 14, 141300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (167, 1414001, 'SANTA ELENA DE ARENALES', 14, 141400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (168, 1415001, 'SANTA MARÍA DE CAPARO', 14, 141500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (169, 1416001, 'PUEBLO LLANO', 14, 141600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (170, 1417001, 'MUCHUCHÍES', 14, 141700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (171, 1418001, 'BAILADORES', 14, 141800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (172, 1419001, 'TABAY', 14, 141900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (173, 1420001, 'LAGUNILLAS', 14, 142000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (174, 1421001, 'TOVAR', 14, 142100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (175, 1422001, 'NUEVA BOLIVIA', 14, 142200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (176, 1423001, 'ZEA', 14, 142300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (177, 1501001, 'CAUCAGUA', 15, 150100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (178, 1502001, 'SAN JOSÉ DE BARLOVENTO', 15, 150200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (179, 1503001, 'NUESTRA SEÑORA DEL ROSARIO DE BARUTA', 15, 150300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (180, 1504001, 'HIGUEROTE', 15, 150400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (181, 1505001, 'MAMPORAL', 15, 150500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (182, 1506001, 'CARRIZAL', 15, 150600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (183, 1507001, 'CHACAO', 15, 150700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (184, 1508001, 'CHARALLAVE', 15, 150800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (185, 1509001, 'EL HATILLO', 15, 150900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (186, 1510001, 'LOS TEQUES', 15, 151000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (187, 1511001, 'SANTA TERESA DEL  TUY', 15, 151100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (188, 1512001, 'OCUMARE DEL TUY', 15, 151200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (189, 1513001, 'SAN ANTONIO DE LOS ALTOS', 15, 151300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (190, 1514001, 'RÍO CHICO', 15, 151400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (191, 1515001, 'SANTA LUCÍA', 15, 151500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (192, 1516001, 'CÚPIRA', 15, 151600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (193, 1517001, 'GUARENAS', 15, 151700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (194, 1518001, 'SAN FRANCISCO DE YARE', 15, 151800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (195, 1519001, 'PETARE', 15, 151900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (196, 1520001, 'CÚA', 15, 152000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (197, 1521001, 'GUATIRE', 15, 152100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (198, 1601001, 'SAN ANTONIO DE LOS ALTOS', 16, 160100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (199, 1602001, 'AGUASAY', 16, 160200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (200, 1603001, 'CARIPITO', 16, 160300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (201, 1604001, 'CARIPE', 16, 160400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (202, 1605001, 'CAICARA', 16, 160500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (203, 1606001, 'PUNTA DE MATA', 16, 160600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (204, 1607001, 'TEMBLADOR', 16, 160700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (205, 1608001, 'MATURÍN', 16, 160800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (206, 1609001, 'ARAGUA DE MATURÍN', 16, 160900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (207, 1610001, 'PUNCERES', 16, 161000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (208, 1611001, 'SANTA BÁRBARA', 16, 161100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (209, 1612001, 'BARRANCAS', 16, 161200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (210, 1613001, 'URACOA', 16, 161300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (211, 1701001, 'PLAZA PARAGUACHI', 17, 170100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (212, 1702001, 'LA ASUNCIÓN', 17, 170200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (213, 1703001, 'SAN JUAN BAUTISTA', 17, 170300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (214, 1704001, 'EL VALLE DEL ESPÍRITU SANTO', 17, 170400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (215, 1705001, 'SANTA ANA', 17, 170500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (216, 1706001, 'PAMPATAR', 17, 170600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (217, 1707001, 'JUAN GRIEGO', 17, 170700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (218, 1708001, 'PORLAMAR', 17, 170800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (219, 1709001, 'BOCA DEL RÍO', 17, 170900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (220, 1710001, 'PUNTA DE PIEDRA', 17, 171000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (221, 1711001, 'SAN PEDRO DE COCHE', 17, 171100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (222, 1801001, 'AGUA BLANCA', 18, 180100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (223, 1802001, 'ARAURE', 18, 180200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (224, 1803001, 'PÍRITU', 18, 180300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (225, 1804001, 'GUANARE', 18, 180400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (226, 1805001, 'GUANARITO', 18, 180500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (227, 1806001, 'PARAÍSO DE CHABASQUÉN', 18, 180600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (228, 1807001, 'OSPINO', 18, 180700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (229, 1808001, 'ACARIGUA', 18, 180800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (230, 1809001, 'PAPELÓN', 18, 180900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (231, 1810001, 'BOCONOITO', 18, 181000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (232, 1811001, 'SAN RAFAEL DE ONOTO', 18, 181100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (233, 1812001, 'EL PLAYÓN', 18, 181200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (234, 1813001, 'MUNICIPIO SUCRE BISCUCUY', 18, 181300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (235, 1814001, 'VILLA BRUZUAL', 18, 181400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (236, 1901001, 'CASANAY', 19, 190100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (237, 1902001, 'SAN JOSÉ DE AEROCUAR', 19, 190200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (238, 1903001, 'RÍO CARIBE', 19, 190300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (239, 1904001, 'EL PILAR', 19, 190400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (240, 1905001, 'CARÚPANO', 19, 190500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (241, 1906001, 'MARIGÜITAR', 19, 190600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (242, 1907001, 'YAGUARAPARO', 19, 190700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (243, 1908001, 'ARAYA', 19, 190800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (244, 1909001, 'TUNAPUY', 19, 190900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (245, 1910001, 'IRAPA', 19, 191000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (246, 1911001, 'SAN ANTONIO DEL GOLFO', 19, 191100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (247, 1912001, 'CUMANACOA', 19, 191200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (248, 1913001, 'CARIACO', 19, 191300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (249, 1914001, 'CUMANÁ', 19, 191400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (250, 1915001, 'GÜIRIA', 19, 191500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (251, 2001001, 'CORDERO', 20, 200100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (252, 2002001, 'LAS MESAS', 20, 200200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (253, 2003001, 'COLÓN', 20, 200300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (254, 2004001, 'SAN ANTONIO DEL TÁCHIRA', 20, 200400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (255, 2005001, 'TÁRIBA', 20, 200500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (256, 2006001, 'SANTA ANA DEL TÁCHIRA', 20, 200600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (257, 2007001, 'SAN RAFAEL DEL PIÑAL', 20, 200700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (258, 2008001, 'SAN JOSÉ DE BOLÍVAR', 20, 200800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (259, 2009001, 'LA FRÍA', 20, 200900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (260, 2010001, 'PALMIRA', 20, 201000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (261, 2011001, 'CAPACHO NUEVO', 20, 201100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (262, 2012001, 'LA GRITA', 20, 201200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (263, 2013001, 'EL COBRE', 20, 201300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (264, 2014001, 'RUBIO', 20, 201400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (265, 2015001, 'CAPACHO VIEJO', 20, 201500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (266, 2016001, 'ABEJALES', 20, 201600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (267, 2017001, 'LOBATERA', 20, 201700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (268, 2018001, 'MICHELENA', 20, 201800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (269, 2019001, 'COLONCITO', 20, 201900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (270, 2020001, 'UREÑA', 20, 202000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (271, 2021001, 'DELICIAS', 20, 202100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (272, 2022001, 'LA TENDIDA', 20, 202200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (273, 2023001, 'SAN CRISTÓBAL', 20, 202300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (274, 2024001, 'SEBORUCO', 20, 202400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (275, 2025001, 'SAN SIMÓN', 20, 202500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (276, 2026001, 'QUENIQUEA', 20, 202600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (277, 2027001, 'SAN JOSECITO', 20, 202700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (278, 2028001, 'PREGONERO', 20, 202800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (279, 2029001, 'UMUQUENA', 20, 202900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (280, 2101001, 'SANTA ISABEL', 21, 210100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (281, 2102001, 'BOCONÓ', 21, 210200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (282, 2103001, 'SABANA GRANDE', 21, 210300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (283, 2104001, 'CHEJENDÉ', 21, 210400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (284, 2105001, 'CARACHE', 21, 210500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (285, 2106001, 'ESCUQUE', 21, 210600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (286, 2107001, 'EL PARADERO', 21, 210700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (287, 2108001, 'CAMPO ELÍAS', 21, 210800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (288, 2109001, 'SANTA APOLONIA', 21, 210900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (289, 2110001, 'EL DIVIDIVE', 21, 211000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (290, 2111001, 'MONTE CARMELO', 21, 211100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (291, 2112001, 'MOTATÁN', 21, 211200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (292, 2113001, 'PAMPÁN', 21, 211300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (293, 2114001, 'PAMPANITO', 21, 211400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (294, 2115001, 'BETIJOQUE', 21, 211500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (295, 2116001, 'CARVAJAL', 21, 211600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (296, 2117001, 'SABANA DE MENDOZA', 21, 211700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (297, 2118001, 'TRUJILLO', 21, 211800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (298, 2119001, 'LA QUEBRADA', 21, 211900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (299, 2120001, 'VALERA', 21, 212000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (300, 2201001, 'SAN PABLO', 22, 220100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (301, 2202001, 'AROA', 22, 220200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (302, 2203001, 'CHIVACOA', 22, 220300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (303, 2204001, 'COCOROTE', 22, 220400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (304, 2205001, 'INDEṔENDENCIA', 22, 220500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (305, 2206001, 'SABAN DE PARRA', 22, 220600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (306, 2207001, 'BORAURE', 22, 220700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (307, 2208001, 'YUMARE', 22, 220800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (308, 2209001, 'NIRGUA', 22, 220900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (309, 2210001, 'YARITAGUA', 22, 221000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (310, 2211001, 'SAN FELIPE', 22, 221100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (311, 2212001, 'GUAMA', 22, 221200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (312, 2213001, 'URACHICHE', 22, 221300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (313, 2214001, 'FARRIAR', 22, 221400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (314, 2301001, 'EL TORO', 23, 230100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (315, 2302001, 'SAN TIMOTEO', 23, 230200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (316, 2303001, 'CABIMAS', 23, 230300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (317, 2304001, 'ENCONTRADOS', 23, 230400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (318, 2305001, 'SAN CARLOS DEL ZULIA', 23, 230500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (319, 2306001, 'PUEBLO NUEVO', 23, 230600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (320, 2307001, 'LA CONCEPCIÓN', 23, 230700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (321, 2308001, 'CASIGUA EL CUBO', 23, 230800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (322, 2309001, 'CONCEPCIÓN', 23, 230900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (323, 2310001, 'CIUDAD OJEDA', 23, 231000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (324, 2311001, 'MACHIQUES', 23, 231100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (325, 2312001, 'SAN RAFAEL DE MOJÁN', 23, 231200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (326, 2313001, 'MARACAIBO', 23, 231300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (327, 2314001, 'LOS PUERTOS DE ALTAGRACIA', 23, 231400, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (328, 2315001, 'SINAMAICA', 23, 231500, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (329, 2316001, 'LA VILLLA DEL ROSARIO', 23, 231600, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (330, 2317001, 'SAN FRANCISCO', 23, 231700, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (331, 2318001, 'SANTA RITA', 23, 231800, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (332, 2319001, 'TÍA JUANA', 23, 231900, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (333, 2320001, 'BOBURES', 23, 232000, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (334, 2321001, 'BACHAQUERO', 23, 232100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (335, 2401001, 'LA GUAIRA', 24, 240100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (336, 2501001, 'LOS ROQUES', 25, 250100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (337, 2502001, 'LAS AVES', 25, 250200, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (338, 2503001, 'LA ORCHILA', 25, 250300, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (339, 2601001, 'OTRA  LOCALIDAD (DEPENDENCIA FEDERAL)', 26, 260100, 230, true);
INSERT INTO top_ciudades (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (340, 2701001, 'OTRA CIUDAD O LOCALIDAD', 27, 270100, 230, true);


--
-- TOC entry 2165 (class 0 OID 0)
-- Dependencies: 190
-- Name: top_ciudades_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('top_ciudades_id_seq', 1, false);


--
-- TOC entry 2126 (class 0 OID 1151774)
-- Dependencies: 191
-- Data for Name: top_estados; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (1, 1, 'DISTRITO CAPITAL', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (2, 2, 'AMAZONAS', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (3, 3, 'ANZOATEGUI', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (4, 4, 'APURE', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (5, 5, 'ARAGUA', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (6, 6, 'BARINAS', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (7, 7, 'BOLIVAR', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (8, 8, 'CARABOBO', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (9, 9, 'COJEDES', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (10, 10, 'DELTA AMACURO', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (11, 11, 'FALCON', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (12, 12, 'GUARICO', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (13, 13, 'LARA', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (14, 14, 'MERIDA', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (15, 15, 'MIRANDA', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (16, 16, 'MONAGAS', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (17, 17, 'NUEVA ESPARTA', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (18, 18, 'PORTUGUESA', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (19, 19, 'SUCRE', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (20, 20, 'TACHIRA', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (21, 21, 'TRUJILLO', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (22, 22, 'YARACUY', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (23, 23, 'ZULIA', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (24, 24, 'VARGAS', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (25, 25, 'TERRITORIO INSULAR FRANCISCO DE MIRANDA', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (26, 26, 'OTRAS DEPENDECIAS FEDERALES', 230, true);
INSERT INTO top_estados (id, codigo, descripcion, pais_id, activo) VALUES (27, 27, 'REGISTRO DE NORMALIZACIÓN (ESTADO)', 230, true);


--
-- TOC entry 2166 (class 0 OID 0)
-- Dependencies: 192
-- Name: top_estados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('top_estados_id_seq', 1, false);


--
-- TOC entry 2128 (class 0 OID 1151779)
-- Dependencies: 193
-- Data for Name: top_municipios; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (1, 10100, 'LIBERTADOR', 1, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (2, 20100, 'AUTÓNOMO ALTO ORINOCO', 2, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (3, 20200, 'AUTÓNOMO ATABAPO', 2, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (4, 20300, 'AUTÓNOMO ATURES', 2, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (5, 20400, 'AUTONOMO AUTANA', 2, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (6, 20500, 'AUTÓNOMO MAROA', 2, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (7, 20600, 'AUTÓNOMO MANAPIARE', 2, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (8, 20700, 'AUTÓNOMO RÍO NEGRO', 2, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (9, 30100, 'ANACO', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (10, 30200, 'ARAGUA', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (11, 30300, 'FERNANDO DE PEÑALVER', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (12, 30400, 'FRANCISCO DEL CARMEN CARVAJAL', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (13, 30500, 'FRANCISCO DE MIRANDA', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (14, 30600, 'GUANTA', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (15, 30700, 'INDEPENDENCIA', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (16, 30800, 'JUAN ANTONIO SOTILLO', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (17, 30900, 'JUAN MANUEL CAJIGAL', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (18, 31000, 'JOSÉ GREGORIO MONAGAS', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (19, 31100, 'LIBERTAD', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (20, 31200, 'MANUEL EZEQUIEL BRUZUAL', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (21, 31300, 'PEDRO MARÍA FREITES', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (22, 31400, 'PÍRITU', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (23, 31500, 'SAN JOSÉ DE GUANIPA', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (24, 31600, 'SAN JUAN DE CAPISTRANO', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (25, 31700, 'SANTA ANA', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (26, 31800, 'SIMÓN BOLÍVAR', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (27, 31900, 'SIMÓN RODRÍGUEZ', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (28, 32000, 'SIR ARTHUR MC GREGOR', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (29, 32100, 'TURÍSTICO DIEGO BAUTISTA URBANEJA', 3, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (30, 40100, 'ACHAGUAS', 4, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (31, 40200, 'BIRUACA', 4, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (32, 40300, 'MUÑOZ', 4, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (33, 40400, 'PÁEZ', 4, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (34, 40500, 'PEDRO CAMEJO', 4, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (35, 40600, 'RÓMULO GALLEGOS', 4, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (36, 40700, 'SAN FERNANDO', 4, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (37, 50100, 'BOLÍVAR', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (38, 50200, 'CAMATAGUA', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (39, 50300, 'GIRARDOT', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (40, 50400, 'JOSÉ ÁNGEL LAMAS', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (41, 50500, 'JOSÉ FÉLIX RIBAS', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (42, 50600, 'JOSÉ RAFAEL REVENGA', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (43, 50700, 'LIBERTADOR', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (44, 50800, 'MARIO BRICEÑO IRAGORRY', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (45, 50900, 'SAN CASIMIRO', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (46, 51000, 'SAN SEBASTIÁN', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (47, 51100, 'SANTIAGO MARIÑO', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (48, 51200, 'SANTOS MICHELENA', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (49, 51300, 'SUCRE', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (50, 51400, 'TOVAR', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (51, 51500, 'URDANETA', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (52, 51600, 'ZAMORA', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (53, 51700, 'FRANCISCO LINARES ALCANTARA', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (54, 51800, 'OCUMARE DE LA COSTA DE ORO', 5, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (55, 60100, 'ALBERTO ARVELO TORREALBA', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (56, 60200, 'ANTONIO JOSÉ DE SUCRE', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (57, 60300, 'ARISMENDI', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (58, 60400, 'BARINAS', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (59, 60500, 'BOLÍVAR', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (60, 60600, 'CRUZ PAREDES', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (61, 60700, 'EZEQUIEL ZAMORA', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (62, 60800, 'OBISPOS', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (63, 60900, 'PEDRAZA', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (64, 61000, 'ROJAS', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (65, 61100, 'SOSA', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (66, 61200, 'ANDRÉS ELOY BLANCO', 6, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (67, 70100, 'CARONÍ', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (68, 70200, 'CEDEÑO', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (69, 70300, 'EL CALLAO', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (70, 70400, 'GRAN SABANA', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (71, 70500, 'HERES', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (72, 70600, 'PIAR', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (73, 70700, 'RAÚL LEONI', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (74, 70800, 'ROSCIO', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (75, 70900, 'SIFONTES', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (76, 71000, 'SUCRE', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (77, 71100, 'PADRE PEDRO CHIEN', 7, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (78, 80100, 'BEJUMA', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (79, 80200, 'CARLOS ARVELO', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (80, 80300, 'DIEGO IBARRA', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (81, 80400, 'GUACARA', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (82, 80500, 'JUAN JOSÉ MORA', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (83, 80600, 'LIBERTADOR', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (84, 80700, 'LOS GUAYOS', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (85, 80800, 'MIRANDA', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (86, 80900, 'MONTALBÁN', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (87, 81000, 'NAGUANAGUA', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (88, 81100, 'PUERTO CABELLO', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (89, 81200, 'SAN DIEGO', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (90, 81300, 'SAN JOAQUÍN', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (91, 81400, 'VALENCIA', 8, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (92, 90100, 'ANZOÁTEGUI', 9, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (93, 90200, 'FALCÓN', 9, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (94, 90300, 'GIRARDOT', 9, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (95, 90400, 'LIMA BLANCO', 9, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (96, 90500, 'PAO DE SAN JUAN BAUTISTA', 9, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (97, 90600, 'RICAURTE', 9, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (98, 90700, 'RÓMULO GALLEGOS', 9, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (99, 90800, 'SAN CARLOS', 9, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (100, 90900, 'TINACO', 9, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (101, 100100, 'ANTONIO DÍAZ', 10, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (102, 100200, 'CASACOIMA', 10, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (103, 100300, 'PEDERNALES', 10, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (104, 100400, 'TUCUPITA', 10, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (105, 110100, 'ACOSTA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (106, 110200, 'BOLÍVAR', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (107, 110300, 'BUCHIVACOA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (108, 110400, 'CACIQUE MANAURE', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (109, 110500, 'CARIRUBANA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (110, 110600, 'COLINA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (111, 110700, 'DABAJURO', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (112, 110800, 'DEMOCRACIA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (113, 110900, 'FALCÓN', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (114, 111000, 'FEDERACIÓN', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (115, 111100, 'JACURA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (116, 111200, 'LOS TAQUES', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (117, 111300, 'MAUROA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (118, 111400, 'MIRANDA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (119, 111500, 'MONSEÑOR ITURRIZA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (120, 111600, 'PALMASOLA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (121, 111700, 'PETIT', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (122, 111800, 'PIRITU', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (123, 111900, 'SAN FRANCISCO', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (124, 112000, 'SILVA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (125, 112100, 'SUCRE', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (126, 112200, 'TOCOPERO', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (127, 112300, 'UNIÓN', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (128, 112400, 'URUMACO', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (129, 112500, 'ZAMORA', 11, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (130, 120100, 'CAMAGUAN', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (131, 120200, 'CHAGUARAMAS', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (132, 120300, 'EL SOCORRO', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (133, 120400, 'SAN GERÓNIMO DE GUAYABAL', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (134, 120500, 'LEONARDO INFANTE', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (135, 120600, 'LAS MERCEDES', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (136, 120700, 'JULIÁN MELLADO', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (137, 120800, 'FRANCISCO DE MIRANDA', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (138, 120900, 'JOSÉ TADEO MONAGAS', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (139, 121000, 'ORTIZ', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (140, 121100, 'JOSÉ FÉLIX RIBAS', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (141, 121200, 'JUAN GERMAN ROSCIO', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (142, 121300, 'SAN JOSÉ DE GUARIBE', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (143, 121400, 'SANTA MARÍA DE IPIRE', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (144, 121500, 'PEDRO ZARAZA', 12, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (145, 130100, 'ANDRÉS ELOY BLANCO', 13, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (146, 130200, 'CRESPO', 13, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (147, 130300, 'IRIBARREN', 13, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (148, 130400, 'JIMÉNEZ', 13, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (149, 130500, 'MORÓN', 13, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (150, 130600, 'PALAVECINO', 13, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (151, 130700, 'SIMÓN PLANAS', 13, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (152, 130800, 'TORRES', 13, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (153, 130900, 'URDANETA', 13, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (154, 140100, 'ALBERTO ADRIANI', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (155, 140200, 'ANDRÉS BELLO', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (156, 140300, 'ANTONIO PINTO SALINAS', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (157, 140400, 'ARICAGUA', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (158, 140500, 'ARZOBISPO CHACÓN', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (159, 140600, 'CAMPO ELÍAS', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (160, 140700, 'CARACCIOLO PARRA OLMEDO', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (161, 140800, 'CARDENAL QUINTERO', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (162, 140900, 'GUARAQUE', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (163, 141000, 'JULIO CESAR SALAS', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (164, 141100, 'JUSTO BRICEÑO', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (165, 141200, 'LIBERTADOR', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (166, 141300, 'MIRANDA', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (167, 141400, 'OBISPO RAMOS DE LORA', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (168, 141500, 'PADRE NOGUERA', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (169, 141600, 'PUEBLO LLANO', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (170, 141700, 'RANGEL', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (171, 141800, 'RIVAS DÁVILA', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (172, 141900, 'SANTOS MARQUINA', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (173, 142000, 'SUCRE', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (174, 142100, 'TOVAR', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (175, 142200, 'TULIO FEBRES CORDERO', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (176, 142300, 'ZEA', 14, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (177, 150100, 'ACEVEDO', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (178, 150200, 'ANDRÉS BELLO', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (179, 150300, 'BARUTA', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (180, 150400, 'BRION', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (181, 150500, 'BUROZ', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (182, 150600, 'CARRIZAL', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (183, 150700, 'CHACAO', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (184, 150800, 'CRISTÓBAL ROJAS', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (185, 150900, 'EL HATILLO', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (186, 151000, 'GUAICAIPURO', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (187, 151100, 'INDEPENDENCIA', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (188, 151200, 'LANDER', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (189, 151300, 'LOS SALÍAS', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (190, 151400, 'PÁEZ', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (191, 151500, 'PAZ CASTILLO', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (192, 151600, 'PEDRO GUAL', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (193, 151700, 'PLAZA', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (194, 151800, 'SIMÓN BOLÍVAR', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (195, 151900, 'SUCRE', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (196, 152000, 'URDANETA', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (197, 152100, 'ZAMORA', 15, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (198, 160100, 'ACOSTA', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (199, 160200, 'AGUASAY', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (200, 160300, 'BOLÍVAR', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (201, 160400, 'CARIPE', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (202, 160500, 'CEDEÑO', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (203, 160600, 'EZEQUIEL ZAMORA', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (204, 160700, 'LIBERTADOR', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (205, 160800, 'MATURÍN', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (206, 160900, 'PIAR', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (207, 161000, 'PUNCERES', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (208, 161100, 'SANTA BÁRBARA', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (209, 161200, 'SOTILLO', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (210, 161300, 'URACOA', 16, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (211, 170100, 'ANTOLON DEL CAMPO', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (212, 170200, 'ARISMENDI', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (213, 170300, 'DÍAZ', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (214, 170400, 'GARCÍA', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (215, 170500, 'GÓMEZ', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (216, 170600, 'MANEIRO', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (217, 170700, 'MARCANO', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (218, 170800, 'MARIÑO', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (219, 170900, 'PENÍNSULA DE MACANAO', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (220, 171000, 'TUBORES', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (221, 171100, 'VILLALBA', 17, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (222, 180100, 'AGUA BLANCA', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (223, 180200, 'ARAURE', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (224, 180300, 'ESTELLER', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (225, 180400, 'GUANARE', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (226, 180500, 'GUANARITO', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (227, 180600, 'MONSEÑOR JOSÉ VICENTE DE UNDA', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (228, 180700, 'OSPINO', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (229, 180800, 'PÁEZ', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (230, 180900, 'PAPELON', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (231, 181000, 'SAN GENARO DE BOCONOITO', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (232, 181100, 'SAN RAFAEL DE ONOTO', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (233, 181200, 'SANTA ROSALÍA', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (234, 181300, 'SUCRE', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (235, 181400, 'TURÍN', 18, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (236, 190100, 'ANDRÉS ELOY BLANCO', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (237, 190200, 'ANDRÉS MATA', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (238, 190300, 'ARISMENDI', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (239, 190400, 'BENÍTEZ', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (240, 190500, 'BERMÚDEZ', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (241, 190600, 'BOLÍVAR', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (242, 190700, 'CAJIGAL', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (243, 190800, 'CRUZ SALMERÓN ACOSTA', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (244, 190900, 'LIBERTADOR', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (245, 191000, 'MARIÑO', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (246, 191100, 'MEJÍAS', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (247, 191200, 'MONTES', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (248, 191300, 'RIBERO', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (249, 191400, 'SUCRE', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (250, 191500, 'VALDEZ', 19, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (251, 200100, 'ANDRÉS BELLO', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (252, 200200, 'ANTONIO RÓMULO COSTA', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (253, 200300, 'AYACUCHO', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (254, 200400, 'BOLÍVAR', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (255, 200500, 'CÁRDENAS', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (256, 200600, 'CÓRDOBA', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (257, 200700, 'FERNÁNDEZ FEO', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (258, 200800, 'FRANCISCO DE MIRANDA', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (259, 200900, 'GARCÍA DE HEVIA', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (260, 201000, 'GUASIMOS', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (261, 201100, 'INDEPENDENCIA', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (262, 201200, 'JÁUREGUI', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (263, 201300, 'JOSÉ MARÍA VARGAS', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (264, 201400, 'JUNÍN', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (265, 201500, 'LIBERTAD', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (266, 201600, 'LIBERTADOR', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (267, 201700, 'LOBATERA', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (268, 201800, 'MICHELENA', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (269, 201900, 'PANAMERICANO', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (270, 202000, 'PEDRO MARÍA UREÑA', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (271, 202100, 'RAFAEL URDANETA', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (272, 202200, 'SAMUEL DARÍO MALDONADO', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (273, 202300, 'SAN CRISTÓBAL', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (274, 202400, 'SEBORUCO', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (275, 202500, 'SIMÓN RODRÍGUEZ', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (276, 202600, 'SUCRE', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (277, 202700, 'TORBES', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (278, 202800, 'URIBANTE', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (279, 202900, 'SAN JUDAS TADEO', 20, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (280, 210100, 'ANDRÉS BELLO', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (281, 210200, 'BOCONO', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (282, 210300, 'BOLÍVAR', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (283, 210400, 'CANDELARIA', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (284, 210500, 'CARACHE', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (285, 210600, 'ESCUQUE', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (286, 210700, 'JOSÉ FELIPE MÁRQUEZ CARRIZALES', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (287, 210800, 'JUAN VICENTE CAMPO ELÍAS', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (288, 210900, 'LA CEIBA', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (289, 211000, 'MIRANDA', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (290, 211100, 'MONTE CARMELO', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (291, 211200, 'MOTATAN', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (292, 211300, 'PAMPAN', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (293, 211400, 'PAMPANITO', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (294, 211500, 'RAFAEL RANGEL', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (295, 211600, 'SAN RAFAEL DE CARVAJAL', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (296, 211700, 'SUCRE', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (297, 211800, 'TRUJILLO', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (298, 211900, 'URDANETA', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (299, 212000, 'VALERA', 21, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (300, 220100, 'ARÍSTIDES BASTIDAS', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (301, 220200, 'BOLÍVAR', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (302, 220300, 'BRUZUAL', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (303, 220400, 'COCOROTE', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (304, 220500, 'INDEPENDENCIA', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (305, 220600, 'JOSÉ ANTONIO PÁEZ', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (306, 220700, 'LA TRINIDAD', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (307, 220800, 'MANUEL MONGE', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (308, 220900, 'NIRGUA', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (309, 221000, 'PEÑA', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (310, 221100, 'SAN FELIPE', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (311, 221200, 'SUCRE', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (312, 221300, 'URACHICHE', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (313, 221400, 'VEROES', 22, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (314, 230100, 'ALMIRANTE PADILLA', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (315, 230200, 'BARALT', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (316, 230300, 'CABIMAS', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (317, 230400, 'CATATUMBO', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (318, 230500, 'COLON', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (319, 230600, 'FRANCISCO JAVIER PULGAR', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (320, 230700, 'JESÚS ENRIQUE LOSSADA', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (321, 230800, 'JESÚS MARÍA SEMPRON', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (322, 230900, 'LA CAÑADA DE URDANETA', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (323, 231000, 'LAGUNILLAS', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (324, 231100, 'MACHIQUES DE PERIJÁ', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (325, 231200, 'MARA', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (326, 231300, 'MARACAIBO', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (327, 231400, 'MIRANDA', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (328, 231500, 'PÁEZ', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (329, 231600, 'ROSARIO DE PERIJÁ', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (330, 231700, 'SAN FRANCISCO', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (331, 231800, 'SANTA RITA', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (332, 231900, 'SIMÓN BOLÍVAR', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (333, 232000, 'SUCRE', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (334, 232100, 'VALMORE RODRÍGUEZ', 23, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (335, 240100, 'VARGAS', 24, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (336, 250100, 'ARCHIPIÉLAGO LOS ROQUES', 25, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (337, 250200, 'ARCHIPIÉLAGO LAS AVES', 25, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (338, 250300, 'ARCHIPIÉLAGO LA ORCHILA', 25, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (339, 260100, 'OTRAS DEPENDENCIAS FEDERALES (M)', 26, 230, true);
INSERT INTO top_municipios (id, codigo, descripcion, estado_id, pais_id, activo) VALUES (340, 270100, 'REGISTRO DE NORMALIZACIÓN (MUNICIPIO)', 27, 230, true);


--
-- TOC entry 2167 (class 0 OID 0)
-- Dependencies: 194
-- Name: top_municipios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('top_municipios_id_seq', 1, false);


--
-- TOC entry 2130 (class 0 OID 1151784)
-- Dependencies: 195
-- Data for Name: top_paises; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (1, 1, 'AFGANISTÁN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (2, 2, 'AMERICAN SAMOA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (3, 3, 'ALEMANIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (4, 4, 'ALBANIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (5, 5, 'ANDORRA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (6, 6, 'ANGOLA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (7, 7, 'ANGUILA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (8, 8, 'ANTIGUA AND BARBUDA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (9, 9, 'ANTILLAS HOLANDESAS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (10, 10, 'ANTÁRTIDA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (11, 11, 'ARABIA SAUDITA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (12, 12, 'ARGELIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (13, 13, 'ARGENTINA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (14, 14, 'ARMENIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (15, 15, 'ARUBA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (16, 16, 'AUSTRALIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (17, 17, 'AUSTRIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (18, 18, 'AZERBAIYÁN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (19, 19, 'BAHAMAS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (20, 20, 'BAHRÉIN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (21, 21, 'BANGLADESH', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (22, 22, 'BARBADOS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (23, 23, 'BELICE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (24, 24, 'BENÍN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (25, 25, 'BERMUDA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (26, 26, 'BIELORRUSIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (27, 27, 'BOLIVIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (28, 28, 'BOSNIA Y HERZEGOVINA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (29, 29, 'BOTSUANA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (30, 30, 'BOUVET ISLAND', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (31, 31, 'BRASIL', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (32, 32, 'BRITISH INDIA OCEAN TERRITORY', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (33, 33, 'BRUNEI DARUSSALAM', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (34, 34, 'BULGARIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (35, 35, 'BURKINA FASO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (36, 36, 'BURUNDI', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (37, 37, 'BUTÁN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (38, 38, 'BÉLGICA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (39, 39, 'CABO VERDA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (40, 40, 'CAMBOYA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (41, 41, 'CAMERÚN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (42, 42, 'CANADA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (43, 43, 'CHAD', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (44, 44, 'CHILE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (45, 45, 'CHINA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (46, 46, 'CHIPRE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (47, 47, 'COLOMBIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (48, 48, 'COMORES', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (49, 49, 'CONGO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (50, 50, 'COREA DEL NORTE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (51, 51, 'COREA DEL SUR', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (52, 52, 'COSTA RICA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (53, 53, 'COTE D IVOIRE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (54, 54, 'CROACIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (55, 55, 'CUBA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (56, 56, 'DINAMARCA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (57, 57, 'DJIBOUTI', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (58, 58, 'DOMINICA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (59, 59, 'EAST TIMOR', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (60, 60, 'ECUADOR', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (61, 61, 'EGIPTO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (62, 62, 'EL SALVADOR', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (63, 63, 'EL VATICANO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (64, 64, 'EMIRATOS ARABES UNIDOS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (65, 65, 'ERITREA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (66, 66, 'ESLOVAQUIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (67, 67, 'ESLOVENIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (68, 68, 'ESPAÑA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (69, 69, 'ESTADOS UNIDOS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (70, 70, 'ESTONIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (71, 71, 'ETIOPIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (72, 72, 'FIJI', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (73, 73, 'FILIPINAS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (74, 74, 'FINLANDIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (75, 75, 'FRANCIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (76, 76, 'FRENCH GUIANA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (77, 77, 'FRENCH POLYNESIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (78, 78, 'FRENCH SOUTHERN TERRITORIES', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (79, 79, 'GABON', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (80, 80, 'GAMBIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (81, 81, 'GEORGIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (82, 82, 'GHANA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (83, 83, 'GIBRALTAR', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (84, 84, 'GRANADA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (85, 85, 'GRECIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (86, 86, 'GROENLANDIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (87, 87, 'GUADALUPE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (88, 88, 'GUAM', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (89, 89, 'GUATEMALA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (90, 90, 'GUINEA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (91, 91, 'GUINEA ECUATORIAL', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (92, 92, 'GUINEA BISSAU', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (93, 93, 'GUYANA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (94, 94, 'HAITÍ', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (95, 95, 'HEARD ISLAND AND MCDONALD ISLA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (96, 96, 'HOLANDA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (97, 97, 'HONDURAS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (98, 98, 'HONG KONG', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (99, 99, 'HUNGRÍA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (100, 100, 'INDIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (101, 101, 'INDONESIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (102, 102, 'IRAQ', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (103, 103, 'IRLANDA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (104, 104, 'ISLAS COCOS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (105, 105, 'ISLA CHRISTMAS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (106, 106, 'ISLANDIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (107, 107, 'ISLAS CAIMÁN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (108, 108, 'ISLAS COOK', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (109, 109, 'ISLAS FEROE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (110, 110, 'ISLAS MALVINAS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (111, 111, 'ISLAS MARSHALL', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (112, 112, 'ISLAS MAURICIO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (113, 113, 'ISLAS SALOMÓN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (114, 114, 'ISLAS SÁNDWICH', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (115, 115, 'ISLAS TURKS Y CAICOS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (116, 116, 'ISLAS WALLIS Y FUTUNA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (117, 117, 'ISRAEL', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (118, 118, 'ITALIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (119, 119, 'JAMAICA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (120, 120, 'JAPÓN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (121, 121, 'JORDANIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (122, 122, 'KAZAKHSTAN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (123, 123, 'KENIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (124, 124, 'KIRIBATI', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (125, 125, 'KUWAIT', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (126, 126, 'KYRGYZSTAN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (127, 127, 'LAOS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (128, 128, 'LATVIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (129, 129, 'LESOTO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (130, 130, 'LIBERIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (131, 131, 'LIBIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (132, 132, 'LIECHTENSTEIN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (133, 133, 'LITUANIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (134, 134, 'LUXEMBURGO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (135, 135, 'LÍBANO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (136, 136, 'MACAO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (137, 137, 'MACEDONIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (138, 138, 'MADAGASCAR', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (139, 139, 'MALASIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (140, 140, 'MALAUI', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (141, 141, 'MALDIVAS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (142, 142, 'MALTA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (143, 143, 'MALI', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (144, 144, 'MARRUECOS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (145, 145, 'MARTINIQUE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (146, 146, 'MAURITANIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (147, 147, 'MAYOTTE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (148, 148, 'MICRONESIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (149, 149, 'MOLDAVIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (150, 150, 'MONGOLIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (151, 151, 'MONTSERRAT', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (152, 152, 'MOZAMBIQUE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (153, 153, 'MYANMAR', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (154, 154, 'MÉXICO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (155, 155, 'MÓNACO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (156, 156, 'NAMIBIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (157, 157, 'NAURU', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (158, 158, 'NEPAL', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (159, 159, 'NICARAGUA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (160, 160, 'NIGERIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (161, 161, 'NIUE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (162, 162, 'NORFOLK ISLAND', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (163, 163, 'NORTHERN MARIANA ISLANDS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (164, 164, 'NORUEGA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (165, 165, 'NUEVA CALEDONIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (166, 166, 'NUEVA ZELANDANIGER', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (167, 167, 'OMÁN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (168, 168, 'PAKISTÁN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (169, 169, 'PALAU', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (170, 170, 'PALESTINIAN TERRITORY', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (171, 171, 'PANAMÁ', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (172, 172, 'PAPUA NUEVA GUINEA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (173, 173, 'PARAGUAY', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (174, 174, 'PERÚ', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (175, 175, 'PITCAIRN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (176, 176, 'POLONIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (177, 177, 'PORTUGAL', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (178, 178, 'PUERTO RICO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (179, 179, 'QATAR', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (180, 180, 'REINO UNIDO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (181, 181, 'REPUBLICA CENTROAFRICANA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (182, 182, 'REPUBLICA CHECA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (183, 183, 'REPUBLICA DEMOCRÁTICA DEL CONG', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (184, 184, 'REPUBLICA DOMINICANA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (185, 185, 'REPUBLICA ISLÁMICA DE IRÁN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (186, 186, 'RUANDA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (187, 187, 'RUMANIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (188, 188, 'RUSIAN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (189, 189, 'SAINT KITTS AND NEVIS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (190, 190, 'SAINT PIERRE Y MIQUELON', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (191, 191, 'SAMOA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (192, 192, 'SAN MARINO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (193, 193, 'SAN VICENTE Y LAS GRANADINAS', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (194, 194, 'SANTA ELENA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (195, 195, 'SANTA LUCIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (196, 196, 'SAO TOME AND PRÍNCIPE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (197, 197, 'SENEGAL', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (198, 198, 'SERBIA Y MONTENEGRO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (199, 199, 'SEYCHELLES', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (200, 200, 'SIERRA LEONA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (201, 201, 'SINGAPUR', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (202, 202, 'SIRIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (203, 203, 'SOMALIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (204, 204, 'SRI LANKA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (205, 205, 'SUAZILANDIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (206, 206, 'SUDÁFRICA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (207, 207, 'SUDAN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (208, 208, 'SUECIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (209, 209, 'SUIZA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (210, 210, 'SURINAM', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (211, 211, 'SALVAR AND JAN MAYEN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (212, 212, 'TAILANDIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (213, 213, 'TAIWÁN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (214, 214, 'TAJIKISTAN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (215, 215, 'TANZANIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (216, 216, 'TOGO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (217, 217, 'TONGA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (218, 218, 'TOQUELAU', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (219, 219, 'TRINIDAD Y TOBAGO', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (220, 220, 'TURKMENISTAN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (221, 221, 'TURQUÍA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (222, 222, 'TUVALU', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (223, 223, 'TÚNEZ', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (224, 224, 'UCRANIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (225, 225, 'UGANDA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (226, 226, 'UNITED STATES MINOR OUTLYING I', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (227, 227, 'URUGUAY', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (228, 228, 'UZBEKISTAN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (229, 229, 'VANUATU', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (230, 230, 'VENEZUELA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (231, 231, 'VIETNAM', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (232, 232, 'VIRGIN ISLANDS BRITISH', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (233, 233, 'VIRGIN ISLANDS U.S.', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (234, 234, 'WESTERN SAHARA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (235, 235, 'YEMEN', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (236, 236, 'ZAIRE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (237, 237, 'ZAMBIA', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (238, 238, 'ZIMBABUE', true);
INSERT INTO top_paises (id, codigo, descripcion, activo) VALUES (239, 239, 'OTRO PAÍS', true);


--
-- TOC entry 2168 (class 0 OID 0)
-- Dependencies: 196
-- Name: top_paises_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('top_paises_id_seq', 1, false);


--
-- TOC entry 2132 (class 0 OID 1151789)
-- Dependencies: 197
-- Data for Name: top_parroquias; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1, 10101, 'ALTAGRACIA', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (2, 10102, 'ANTÍMANO', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (3, 10103, 'CANDELARIA', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (4, 10104, 'CARICUAO', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (5, 10105, 'CATEDRAL', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (6, 10106, 'COCHE', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (7, 10107, 'EL JUNQUITO', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (8, 10108, 'EL PARAÍSO', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (9, 10109, 'EL RECREO', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (10, 10110, 'EL VALLE', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (11, 10111, 'LA PASTORA', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (12, 10112, 'LA VEGA', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (13, 10113, 'MACARAO', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (14, 10114, 'SAN AGUSTÍN', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (15, 10115, 'SAN BERNARDINO', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (16, 10116, 'SAN JOSÉ', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (17, 10117, 'SAN JUAN', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (18, 10118, 'SAN PEDRO', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (19, 10119, 'SANTA ROSALÍA', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (20, 10120, 'SANTA TERESA', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (21, 10121, 'SUCRE', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (22, 10122, '23 DE ENERO', 1, 10100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (23, 20101, 'HUACHAMACARE', 2, 20100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (24, 20102, 'MARAWAKA', 2, 20100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (25, 20103, 'MAVACA', 2, 20100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (26, 20104, 'SIERRA PARIMA', 2, 20100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (27, 20201, 'UCATA', 2, 20200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (28, 20202, 'YAPACANA', 2, 20200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (29, 20203, 'CANAME', 2, 20200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (30, 20301, 'FERNANDO GIRÓN TOVAR', 2, 20300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (31, 20302, 'LUIS ALBERTO GÓMEZ', 2, 20300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (32, 20303, 'PARHUEÑA', 2, 20300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (33, 20304, 'PLATANILLA', 2, 20300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (34, 20401, 'SAMARIAPO', 2, 20400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (35, 20402, 'SIPAPO', 2, 20400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (36, 20403, 'MUNDUAPO', 2, 20400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (37, 20404, 'GUAYAPO', 2, 20400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (38, 20501, 'VICTORINO', 2, 20500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (39, 20502, 'COMUNIDAD', 2, 20500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (40, 20601, 'ALTO VENTUARI', 2, 20600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (41, 20602, 'MEDIO VENTUARI', 2, 20600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (42, 20603, 'BAJO VENTUARI', 2, 20600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (43, 20701, 'SOLANO', 2, 20700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (44, 20702, 'CASIQUIARE', 2, 20700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (45, 20703, 'COCUY', 2, 20700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (46, 30101, 'CAPITAL ANACO', 3, 30100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (47, 30102, 'SAN JOAQUÍN', 3, 30100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (48, 30201, 'CAPITAL ARAGUA', 3, 30200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (49, 30202, 'CACHIPO', 3, 30200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (50, 30301, 'CAPITAL FERNANDO DE PEÑALVER', 3, 30300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (51, 30302, 'SAN MIGUEL', 3, 30300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (52, 30303, 'SUCRE', 3, 30300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (53, 30401, 'CAPITAL FRANCISCO DEL CARMEN CARVAJAL', 3, 30400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (54, 30402, 'SANTA BÁRBARA', 3, 30400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (55, 30501, 'CAPITAL FRANCISCO DE MIRANDA', 3, 30500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (56, 30502, 'ATAPIRIRE', 3, 30500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (57, 30503, 'BOCA DEL PAO', 3, 30500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (58, 30504, 'EL PAO', 3, 30500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (59, 30505, 'MÁCURA', 3, 30500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (60, 30601, 'CAPITAL GUANTA', 3, 30600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (61, 30602, 'CHORRERÓN', 3, 30600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (62, 30701, 'CAPITAL INDEPENDENCIA', 3, 30700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (63, 30702, 'MAMO', 3, 30700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (64, 30801, 'CAPITAL PUERTO LA CRUZ', 3, 30800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (65, 30802, 'POZUELOS', 3, 30800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (66, 30901, 'CAPITAL JUAN MANUEL CAJIGAL', 3, 30900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (67, 30902, 'SAN PABLO', 3, 30900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (68, 31001, 'CAPITAL JOSÉ GREGORIO MONAGAS', 3, 31000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (69, 31002, 'PIAR', 3, 31000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (70, 31003, 'SAN DIEGO DE CABRUTICA', 3, 31000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (71, 31004, 'SANTA CLARA', 3, 31000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (72, 31005, 'UVERITO', 3, 31000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (73, 31006, 'ZUATA', 3, 31000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (74, 31101, 'CAPITAL LIBERTAD', 3, 31100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (75, 31102, 'EL CARITO', 3, 31100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (76, 31103, 'SANTA INÉS', 3, 31100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (77, 31201, 'CAPITAL MANUEL EZEQUIEL BRUZUAL', 3, 31200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (78, 31202, 'GUANAPE', 3, 31200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (79, 31203, 'SABANA DE UCHIRE', 3, 31200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (80, 31301, 'CAPITAL PEDRO MARÍA FREITES', 3, 31300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (81, 31302, 'LIBERTADOR', 3, 31300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (82, 31303, 'SANTA ROSA', 3, 31300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (83, 31304, 'URICA', 3, 31300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (84, 31401, 'CAPITAL PÍRITU', 3, 31400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (85, 31402, 'SAN FRANCISCO', 3, 31400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (86, 31501, 'CM SAN JOSÉ DE GUANIPA', 3, 31500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (87, 31601, 'CAPITAL SAN JUAN DE CAPISTRANO', 3, 31600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (88, 31602, 'BOCA DE CHÁVEZ', 3, 31600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (89, 31701, 'CAPITAL SANTA ANA', 3, 31700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (90, 31702, 'PUEBLO NUEVO', 3, 31700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (91, 31801, 'EL CARMEN', 3, 31800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (92, 31802, 'SAN CRISTÓBAL', 3, 31800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (93, 31803, 'BERGANTÍN', 3, 31800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (94, 31804, 'CAIGUA', 3, 31800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (95, 31805, 'EL PILAR', 3, 31800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (96, 31806, 'NARICUAL', 3, 31800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (97, 31901, 'EDMUNDO BARRIOS', 3, 31900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (98, 31902, 'MIGUEL OTERO SILVA', 3, 31900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (99, 32001, 'CAPITAL SIR ARTHUR MC GREGOR', 3, 32000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (100, 32002, 'TOMAS ALFARO CALATRAVA', 3, 32000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (101, 32101, 'CAPITAL DIEGO BAUTISTA URBANEJA', 3, 32100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (102, 32102, 'EL MORRO', 3, 32100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (103, 40101, 'URBANA ACHAGUAS', 4, 40100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (104, 40102, 'APURITO', 4, 40100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (105, 40103, 'EL YAGUAL', 4, 40100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (106, 40104, 'GUACHARA', 4, 40100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (107, 40105, 'MUCURITAS', 4, 40100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (108, 40106, 'QUESERAS DEL MEDIO', 4, 40100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (109, 40201, 'URBANA BIRUACA', 4, 40200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (110, 40301, 'URBANA BRUZUAL', 4, 40300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (111, 40302, 'MANTECAL', 4, 40300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (112, 40303, 'QUINTERO', 4, 40300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (113, 40304, 'RINCÓN HONDO', 4, 40300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (114, 40305, 'SAN VICENTE', 4, 40300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (115, 40401, 'URBANA GUASDUALITO', 4, 40400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (116, 40402, 'ARISMENDI', 4, 40400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (117, 40403, 'EL AMPARO', 4, 40400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (118, 40404, 'SAN CAMILO', 4, 40400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (119, 40405, 'URDANETA', 4, 40400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (120, 40501, 'URBANA SAN JUAN DE PAYARA', 4, 40500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (121, 40502, 'CODAZZI', 4, 40500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (122, 40503, 'CUNAVICHE', 4, 40500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (123, 40601, 'URBANA ELORZA', 4, 40600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (124, 40602, 'LA TRINIDAD', 4, 40600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (125, 40701, 'URBANA SAN FERNANDO', 4, 40700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (126, 40702, 'EL RECREO', 4, 40700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (127, 40703, 'PEÑALVER', 4, 40700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (128, 40704, 'SAN RAFAEL DE ATAMAICA', 4, 40700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (129, 50101, 'CM SAN MATEO', 5, 50100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (130, 50201, 'CAMATAGUA', 5, 50200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (131, 50202, 'CARMEN DE CURA', 5, 50200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (132, 50301, 'CHORONÍ', 5, 50300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (133, 50302, 'ANDRÉS ELOY BLANCO', 5, 50300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (134, 50303, 'JOAQUÍN CRESPO', 5, 50300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (135, 50304, 'JOSÉ CASANOVA GODOY', 5, 50300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (136, 50305, 'LAS DELICIAS', 5, 50300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (137, 50306, 'LOS TACARIGUAS', 5, 50300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (138, 50307, 'MADRE MARÍA DE SAN JOSÉ', 5, 50300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (139, 50308, 'JOSÉ PEDRO OVALLES', 5, 50300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (140, 50401, 'CM SANTA CRUZ', 5, 50400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (141, 50501, 'CASTOR NIENES RÍOS', 5, 50500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (142, 50502, 'LA GUACAMAYA', 5, 50500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (143, 50503, 'PAO DE ZÁRATE', 5, 50500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (144, 50504, 'URBANA ZUATA', 5, 50500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (145, 50505, 'JUAN VICENTE BOLÍVAR', 5, 50500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (146, 50601, 'CM EL CONSEJO', 5, 50600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (147, 50701, 'LIBERTADOR', 5, 50700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (148, 50702, 'SAN MARTÍN DE PORRAS', 5, 50700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (149, 50801, 'CAÑA DE AZÚCAR', 5, 50800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (150, 50802, 'MARIO BRICEÑO IRAGORRY', 5, 50800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (151, 50901, 'GÜIRIPA', 5, 50900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (152, 50902, 'OLLAS DE CARAMACATE', 5, 50900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (153, 50903, 'VALLE MORÍN', 5, 50900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (154, 50904, 'SAN CASIMIRO', 5, 50900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (155, 51101, 'CM SAN SEBASTIÁN', 5, 51000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (156, 51101, 'ALFREDO PACHECO MIRANDA', 5, 51100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (157, 51102, 'ARÉVALO APONTE', 5, 51100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (158, 51103, 'CHUAO', 5, 51100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (159, 51104, 'SAMÁN DE GUERE', 5, 51100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (160, 51105, 'SANTIGO MARIÑO', 5, 51100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (161, 51201, 'TIARA', 5, 51200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (162, 51202, 'SANTOS MICHELENA', 5, 51200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (163, 51301, 'BELLA VISTA', 5, 51300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (164, 51302, 'SUCRE', 5, 51300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (165, 51401, 'LA COLONIA TOVAR', 5, 51400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (166, 51501, 'LAS PEÑITAS', 5, 51500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (167, 51502, 'SAN FRANCISCO DE CARA', 5, 51500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (168, 51503, 'TAGUAY', 5, 51500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (169, 51504, 'URDANETA', 5, 51500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (170, 51601, 'AUGUSTO MIJARES', 5, 51600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (171, 51602, 'SAN FRANCISCO DE ASÍS', 5, 51600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (172, 51603, 'VALLES DE TUCUNEMO', 5, 51600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (173, 51604, 'MAGDALENO', 5, 51600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (174, 51605, 'ZAMORA', 5, 51600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (175, 51701, 'MONSEÑOR FELICIANO GONZÁLEZ', 5, 51700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (176, 51702, 'FRANCISCO LINARES ALCÁNTARA', 5, 51700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (177, 51703, 'FRANCISCO DE MIRANDA', 5, 51700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (178, 51801, 'OCUMARE DE LA COSTA', 5, 51800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (179, 60101, 'SABANETA', 6, 60100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (180, 60102, 'RODRÍGUEZ DOMÍNGUEZ', 6, 60100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (181, 60201, 'TICOPORO', 6, 60200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (182, 60202, 'ANDRÉS BELLO', 6, 60200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (183, 60203, 'NICOLÁS PULIDO', 6, 60200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (184, 60301, 'ARISMENDI', 6, 60300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (185, 60302, 'GUADARRAMA', 6, 60300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (186, 60303, 'LA UNIÓN', 6, 60300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (187, 60304, 'SAN ANTONIO', 6, 60300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (188, 60401, 'BARINAS', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (189, 60402, 'ALFREDO ARVELO LARRIVA', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (190, 60403, 'SAN SILVESTRE', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (191, 60404, 'SANTA INÉS', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (192, 60405, 'SANTA LUCÍA', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (193, 60406, 'TORUNOS', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (194, 60407, 'EL CARMEN', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (195, 60408, 'RÓMULO BETANCOURT', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (196, 60409, 'CORAZÓN DE JESÚS', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (197, 60410, 'RAMÓN IGNACIO MÉNDEZ', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (198, 60411, 'ALTO BARINAS', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (199, 60412, 'MANUEL PALACIO FAJARDO', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (200, 60413, 'JUAN ANTONIO RODRÍGUEZ DOMÍNGUEZ', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (201, 60414, 'DOMINGA ORTIZ DE PÁEZ', 6, 60400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (202, 60501, 'BARINITAS', 6, 60500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (203, 60502, 'ALTAMIRA', 6, 60500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (204, 60503, 'CALDERAS', 6, 60500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (205, 60601, 'BARRANCAS', 6, 60600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (206, 60602, 'EL SOCORRO', 6, 60600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (207, 60603, 'MASPARRITO', 6, 60600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (208, 60701, 'SANTA BÁRBARA', 6, 60700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (209, 60702, 'JOSÉ IGNACIO DEL PUMAR', 6, 60700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (210, 60703, 'PEDRO BRICEÑO MÉNDEZ', 6, 60700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (211, 60704, 'RAMÓN IGNACIO MÉNDEZ', 6, 60700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (212, 60801, 'OBISPOS', 6, 60800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (213, 60802, 'EL REAL', 6, 60800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (214, 60803, 'LA LUZ', 6, 60800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (215, 60804, 'LOS GUASIMITOS', 6, 60800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (216, 60901, 'CIUDAD BOLIVIA', 6, 60900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (217, 60902, 'IGNACIO BRICEÑO', 6, 60900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (218, 60903, 'JOSÉ FÉLIX RIBAS', 6, 60900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (219, 60904, 'PÁEZ', 6, 60900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (220, 61001, 'LIBERTAD', 6, 61000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (221, 61002, 'DOLORES', 6, 61000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (222, 61003, 'PALACIOS FAJARDO', 6, 61000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (223, 61004, 'SANTA ROSA', 6, 61000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (224, 61005, 'SIMÓN RODRÍGUEZ', 6, 61000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (225, 61101, 'CIUDAD DE NUTRIAS', 6, 61100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (226, 61102, 'EL REGALO', 6, 61100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (227, 61103, 'PUERTO DE NUTRIAS', 6, 61100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (228, 61104, 'SANTA CATALINA', 6, 61100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (229, 61105, 'SIMÓN BOLÍVAR', 6, 61100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (230, 61201, 'EL CANTÓN', 6, 61200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (231, 61202, 'SANTA CRUZ DE GUACAS', 6, 61200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (232, 61203, 'PUERTO VIVAS', 6, 61200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (233, 70101, 'CACHAMAY', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (234, 70102, 'CHIRICA', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (235, 70103, 'DALLA COSTA', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (236, 70104, 'ONCE DE ABRIL', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (237, 70105, 'SIMÓN BOLÍVAR', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (238, 70106, 'UNARE', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (239, 70107, 'UNIVERSIDAD', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (240, 70108, 'VISTA AL SOL', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (241, 70109, 'POZO VERDE', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (242, 70110, 'YOCOIMA', 7, 70100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (243, 70201, 'SECCIÓN CAPITAL CEDEÑO', 7, 70200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (244, 70202, 'ALTAGRACIA', 7, 70200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (245, 70203, 'ASCENSIÓN FARRERAS', 7, 70200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (246, 70204, 'GUANIAMO', 7, 70200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (247, 70205, 'LA URBANA', 7, 70200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (248, 70206, 'PIJIGUAOS', 7, 70200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (249, 70401, 'SECCIÓN CAPITAL GRAN SABANA', 7, 70400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (250, 70402, 'IKABARÚ', 7, 70400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (251, 70501, 'AGUA SALADA', 7, 70500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (252, 70502, 'CATEDRAL', 7, 70500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (253, 70503, 'JOSÉ ANTONIO PÁEZ', 7, 70500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (254, 70504, 'LA SABANITA', 7, 70500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (255, 70505, 'MARHUANTA', 7, 70500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (256, 70506, 'VISTA HERMOSA', 7, 70500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (257, 70507, 'ORINOCO', 7, 70500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (258, 70508, 'PANAPANA', 7, 70500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (259, 70509, 'ZEA', 7, 70500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (260, 70601, 'SECCIÓN CAPITAL PIAR', 7, 70600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (261, 70602, 'ANDRÉS ELOY BLANCO', 7, 70600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (262, 70603, 'PEDRO COVA', 7, 70600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (263, 70701, 'SECCIÓN CAPITAL RAÚL LEONI', 7, 70700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (264, 70702, 'BARCELONETA', 7, 70700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (265, 70703, 'SAN FRANCISCO', 7, 70700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (266, 70704, 'SANTA BÁRBARA', 7, 70700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (267, 70801, 'SECCIÓN CAPITAL ROSCIO', 7, 70800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (268, 70802, 'SALOM', 7, 70800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (269, 70901, 'SECCIÓN CAPITAL SIFONTES', 7, 70900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (270, 70902, 'DALLA COSTA', 7, 70900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (271, 70903, 'SAN ISIDRO', 7, 70900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (272, 71001, 'SECCIÓN CAPITAL SUCRE', 7, 71000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (273, 71002, 'ARIPAO', 7, 71000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (274, 71003, 'GUARATARO', 7, 71000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (275, 71004, 'LAS MAJADAS', 7, 71000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (276, 71005, 'MOITACO', 7, 71000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (277, 80101, 'URBANA BEJUMA', 8, 80100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (278, 80102, 'NO URBANA CANOABO', 8, 80100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (279, 80103, 'NO URBANA SIMÓN BOLÍVAR', 8, 80100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (280, 80201, 'URBANA GUIGUE', 8, 80200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (281, 80202, 'NO URBANA BELÉN', 8, 80200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (282, 80203, 'NO URBANA TACARIGUA', 8, 80200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (283, 80301, 'URBANA AGUAS CALIENTES', 8, 80300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (284, 80302, 'URBANA MARIARA', 8, 80300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (285, 80401, 'URBANA CIUDAD ALIANZA', 8, 80400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (286, 80402, 'URBANA GUACARA', 8, 80400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (287, 80403, 'NO URBANA YAGUA', 8, 80400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (288, 80501, 'URBANA MORÓN', 8, 80500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (289, 80502, 'NO URBANA URAMA', 8, 80500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (290, 80601, 'URBANA TOCUYITO', 8, 80600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (291, 80602, 'URBANA INDEPENDENCIA', 8, 80600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (292, 80701, 'URBANA LOS GUAYOS', 8, 80700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (293, 80801, 'URBANA MIRANDA', 8, 80800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (294, 80901, 'URBANA MONTALBÁN', 8, 80900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (295, 81001, 'URBANA NAGUANAGUA', 8, 81000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (296, 81101, 'URBANA BARTOLOMÉ SALOM', 8, 81100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (297, 81102, 'URBANA DEMOCRACIA', 8, 81100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (298, 81103, 'URBANA FRATERNIDAD', 8, 81100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (299, 81104, 'URBANA GOAIGOAZA', 8, 81100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (300, 81105, 'URBANA JUAN JOSÉ FLORES', 8, 81100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (301, 81106, 'URBANA UNIÓN', 8, 81100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (302, 81107, 'NO URBANA BORBURATA', 8, 81100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (303, 81108, 'NO URBANA PATANEMO', 8, 81100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (304, 81201, 'URBANA SAN DIEGO', 8, 81200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (305, 81301, 'URBANA SAN JOAQUÍN', 8, 81300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (306, 81401, 'URBANA CANDELARIA', 8, 81400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (307, 81402, 'URBANA CATEDRAL', 8, 81400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (308, 81403, 'URBANA EL SOCORRO', 8, 81400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (309, 81404, 'URBANA MIGUEL PEÑA', 8, 81400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (310, 81405, 'URBANA RAFAEL URDANETA', 8, 81400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (311, 81406, 'URBANA SAN BLAS', 8, 81400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (312, 81407, 'URBANA SAN JOSÉ', 8, 81400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (313, 81408, 'URBANA SANTA ROSA', 8, 81400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (314, 81409, 'NO URBANA NEGRO PRIMERO', 8, 81400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (315, 90101, 'COJEDES', 9, 90100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (316, 90102, 'JUAN DE MATA SUÁREZ', 9, 90100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (317, 90201, 'TINAQUILLO', 9, 90200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (318, 90301, 'EL BAÚL', 9, 90300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (319, 90302, 'SUCRE', 9, 90300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (320, 90401, 'MACAPO', 9, 90400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (321, 90402, 'LA AGUADITA', 9, 90400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (322, 90501, 'EL PAO', 9, 90500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (323, 90601, 'LIBERTAD DE COJEDES', 9, 90600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (324, 90602, 'EL AMPARO', 9, 90600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (325, 90701, 'RÓMULO GALLEGOS', 9, 90700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (326, 90801, 'SAN CARLOS DE AUSTRIA', 9, 90800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (327, 90802, 'JUAN ÁNGEL BRAVO', 9, 90800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (328, 90803, 'MANUEL MANRIQUE', 9, 90800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (329, 90901, 'GENERAL EN JEFE JOSÉ LAURENCIO SILVA', 9, 90900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (330, 100101, 'CURIAPO', 10, 100100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (331, 100102, 'ALMIRANTE LUIS BRIÓN', 10, 100100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (332, 100103, 'FRANCISCO ANICETO LUGO', 10, 100100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (333, 100104, 'MANUEL RENAUD', 10, 100100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (334, 100105, 'PADRE BARRAL', 10, 100100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (335, 100106, 'SANTOS DE ABELGAS', 10, 100100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (336, 100201, 'IMATACA', 10, 100200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (337, 100202, 'CINCO DE JULIO', 10, 100200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (688, 160803, 'BOQUERÓN', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (338, 100203, 'JUAN BAUTISTA ARISMENDI', 10, 100200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (339, 100204, 'MANUEL PIAR', 10, 100200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (340, 100205, 'RÓMULO GALLEGOS', 10, 100200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (341, 100301, 'PEDERNALES', 10, 100300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (342, 100302, 'LUIS BELTRÁN PRIETO FIGUEROA', 10, 100300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (343, 100401, 'SAN JOSÉ', 10, 100400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (344, 100402, 'JOSÉ VIDAL MARCANO', 10, 100400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (345, 100403, 'JUAN MILLÁN', 10, 100400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (346, 100404, 'LEONARDO RUÍZ PINEDA', 10, 100400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (347, 100405, 'MARISCAL ANTONIO JOSÉ DE SUCRE', 10, 100400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (348, 100406, 'MONSEÑOR ARGIMIRO GARCÍA', 10, 100400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (349, 100407, 'SAN RAFAEL', 10, 100400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (350, 100408, 'VIRGEN DEL VALLE', 10, 100400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (351, 110101, 'SAN JUAN DE LOS CAYOS', 11, 110100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (352, 110102, 'CAPADARE', 11, 110100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (353, 110103, 'LA PASTORA', 11, 110100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (354, 110104, 'LIBERTADOR', 11, 110100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (355, 110201, 'SAN LUIS', 11, 110200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (356, 110202, 'ARACUA', 11, 110200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (357, 110203, 'LA PEÑA', 11, 110200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (358, 110301, 'CAPATÁRIDA', 11, 110300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (359, 110302, 'BARIRO', 11, 110300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (360, 110303, 'BOROJÓ', 11, 110300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (361, 110304, 'GUAJIRO', 11, 110300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (362, 110305, 'SEQUE', 11, 110300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (363, 110306, 'ZAZÁRIDA', 11, 110300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (364, 110401, 'CM YARACAL', 11, 110400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (365, 110501, 'CARIRUBANA', 11, 110500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (366, 110502, 'NORTE', 11, 110500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (367, 110503, 'PUNTA CARDÓN', 11, 110500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (368, 110504, 'SANTA ANA', 11, 110500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (369, 110601, 'LA VELA DE CORO', 11, 110600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (370, 110602, 'ACURIGUA', 11, 110600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (371, 110603, 'GUAIBACOA', 11, 110600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (372, 110604, 'LAS CALDERAS', 11, 110600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (373, 110605, 'MACORUCA', 11, 110600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (374, 110701, 'CM DABAJURO', 11, 110700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (375, 110801, 'PEDREGAL', 11, 110800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (376, 110802, 'AGUA CLARA', 11, 110800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (377, 110803, 'AVARIA', 11, 110800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (378, 110804, 'PIEDRA GRANDE', 11, 110800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (379, 110805, 'PURURECHE', 11, 110800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (380, 110901, 'PUEBLO NUEVO', 11, 110900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (381, 110902, 'ADÍCORA', 11, 110900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (382, 110903, 'BARAIVED', 11, 110900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (383, 110904, 'BUENA VISTA', 11, 110900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (384, 110905, 'JADACAQUIVA', 11, 110900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (385, 110906, 'MORUY', 11, 110900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (386, 110907, 'ADAURE', 11, 110900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (387, 110908, 'EL HATO', 11, 110900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (388, 110909, 'EL VÍNCULO', 11, 110900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (389, 111001, 'CHURUGUARA', 11, 111000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (390, 111002, 'AGUA LARGA', 11, 111000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (391, 111003, 'EL PAUJÍ', 11, 111000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (392, 111004, 'INDEPENDENCIA', 11, 111000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (393, 111005, 'MAPARARÁ', 11, 111000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (394, 111101, 'JACURA', 11, 111000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (395, 111102, 'AGUA LINDA', 11, 111000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (396, 111103, 'ARAURIMA', 11, 111000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (397, 111101, 'AGUA LINDA', 11, 111100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (398, 111102, 'ARAURIMA', 11, 111100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (399, 111103, 'JACURA', 11, 111100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (400, 111201, 'LOS TAQUES', 11, 111200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (401, 111202, 'JUDIBANA', 11, 111200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (402, 111301, 'MENE DE MAUROA', 11, 111300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (403, 111302, 'CASIGUA', 11, 111300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (404, 111303, 'SAN FÉLIX', 11, 111300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (405, 111401, 'SAN ANTONIO', 11, 111400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (406, 111402, 'SAN GABRIEL', 11, 111400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (407, 111403, 'SANTA ANA', 11, 111400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (408, 111404, 'GUZMÁN GUILLERMO', 11, 111400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (409, 111405, 'MITARE', 11, 111400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (410, 111406, 'RÍO SECO', 11, 111400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (411, 111407, 'SABANETA', 11, 111400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (412, 111501, 'CHICHIRIVICHE', 11, 111500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (413, 111502, 'BOCA DE TOCUYO', 11, 111500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (414, 111503, 'TOCUYO DE LA COSTA', 11, 111500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (415, 111701, 'CABURÉ', 11, 111700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (416, 111702, 'COLINA', 11, 111700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (417, 111703, 'CURIMAGUA', 11, 111700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (418, 111801, 'PÍRITU', 11, 111800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (419, 111802, 'SAN JOSÉ DE LA COSTA', 11, 111800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (420, 112001, 'TUCACAS', 11, 112000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (421, 112002, 'BOCA DE AROA', 11, 112000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (422, 112101, 'SUCRE', 11, 112100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (423, 112102, 'PECAYA', 11, 112100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (424, 112301, 'SANTA CRUZ DE BUCARAL', 11, 112300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (425, 112302, 'EL CHARAL', 11, 112300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (426, 112303, 'LAS VEGAS DEL TUY', 11, 112300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (427, 112401, 'URUMACO', 11, 112400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (428, 112402, 'BRUZUAL', 11, 112400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (429, 112501, 'PUERTO CUMAREBO', 11, 112500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (430, 112502, 'LA CIÁNAGA', 11, 112500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (431, 112503, 'LA SOLEDAD', 11, 112500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (432, 112504, 'PUEBLO CUMAREBO', 11, 112500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (433, 112505, 'ZAZÁRIDA', 11, 112500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (434, 120101, 'CAPITAL CAMAGUÁN', 12, 120100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (435, 120102, 'PUERTO MIRANDA', 12, 120100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (436, 120103, 'UVERITO', 12, 120100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (437, 120201, 'CHAGUARAMAS', 12, 120200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (438, 120301, 'EL SOCORRO', 12, 120300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (439, 120401, 'CAPITAL SAN GERÓNIMO DE GUAYABAL', 12, 120400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (440, 120402, 'CAZORLA', 12, 120400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (441, 120501, 'CAPITAL VALLE DE LA PASCUA', 12, 120500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (442, 120502, 'ESPINO', 12, 120500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (443, 120601, 'CAPITAL LAS MERCEDES', 12, 120600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (444, 120602, 'CABRUTA', 12, 120600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (445, 120603, 'SANTA RITA DE MANAPIRE', 12, 120600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (446, 120701, 'CAPITAL EL SOMBRERO', 12, 120700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (447, 120702, 'SOSA', 12, 120700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (448, 120801, 'CAPITAL CALABOZO', 12, 120800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (449, 120802, 'EL CALVARIO', 12, 120800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (450, 120803, 'EL RASTRO', 12, 120800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (451, 120804, 'GUARDATINAJAS', 12, 120800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (452, 141213, 'SAGRARIO', 12, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (453, 120901, 'CAPITAL ALTAGRACIA DE ORITUCO', 12, 120900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (454, 120902, 'LEZAMA', 12, 120900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (455, 120903, 'LIBERTAD DE ORITUCO', 12, 120900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (456, 120904, 'PASO REAL DE MACAIRA', 12, 120900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (457, 120905, 'SAN FRANCISCO DE MACAIRA', 12, 120900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (458, 120906, 'SAN RAFAEL DE ORITUCO', 12, 120900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (459, 120907, 'SOUBLETTE', 12, 120900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (460, 121001, 'CAPITAL ORTIZ', 12, 121000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (461, 121002, 'SAN FRANCISCO DE TIZNADO', 12, 121000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (462, 121003, 'SAN JOSÉ DE TIZNADO', 12, 121000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (463, 121004, 'SAN LORENZO DE TIZNADO', 12, 121000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (464, 121101, 'CAPITAL TUCUPIDO', 12, 121100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (465, 121102, 'SAN RAFAEL DE LAYA', 12, 121100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (466, 121201, 'CAPITAL SAN JUAN DE LOS MORROS', 12, 121200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (467, 121202, 'CANTAGALLO', 12, 121200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (468, 121203, 'PARAPARA', 12, 121200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (469, 121301, 'SAN JOSÉ DE GUARIBE', 12, 121300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (470, 121401, 'CAPITAL SANTA MARÍA DE IPIRE', 12, 121400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (471, 121402, 'ALTAMIRA', 12, 121400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (472, 121501, 'CAPITAL ZARAZA', 12, 121500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (473, 121502, 'SAN JOSÉ DE UNARE', 12, 121500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (474, 130101, 'PAO TAMAYO', 13, 130100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (475, 130102, 'QUEBRADA HONDA DE GUACHE', 13, 130100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (476, 130103, 'YACAMBÚ', 13, 130100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (477, 130201, 'FRÉITEZ', 13, 130200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (478, 130202, 'JOSÉ MARÍA BLANCO', 13, 130200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (479, 130301, 'CATEDRAL', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (480, 130302, 'CONCEPCIÓN', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (481, 130303, 'EL CUJÍ', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (482, 130304, 'JUAN DE VILLEGAS', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (483, 130305, 'SANTA ROSA', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (484, 130306, 'TAMACA', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (485, 130307, 'UNIÓN', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (486, 130308, 'AGUEDO FELIPE ALVARADO', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (487, 130309, 'BUENA VISTA', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (488, 130310, 'JUÁREZ', 13, 130300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (489, 130401, 'JUAN BAUTISTA RODRÍGUEZ', 13, 130400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (490, 130402, 'CUARA', 13, 130400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (491, 130403, 'DIEGO DE LOZADA', 13, 130400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (492, 130404, 'PARAÍSO DE SAN JOSÉ', 13, 130400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (493, 130405, 'SAN MIGUEL', 13, 130400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (494, 130406, 'TINTORERO', 13, 130400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (495, 130407, 'JOSÉ BERNARDO DORANTE', 13, 130400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (496, 130408, 'CORONEL MARIANO PERAZA', 13, 130400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (497, 130501, 'BOLÍVAR', 13, 130500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (498, 130502, 'ANZOÁTEGUI', 13, 130500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (499, 130503, 'GUÁRICO', 13, 130500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (500, 130504, 'HILARIO LUNA Y LUNA', 13, 130500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (501, 130505, 'HUMOCARO ALTO', 13, 130500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (502, 130506, 'HUMOCARO BAJO', 13, 130500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (503, 130507, 'LA CANDELARIA', 13, 130500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (504, 130508, 'MORÁN', 13, 130500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (505, 130601, 'CABUDARE', 13, 130600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (506, 130602, 'JOSÉ GREGORIO BASTIDAS', 13, 130600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (507, 130603, 'AGUA VIVA', 13, 130600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (508, 130701, 'SARARE', 13, 130700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (509, 130702, 'BURÍA', 13, 130700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (510, 130703, 'GUSTAVO VEGAS LEÓN', 13, 130700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (511, 130801, 'TRINIDAD SAMUEL', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (512, 130802, 'ANTONIO DÍAZ', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (513, 130803, 'CAMACARO', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (514, 130804, 'CASTAÑEDA', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (515, 130805, 'CECILIO ZUBILLAGA', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (516, 130806, 'CHIQUINQUIRÁ', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (517, 130807, 'EL BLANCO', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (518, 130808, 'ESPINOZA DE LOS MONTEROS', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (519, 130809, 'LARA', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (520, 130810, 'LAS MERCEDES', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (521, 130811, 'MANUEL MORILLO', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (522, 130812, 'MONTAÑA VERDE', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (523, 130813, 'MONTES DE OCA', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (524, 130814, 'TORRES', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (525, 130815, 'HERIBERTO ARROYO', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (526, 130816, 'REYES VARGAS', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (527, 130817, 'ALTAGRACIA', 13, 130800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (528, 130901, 'SIQUISIQUE', 13, 130900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (529, 130902, 'MOROTURO', 13, 130900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (530, 130903, 'SAN MIGUEL', 13, 130900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (531, 130904, 'XAGUAS', 13, 130900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (532, 140101, 'PRESIDENTE BETANCOURT', 14, 140100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (533, 140102, 'PRESIDENTE PÁEZ', 14, 140100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (534, 140103, 'PRESIDENTE RÓMULO GALLEGOS', 14, 140100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (535, 140104, 'GABRIEL PICÓN GONZÁLEZ', 14, 140100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (536, 140105, 'HÉCTOR AMABLE MORA', 14, 140100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (537, 140106, 'JOSÉ NUCETE SARDI', 14, 140100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (538, 140107, 'PULIDO MÉNDEZ', 14, 140100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (539, 140301, 'CAPITAL ANTONIO PINTO SALINAS', 14, 140300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (540, 140302, 'MESA BOLÍVAR', 14, 140300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (541, 140303, 'MESA DE LAS PALMAS', 14, 140300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (542, 140401, 'CAPITAL ARICAGUA', 14, 140400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (543, 140402, 'SAN ANTONIO', 14, 140400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (544, 140501, 'CAPITAL ARZOBISPO CHACÓN', 14, 140500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (545, 140502, 'CAPURÍ', 14, 140500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (546, 140503, 'CHACANTÁ', 14, 140500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (547, 140504, 'EL MOLINO', 14, 140500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (548, 140505, 'GUAIMARAL', 14, 140500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (549, 140506, 'MUCUTUY', 14, 140500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (550, 140507, 'MUCUCHACHÍ', 14, 140500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (551, 140601, 'FERNÁNDEZ PEÑA', 14, 140600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (552, 140602, 'MATRIZ', 14, 140600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (553, 140603, 'MONTALBÁN', 14, 140600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (554, 140604, 'ACEQUIAS', 14, 140600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (555, 140605, 'JAJÍ', 14, 140600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (556, 140606, 'LA MESA', 14, 140600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (557, 140607, 'SAN JOSÉ DEL SUR', 14, 140600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (558, 140701, 'CAPITAL CARACCIOLO PARRA OLMEDO', 14, 140700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (559, 140702, 'FLORENCIO RAMÍREZ', 14, 140700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (560, 140801, 'CAPITAL CARDENAL QUINTERO', 14, 140800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (561, 140802, 'LAS PIEDRAS', 14, 140800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (562, 140901, 'CAPITAL GUARAQUE', 14, 140900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (563, 140902, 'MESA DE QUINTERO', 14, 140900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (564, 140903, 'RÍO NEGRO', 14, 140900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (565, 141001, 'CAPITAL JULIO CÉSAR SALAS', 14, 141000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (566, 141002, 'PALMIRA', 14, 141000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (567, 141101, 'CAPITAL JUSTO BRICEÑO', 14, 141000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (568, 141102, 'SAN CRISTÓBAL DE TORONDOY', 14, 141000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (569, 141201, 'ANTONIO SPINETTI DINI', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (570, 141202, 'ARIAS', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (571, 141203, 'CARACCIOLO PARRA PÉREZ', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (572, 141204, 'DOMINGO PEÑA', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (573, 141205, 'EL LLANO', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (574, 141206, 'GONZALO PICÓN FEBRES', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (575, 141207, 'JACINTO PLAZA', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (576, 141208, 'JUAN RODRÍGUEZ SUÁREZ', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (577, 141209, 'LASSO DE LA VEGA', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (578, 141210, 'MARIANO PICÓN SALAS', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (579, 141211, 'MILLA', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (580, 141212, 'OSUNA RODRÍGUEZ', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (581, 141214, 'EL MORRO', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (582, 141215, 'LOS NEVADOS', 14, 141200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (583, 141301, 'CAPITAL MIRANDA', 14, 141300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (584, 141302, 'ANDRÉS ELOY BLANCO', 14, 141300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (585, 141303, 'LA VENTA', 14, 141300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (586, 141304, 'PIÑANGO', 14, 141300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (587, 141401, 'CAPITAL OBISPO RAMOS DE LORA', 14, 141400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (588, 141402, 'ELOY PAREDES', 14, 141400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (589, 141403, 'SAN RAFAEL DE ALCÁZAR', 14, 141400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (590, 141701, 'CAPITAL RANGEL', 14, 141700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (591, 141702, 'CACUTE', 14, 141700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (592, 141703, 'LA TOMA', 14, 141700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (593, 141704, 'MUCURUBÍ', 14, 141700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (594, 141705, 'SAN RAFAEL', 14, 141700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (595, 141801, 'CAPITAL RIVAS DÁVILA', 14, 141800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (596, 141802, 'GERÓNIMO MALDONADO', 14, 141800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (597, 142001, 'CAPITAL SUCRE', 14, 142000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (598, 142002, 'CHIGUARÍ', 14, 142000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (599, 142003, 'ESTÁNQUEZ', 14, 142000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (600, 142004, 'LA TRAMPA', 14, 142000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (601, 142005, 'PUEBLO NUEVO DEL SUR', 14, 142000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (602, 142006, 'SAN JUAN', 14, 142000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (603, 142101, 'EL AMPARO', 14, 142100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (604, 142102, 'EL LLANO', 14, 142100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (605, 142103, 'SAN FRANCISCO', 14, 142100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (606, 142104, 'TOVAR', 14, 142100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (607, 142201, 'CAPITAL TULIO FEBRES CORDERO', 14, 142200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (608, 142202, 'INDEPENDENCIA', 14, 142200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (609, 142203, 'MARÍA DE LA CONCEPCIÓN PALACIOS BLANCO', 14, 142200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (610, 142204, 'SANTA APOLONIA', 14, 142200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (611, 142301, 'CAPITAL ZEA', 14, 142300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (612, 142302, 'CAÑO EL TIGRE', 14, 142300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (613, 150101, 'CAUCAGUA', 15, 150100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (614, 150102, 'ARAGUITA', 15, 150100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (615, 150103, 'ARÉVALO GONZÁLEZ', 15, 150100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (616, 150104, 'CAPAYA', 15, 150100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (617, 150105, 'EL CAFÉ', 15, 150100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (618, 150106, 'MARIZAPA', 15, 150100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (619, 150107, 'PANAQUIRE', 15, 150100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (620, 150108, 'RIBAS', 15, 150100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (621, 150201, 'SAN JOSÉ DE BARLOVENTO', 15, 150200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (622, 150202, 'CUMBO', 15, 150200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (623, 150301, 'BARUTA', 15, 150300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (624, 150302, 'EL CAFETAL', 15, 150300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (625, 150303, 'LAS MINAS DE BARUTA', 15, 150300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (626, 150401, 'HIGUEROTE', 15, 150400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (627, 150402, 'CURIEPE', 15, 150400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (628, 150403, 'TACARIGUA', 15, 150400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (629, 150501, 'MAMPORAL', 15, 150500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (630, 150601, 'CARRIZAL', 15, 150500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (631, 150701, 'CHACAO', 15, 150700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (632, 150801, 'CHARALLAVE', 15, 150800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (633, 150802, 'LAS BRISAS', 15, 150800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (634, 150901, 'EL HATILLO', 15, 150900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (635, 151001, 'LOS TEQUES', 15, 151000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (636, 151002, 'ALTAGRACIA DE LA MONTAÑA', 15, 151000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (637, 151003, 'CECILIO ACOSTA', 15, 151000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (638, 151004, 'EL JARILLO', 15, 151000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (639, 151005, 'PARACOTOS', 15, 151000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (640, 151006, 'SAN PEDRO', 15, 151000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (641, 151007, 'TÁCATA', 15, 151000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (642, 151101, 'SANTA TERESA DEL TUY', 15, 151100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (643, 151102, 'EL CARTANAL', 15, 151100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (644, 151201, 'OCUMARE DEL TUY', 15, 151200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (645, 151202, 'LA DEMOCRACIA', 15, 151200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (646, 151203, 'SANTA BÁRBARA', 15, 151200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (647, 151301, 'SAN ANTONIO DE LOS ALTOS', 15, 151300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (648, 151401, 'RÍO CHICO', 15, 151400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (649, 151402, 'EL GUAPO', 15, 151400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (650, 151403, 'TACARIGUA DE LA LAGUNA', 15, 151400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (651, 151404, 'PÁPARO', 15, 151400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (652, 151405, 'SAN FERNANDO DEL GUAPO', 15, 151400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (653, 151501, 'SANTA LUCÍA', 15, 151500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (654, 151601, 'CÚPIRA', 15, 151600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (655, 151602, 'MACHURUCUTO', 15, 151600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (656, 151701, 'GUARENAS', 15, 151700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (657, 151801, 'SAN FRANCISCO DE YARE', 15, 151800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (658, 151802, 'SAN ANTONIO DE YARE', 15, 151800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (659, 151901, 'PETARE', 15, 151900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (660, 151902, 'CAUCAGUITA', 15, 151900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (661, 151903, 'FILA DE MARICHES', 15, 151900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (662, 151904, 'LA DOLORITA', 15, 151900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (663, 151905, 'LEONCIO MARTÍNEZ', 15, 151900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (664, 152001, 'CÚA', 15, 152000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (665, 152002, 'NUEVA CÚA', 15, 152000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (666, 152101, 'GUATIRE', 15, 152100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (667, 152102, 'BOLÍVAR', 15, 152100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (668, 160101, 'CAPITAL ACOSTA', 16, 160100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (669, 160102, 'SAN FRANCISCO', 16, 160100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (670, 160401, 'CAPITAL CARIPE', 16, 160400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (671, 160402, 'EL GUÁCHARO', 16, 160400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (672, 160403, 'LA GUANOTA', 16, 160400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (673, 160404, 'SABANA DE PIEDRA', 16, 160400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (674, 160405, 'SAN AGUSTÍN', 16, 160400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (675, 160406, 'TERESÓN', 16, 160400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (676, 160501, 'CAPITAL CEDEÑO', 16, 160500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (677, 160502, 'AREO', 16, 160500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (678, 160503, 'SAN FÉLIX', 16, 160500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (679, 160504, 'VIENTO FRESCO', 16, 160500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (680, 160601, 'CAPITAL EZEQUIEL ZAMORA', 16, 160600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (681, 160602, 'EL TEJERO', 16, 160600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (682, 160701, 'CAPITAL LIBERTADOR', 16, 160700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (683, 160702, 'CHAGUARAMAS', 16, 160700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (684, 160703, 'LAS ALHUACAS', 16, 160700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (685, 160704, 'TABASCA', 16, 160700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (686, 160801, 'CAPITAL MATURÍN', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (687, 160802, 'ALTO DE LOS GODOS', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (689, 160804, 'LAS COCUIZAS', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (690, 160805, 'SAN SIMÓN', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (691, 160806, 'SANTA CRUZ', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (692, 160807, 'EL COROZO', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (693, 160808, 'EL FURRIAL', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (694, 160809, 'JUSEPÍN', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (695, 160810, 'LA PICA', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (696, 160811, 'SAN VICENTE', 16, 160800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (697, 160901, 'CAPITAL PIAR', 16, 160900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (698, 160902, 'APARICIO', 16, 160900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (699, 160903, 'CHAGUARAMAL', 16, 160900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (700, 160904, 'EL PINTO', 16, 160900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (701, 160905, 'GUANAGUANA', 16, 160900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (702, 160906, 'LA TOSCANA', 16, 160900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (703, 160907, 'TAGUAYA', 16, 160900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (704, 161001, 'CAPITAL PUNCERES', 16, 161000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (705, 161002, 'CACHIPO', 16, 161000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (706, 161101, 'CM SANTA BÁRBARA', 16, 161100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (707, 161201, 'CAPITAL SOTILLO', 16, 161200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (708, 161202, 'LOS BARRANCOS DE FAJARDO', 16, 161200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (709, 161301, 'CM URACOA', 16, 161300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (710, 170101, 'CM LA PLAZA DE PARAGUACHI', 17, 170100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (711, 170201, 'CM LA ASUNCIÓN', 17, 170200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (712, 170301, 'CAPITAL DÍAZ', 17, 170300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (713, 170302, 'ZABALA', 17, 170300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (714, 170401, 'CAPITAL GARCÍA', 17, 170400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (715, 170402, 'FRANCISCO FAJARDO', 17, 170400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (716, 170501, 'CAPITAL GÓMEZ', 17, 170500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (717, 170502, 'BOLÍVAR', 17, 170500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (718, 170503, 'GUEVARA', 17, 170500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (719, 170504, 'MATASIETE', 17, 170500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (720, 170505, 'SUCRE', 17, 170500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (721, 170601, 'CAPITAL MANEIRO', 17, 170600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (722, 170602, 'AGUIRRE', 17, 170600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (723, 170701, 'CAPITAL MARCANO', 17, 170700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (724, 170702, 'ADRIÁN', 17, 170700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (725, 170801, 'CM PORLAMAR', 17, 170800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (726, 170901, 'CAPITAL PENÍNSULA DE MACANAO', 17, 170900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (727, 170902, 'SAN FRANCISCO', 17, 170900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (728, 171001, 'CAPITAL TUBORES', 17, 171000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (729, 171002, 'LOS BARALES', 17, 171000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (730, 171101, 'CAPITAL VILLALBA', 17, 171100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (731, 171102, 'VICENTE FUENTES', 17, 171100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (732, 180101, 'CM AGUA BLANCA', 18, 180100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (733, 180201, 'CAPITAL ARAURE', 18, 180200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (734, 180202, 'RÍO ACARIGUA', 18, 180200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (735, 180301, 'CAPITAL ESTELLER', 18, 180300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (736, 180302, 'UVERAL', 18, 180300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (737, 180401, 'CAPITAL GUANARE', 18, 180400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (738, 180402, 'CÓRDOBA', 18, 180400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (739, 180403, 'SAN JOSÉ DE LA MONTAÑA', 18, 180400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (740, 180404, 'SAN JUAN DE GUANAGUANARE', 18, 180400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (741, 180405, 'VIRGEN DE LA COROMOTO', 18, 180400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (742, 180501, 'CAPITAL GUANARITO', 18, 180500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (743, 180502, 'TRINIDAD DE LA CAPILLA', 18, 180500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (744, 180503, 'DIVINA PASTORA', 18, 180500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (745, 180601, 'CAPITAL MONS. JOSÉ VICENTE DE UNDA', 18, 180600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (746, 180602, 'PEÑA BLANCA', 18, 180600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (747, 180701, 'CAPITAL OSPINO', 18, 180700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (748, 180702, 'APARICIÓN', 18, 180700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (749, 180703, 'LA ESTACIÓN', 18, 180700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (750, 180801, 'CAPITAL PÁEZ', 18, 180800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (751, 180802, 'PAYARA', 18, 180800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (752, 180803, 'PIMPINELA', 18, 180800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (753, 180804, 'RAMÓN PERAZA', 18, 180800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (754, 180901, 'CAPITAL PAPELÓN', 18, 180900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (755, 180902, 'CAÑO DELGADITO', 18, 180900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (756, 181001, 'CAPITAL SAN GENARO DE BOCONOITO', 18, 181000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (757, 181002, 'ANTOLÍN TOVAR', 18, 181000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (758, 181101, 'CAPITAL SAN RAFAEL DE ONOTO', 18, 181100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (759, 181102, 'SANTA FE', 18, 181100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (760, 181103, 'THERMO MORLÉS', 18, 181100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (761, 181201, 'CAPITAL SANTA ROSALÍA', 18, 181200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (762, 181202, 'FLORIDA', 18, 181200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (763, 181301, 'CAPITAL SUCRE', 18, 181300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (764, 181302, 'CONCEPCIÓN', 18, 181300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (765, 181303, 'SAN RAFAEL DE PALO ALZADO', 18, 181300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (766, 181304, 'UVENCIO ANTONIO VELÁSQUEZ', 18, 181300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (767, 181305, 'SAN JOSÉ DE SAGUAZ', 18, 181300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (768, 181306, 'VILLA ROSA', 18, 181300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (769, 181401, 'CAPITAL TURÍN', 18, 181400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (770, 181402, 'CANELONES', 18, 181400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (771, 181403, 'SANTA CRUZ', 18, 181400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (772, 181404, 'SAN ISIDRO LABRADOR', 18, 181400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (773, 190101, 'MARIÑO', 19, 190100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (774, 190102, 'RÓMULO GALLEGOS', 19, 190100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (775, 190201, 'SAN JOSÉ DE AEROCUAR', 19, 190200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (776, 190202, 'TAVERA ACOSTA', 19, 190200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (777, 190301, 'RÍO CARIBE', 19, 190300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (778, 190302, 'ANTONIO JOSÉ DE SUCRE', 19, 190300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (779, 190303, 'EL MORRO DE PUERTO SANTO', 19, 190300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (780, 190304, 'PUERTO SANTO', 19, 190300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (781, 190305, 'SAN JUAN DE LAS GALDONAS', 19, 190300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (782, 190401, 'EL PILAR', 19, 190400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (783, 190402, 'EL RINCÓN', 19, 190400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (784, 190403, 'GENERAL FRANCISCO ANTONIO VÁSQUEZ', 19, 190400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (785, 190404, 'GUARAINOS', 19, 190400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (786, 190405, 'TUNAPUICITO', 19, 190400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (787, 190406, 'UNIÓN', 19, 190400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (788, 190501, 'BOLÍVAR', 19, 190500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (789, 190502, 'MACARAPANA', 19, 190500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (790, 190503, 'SANTA CATALINA', 19, 190500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (791, 190504, 'SANTA ROSA', 19, 190500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (792, 190505, 'SANTA TERESA', 19, 190500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (793, 190601, 'CM MARIGUITAR', 19, 190600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (794, 190701, 'YAGUARAPARO', 19, 190700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (795, 190702, 'EL PAUJIL', 19, 190700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (796, 190703, 'LIBERTAD', 19, 190700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (797, 190801, 'ARAYA', 19, 190800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (798, 190802, 'CHACOPATA', 19, 190800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (799, 190803, 'MANICUARE', 19, 190800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (800, 190901, 'TUNAPUY', 19, 190900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (801, 190902, 'CAMPO ELÍAS', 19, 190900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (802, 191001, 'IRAPA', 19, 191000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (803, 191002, 'CAMPO CLARO', 19, 191000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (804, 191003, 'MARABAL', 19, 191000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1040, 230702, 'LA CONCEPCIÓN', 23, 230700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (805, 191004, 'SAN ANTONIO DE IRAPA', 19, 191000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (806, 191005, 'SORO', 19, 191000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (807, 191101, 'CM SAN ANTONIO DEL GOLFO', 19, 191100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (808, 191201, 'CUMANACOA', 19, 191200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (809, 191202, 'ARENAS', 19, 191200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (810, 191203, 'ARICAGUA', 19, 191200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (811, 191204, 'COCOLLAR', 19, 191200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (812, 191205, 'SAN FERNANDO', 19, 191200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (813, 191206, 'SAN LORENZO', 19, 191200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (814, 191301, 'VILLA FRONTADO (MUELLE DE CARIACO)', 19, 191300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (815, 191302, 'CATUARO', 19, 191300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (816, 191303, 'RENDÓN', 19, 191300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (817, 191304, 'SANTA CRUZ', 19, 191300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (818, 191305, 'SANTA MARÍA', 19, 191300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (819, 191401, 'ALTAGRACIA', 19, 191400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (820, 191402, 'AYACUCHO', 19, 191400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (821, 191403, 'SANTA INÉS', 19, 191400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (822, 191404, 'VALENTÍN VALIENTE', 19, 191400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (823, 191405, 'SAN JUAN', 19, 191400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (824, 191406, 'RAÚL LEONI', 19, 191400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (825, 191407, 'GRAN MARISCAL', 19, 191400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (826, 191501, 'GUIRIA', 19, 191500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (827, 191502, 'BIDEAU', 19, 191500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (828, 191503, 'CRISTÓBAL COLÓN', 19, 191500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (829, 191504, 'PUNTA DE PIEDRAS', 19, 191500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (830, 200101, 'CM CORDERO', 20, 200100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (831, 200201, ' CM LAS MESAS', 20, 200200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (832, 200301, 'AYACUCHO', 20, 200300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (833, 200302, 'RIVAS BERTI', 20, 200300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (834, 200303, 'SAN PEDRO DEL RÍO', 20, 200300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (835, 200401, 'BOLÍVAR', 20, 200400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (836, 200402, 'PALOTAL', 20, 200400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (837, 200403, 'JUAN VICENTE GÓMEZ', 20, 200400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (838, 200404, 'ISAÍAS MEDINA ANGARITA', 20, 200400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (839, 200501, 'CÁRDENAS', 20, 200500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (840, 200502, 'AMENODORO RANGEL LAMÚS', 20, 200500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (841, 200503, 'LA FLORIDA', 20, 200500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (842, 200601, 'CM SANTA ANA DEL TÁCHIRA', 20, 200600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (843, 200701, 'FERNÁNDEZ FEO', 20, 200700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (844, 200702, 'ALBERTO ADRIANI', 20, 200700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (845, 200703, 'SANTO DOMINGO', 20, 200700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (846, 200801, 'CM SAN JOSÉ DE BOLÍVAR', 20, 200800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (847, 200901, 'GARCÍA DE HEVIA', 20, 200900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (848, 200902, 'BOCA DE GRITA', 20, 200900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (849, 200903, 'JOSÉ ANTONIO PÁEZ', 20, 200900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (850, 201001, 'CM PALMIRA', 20, 201000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (851, 201101, 'INDEPENDENCIA', 20, 201100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (852, 201102, 'JUAN GERMÁN ROSCIO', 20, 201100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (853, 201103, 'ROMÁN CÁRDENAS', 20, 201100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (854, 201201, 'JÁUREGUI', 20, 201200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (855, 201202, 'EMILIO CONSTANTINO GUERRERO', 20, 201200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (856, 201203, 'MONSEÑOR MIGUEL ANTONIO SALAS', 20, 201200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (857, 201301, 'CM EL COBRE', 20, 201300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (858, 201401, 'JUNÍN', 20, 201400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (859, 201402, 'LA PETRÓLEA', 20, 201400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (860, 201403, 'QUINIMARÍ', 20, 201400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (861, 201404, 'BRAMÓN', 20, 201400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (862, 201501, 'LIBERTAD', 20, 201500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (863, 201502, 'CIPRIANO CASTRO', 20, 201500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (864, 201503, 'MANUEL FELIPE RUGELES', 20, 201500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (865, 201601, 'LIBERTADOR', 20, 201600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (866, 201602, 'DON EMETERIO OCHOA', 20, 201600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (867, 201603, 'DORADAS', 20, 201600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (868, 201604, 'SAN JOAQUÍN DE NAVAY', 20, 201600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (869, 201701, 'LOBATERA', 20, 201700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (870, 201702, 'CONSTITUCIÓN', 20, 201700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (871, 201801, ' CM MICHELENA', 20, 201800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (872, 201901, 'PANAMERICANO', 20, 201900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (873, 201902, 'LA PALMITA', 20, 201900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (874, 202001, 'PEDRO MARÍA UREÑA', 20, 202000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (875, 202002, 'NUEVA ARCADIA', 20, 202000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (876, 202201, 'SAMUEL DARÍO MALDONADO', 20, 202000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (877, 202202, 'BOCONÓ', 20, 202000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (878, 202203, 'HERNÁNDEZ', 20, 202000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (879, 202101, 'CM DELICIAS', 20, 202100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (880, 202201, 'CM LA TENDIDA', 20, 202200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (881, 202202, 'BOCONÓ', 20, 202200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (882, 202203, 'HERNÁNDEZ', 20, 202200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (883, 202301, 'LA CONCORDIA', 20, 202300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (884, 202302, 'PEDRO MARÍA MORANTES', 20, 202300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (885, 202303, 'SAN JUAN BAUTISTA', 20, 202300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (886, 202304, 'SAN SEBASTIÁN', 20, 202300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (887, 202401, 'CM SEBORUCO', 20, 202400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (888, 202305, 'DR. FRANCISCO ROMERO LOBO', 20, 202500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (889, 202601, 'SUCRE', 20, 202600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (890, 202602, 'ELEAZAR LÓPEZ CONTRERAS', 20, 202600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (891, 202603, 'SAN PABLO', 20, 202600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (892, 202701, 'CM SAN JOSECITO', 20, 202700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (893, 202801, ' URIBANTE', 20, 202800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (894, 202802, 'CÁRDENAS', 20, 202800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (895, 202803, 'JUAN PABLO PEÑALOZA', 20, 202800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (896, 202804, 'POTOSÍ', 20, 202800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (897, 202901, 'CM UMUQUENA', 20, 202900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (898, 210101, 'SANTA ISABEL', 21, 210100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (899, 210102, 'ARAGUANEY', 21, 210100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (900, 210103, 'EL JAGUITO', 21, 210100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (901, 210104, 'LA ESPERANZA', 21, 210100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (902, 210201, 'BOCONÓ', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (903, 210202, 'EL CARMEN', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (904, 210203, 'MOSQUEY', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (905, 210204, 'AYACUCHO', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (906, 210205, 'BURBUSAY', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (907, 210206, 'GENERAL RIVAS', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (908, 210207, 'GUARAMACAL', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (909, 210208, 'VEGA DE GUARAMACAL', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (910, 210209, 'MONSEÑOR JÁUREGUI', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (911, 210210, 'RAFAEL RANGEL', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (912, 210211, 'SAN MIGUEL', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (913, 210212, 'SAN JOSÉ', 21, 210200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (914, 210301, 'SABANA GRANDE', 21, 210300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (915, 210302, 'CHEREGUE', 21, 210300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (916, 210303, 'GRANADOS', 21, 210300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (917, 210401, 'CHEJENDÉ', 21, 210400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (918, 210402, 'ARNOLDO GABALDÓN', 21, 210400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (919, 210403, 'BOLIVIA', 21, 210400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (920, 210404, 'CARRILLO', 21, 210400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (921, 210405, 'CEGARRA', 21, 210400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (922, 210406, 'MANUEL SALVADOR ULLOA', 21, 210400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (923, 210407, 'SAN JOSÉ', 21, 210400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (924, 210501, 'CARACHE', 21, 210500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (925, 210502, 'CUICAS', 21, 210500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (926, 210503, 'LA CONCEPCIÓN', 21, 210500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (927, 210504, 'PANAMERICANA', 21, 210500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (928, 210505, 'SANTA CRUZ', 21, 210500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (929, 210601, 'ESCUQUE', 21, 210600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (930, 210602, 'LA UNIÓN', 21, 210600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (931, 210603, 'SABANA LIBRE', 21, 210600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (932, 210604, 'SANTA RITA', 21, 210600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (933, 210701, 'EL SOCORRO', 21, 210700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (934, 210702, 'ANTONIO JOSÉ DE SUCRE', 21, 210700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (935, 210703, 'LOS CAPRICHOS', 21, 210700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (936, 210801, 'CAMPO ELÍAS', 21, 210800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (937, 210802, 'ARNOLDO GABALDÓN', 21, 210800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (938, 210901, 'SANTA APOLONIA', 21, 210900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (939, 210902, 'EL PROGRESO', 21, 210900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (940, 210903, 'LA CEIBA', 21, 210900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (941, 210904, 'TRES DE FEBRERO', 21, 210900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (942, 211001, 'EL DIVIDIVE', 21, 211000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (943, 211002, 'AGUA SANTA', 21, 211000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (944, 211003, 'AGUA CALIENTE', 21, 211000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (945, 211004, 'EL CENIZO', 21, 211000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (946, 211005, 'VALERITA', 21, 211000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (947, 211101, 'MONTE CARMELO', 21, 211100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (948, 211102, 'BUENA VISTA', 21, 211100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (949, 211103, 'SANTA MARÍA DEL HORCÓN', 21, 211100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (950, 211201, 'MOTATÁN', 21, 211200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (951, 211202, 'EL BAÑO', 21, 211200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (952, 211203, 'JALISCO', 21, 211200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (953, 211301, 'PAMPÁN', 21, 211300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (954, 211302, 'FLOR DE PATRIA', 21, 211300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (955, 211303, 'LA PAZ', 21, 211300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (956, 211304, 'SANTA ANA', 21, 211300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (957, 211401, 'PAMPANITO', 21, 211400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (958, 211402, 'LA CONCEPCIÓN', 21, 211400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (959, 211403, 'PAMPANITO II', 21, 211400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (960, 211501, 'BETIJOQUE', 21, 211500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (961, 211502, 'LA PUEBLITA', 21, 211500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (962, 211503, 'LOS CEDROS', 21, 211500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (963, 211504, 'JOSÉ GREGORIO HERNÁNDEZ', 21, 211500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (964, 211601, 'CARVAJAL', 21, 211600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (965, 211602, 'ANTONIO NICOLÁS BRICEÑO', 21, 211600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (966, 211603, 'CAMPO ALEGRE', 21, 211600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (967, 211604, 'JOSÉ LEONARDO SUÁREZ', 21, 211600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (968, 211701, 'SABANA DE MENDOZA', 21, 211700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (969, 211702, 'EL PARAÍ-SO', 21, 211700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (970, 211703, 'JUNÍN', 21, 211700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (971, 211704, 'VALMORE RODRÍGUEZ', 21, 211700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (972, 211801, 'ANDRÉS LINARES', 21, 211800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (973, 211802, 'CHIQUINQUIRÁ', 21, 211800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (974, 211803, 'CRISTÓBAL MENDOZA', 21, 211800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (975, 211804, 'CRUZ CARRILLO', 21, 211800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (976, 211805, 'MATRIZ', 21, 211800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (977, 211806, 'MONSEÑOR CARRILLO', 21, 211800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (978, 211807, 'TRES ESQUINAS', 21, 211800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (979, 211901, 'LA QUEBRADA', 21, 211900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (980, 211902, 'CABIMBÚ', 21, 211900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (981, 211903, 'JAJÍ', 21, 211900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (982, 211904, 'LA MESA', 21, 211900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (983, 211905, 'SANTIAGO', 21, 211900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (984, 211906, 'TUÑAME', 21, 211900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (985, 212001, 'JUAN IGNACIO MONTILLA', 21, 212000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (986, 212002, 'LA BEATRIZ', 21, 212000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (987, 212003, 'MERCEDES DÍAZ', 21, 212000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (988, 212004, 'SAN LUIS', 21, 212000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (989, 212005, 'LA PUERTA', 21, 212000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (990, 212006, 'MENDOZA', 21, 212000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (991, 220101, 'CM SAN PABLO', 22, 220100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (992, 220201, 'CM AROA', 22, 220200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (993, 220301, 'CAPITAL BRUZUAL', 22, 220300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (994, 220302, 'CAMPO ELÍAS', 22, 220300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (995, 220401, 'CM COCOROTE', 22, 220400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (996, 220501, 'CM INDEPENDENCIA', 22, 220500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (997, 220601, 'CM SABANA DE PARRA', 22, 220600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (998, 220701, 'CM BORAURE', 22, 220700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (999, 220801, 'CM YUMARE', 22, 220800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1000, 220901, 'CAPITAL NIRGUA', 22, 220900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1001, 220902, 'SALOM', 22, 220900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1002, 220903, 'TEMERLA', 22, 220900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1003, 221001, 'CAPITAL PEÑA', 22, 221000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1004, 221002, 'SAN ANDRÉS', 22, 221000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1005, 221101, 'CAPITAL SAN FELIPE', 22, 221100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1006, 221102, 'ALBARICO', 22, 221100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1007, 221103, 'SAN JAVIER', 22, 221100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1008, 221201, 'CM GUAMA', 22, 221200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1009, 221301, 'CM URACHICHE', 22, 221300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1010, 221401, 'CAPITAL VEROES', 22, 221400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1011, 221402, 'EL GUAYABO', 22, 221400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1012, 230101, 'ISLA DE TOAS', 23, 230100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1013, 230102, 'MONAGAS', 23, 230100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1014, 230201, 'GENERAL URDANETA', 23, 230200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1015, 230202, 'LIBERTADOR', 23, 230200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1016, 230203, 'MANUEL GUANIPA MATOS', 23, 230200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1017, 230204, 'MARCELINO BRICEÑO', 23, 230200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1018, 230205, 'PUEBLO NUEVO', 23, 230200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1019, 230206, 'SAN TIMOTEO', 23, 230200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1020, 230301, 'AMBROSIO', 23, 230300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1021, 230302, 'ARÍSTIDES CALVANI', 23, 230300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1022, 230303, 'CARMEN HERRERA', 23, 230300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1023, 230304, 'GERMAN RÍOS LINARES', 23, 230300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1024, 230305, 'JORGE HERNÁNDEZ', 23, 230300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1025, 230306, 'LA ROSA', 23, 230300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1026, 230307, 'PUNTA GORDA', 23, 230300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1027, 230308, 'RÓMULO BETANCOURT', 23, 230300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1028, 230309, 'SAN BENITO', 23, 230300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1029, 230401, 'ENCONTRADOS', 23, 230400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1030, 230402, 'UDON PÉREZ', 23, 230400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1031, 230501, 'MORALITO', 23, 230500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1032, 230502, 'SAN CARLOS DE ZULIA', 23, 230500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1033, 230503, 'SANTA BÁRBARA', 23, 230500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1034, 230504, 'SANTA CRUZ DEL ZULIA', 23, 230500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1035, 230505, 'URIBARRI', 23, 230500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1036, 230601, 'CARLOS QUEVEDO', 23, 230600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1037, 230602, 'FRANCISCO JAVIER PULGAR', 23, 230600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1038, 230603, 'SIMÓN RODRÍGUEZ', 23, 230600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1039, 230701, 'JOSÉ RAMÓN YÉPEZ', 23, 230700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1041, 230703, 'MARIANO PARRA LEÓN', 23, 230700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1042, 230704, 'SAN JOSÉ', 23, 230700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1043, 230801, 'BARI', 23, 230800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1044, 230802, 'JESÚS MARÍA SEMPRÚN', 23, 230800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1045, 230901, 'ANDRÉS BELLO', 23, 230900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1046, 230902, 'CHIQUINQUIRÁ', 23, 230900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1047, 230903, 'CONCEPCIÓN', 23, 230900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1048, 230904, 'EL CARMELO', 23, 230900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1049, 230905, 'POTRERITOS', 23, 230900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1050, 231001, 'ALONSO DE OJEDA', 23, 231000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1051, 231002, 'CAMPO LARA', 23, 231000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1052, 231003, 'ELEAZAR LÓPEZ CONTRERAS', 23, 231000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1053, 231004, 'LIBERTAD', 23, 231000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1054, 231005, 'VENEZUELA', 23, 231000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1055, 231101, 'LIBERTAD', 23, 231100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1056, 231102, 'RÍO NEGRO', 23, 231100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1057, 231103, 'SAN JOSÉ DE PERIJÁ', 23, 231100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1058, 231104, 'BARTOLOMÉ DE LAS CASAS', 23, 231100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1059, 231201, 'LA SIERRITA', 23, 231200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1060, 231202, 'LAS PARCELAS', 23, 231200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1061, 231203, 'LUIS DE VICENTE', 23, 231200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1062, 231204, 'MONSEÑOR MARCOS SERGIO G', 23, 231200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1063, 231205, 'RICAURTE', 23, 231200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1064, 231206, 'SAN RAFAEL', 23, 231200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1065, 231207, 'TAMARE', 23, 231200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1066, 231301, 'ANTONIO BORJAS ROMERO', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1067, 231302, 'BOLÍVAR', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1068, 231303, 'CACIQUE MARA', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1069, 231304, 'CECILIO ACOSTA', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1070, 231305, 'CHIQUINQUIRÁ', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1071, 231306, 'COQUIVACOA', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1072, 231307, 'CRISTO DE ARANZA', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1073, 231308, 'FRANCISCO EUGENIO B', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1074, 231309, 'IDELFONZO VÁSQUEZ', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1075, 231310, 'JUANA DE ÁVILA', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1076, 231311, 'LUIS HURTADO HIGUERA', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1077, 231312, 'MANUEL DANIGNO', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1078, 231313, 'OLEGARIO VILLALOBOS', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1079, 231314, 'RAÚL LEONI', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1080, 231315, 'SAN ISIDRO', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1081, 231316, 'SANTA LUCÍA', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1082, 231317, 'VENANCIO PULGAR', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1083, 231318, 'CARACCIOLO PARRA PÉREZ', 23, 231300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1084, 231405, 'SAN JOSÉ', 23, 231400, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1085, 231501, 'SINAMAICA', 23, 231500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1086, 231502, 'ALTA GUAJIRA', 23, 231500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1087, 231503, 'ELÍAS SÁNCHEZ RUBIO', 23, 231500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1088, 231504, 'GUAJIRA', 23, 231500, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1089, 231601, 'EL ROSARIO', 23, 231600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1090, 231602, 'DONALDO GARCÍA', 23, 231600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1091, 231603, 'SIXTO ZAMBRANO', 23, 231600, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1092, 231701, 'SAN FRANCISCO', 23, 231700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1093, 231702, 'EL BAJO', 23, 231700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1094, 231703, 'DOMITILA FLORES', 23, 231700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1095, 231704, 'FRANCISCO OCHOA', 23, 231700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1096, 231705, 'LOS CORTIJOS', 23, 231700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1097, 231706, 'MARCIAL HERNÁNDEZ', 23, 231700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1098, 231707, 'JOSÉ DOMINGO RUS', 23, 231700, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1099, 231801, 'SANTA RITA', 23, 231800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1100, 231802, 'EL MENE', 23, 231800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1101, 231803, 'JOSÉ CENOVIO URRIBARRI', 23, 231800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1102, 231804, 'PEDRO LUCAS URRIBARRI', 23, 231800, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1103, 231901, 'MANUEL MANRIQUE', 23, 231900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1104, 231902, 'RAFAEL MARÍA BARALT', 23, 231900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1105, 231903, 'RAFAEL URDANETA', 23, 231900, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1106, 232001, 'BOBURES', 23, 232000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1107, 232002, 'EL BATEY', 23, 232000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1108, 232003, 'GIBRALTAR', 23, 232000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1109, 232004, 'HERAS', 23, 232000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1110, 232005, 'MONSEÑOR ARTURO CELESTINO ÁLVAREZ', 23, 232000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1111, 232006, 'RÓMULO GALLEGOS', 23, 232000, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1112, 232101, 'LA VICTORIA', 23, 232100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1113, 232102, 'RAFAEL URDANETA', 23, 232100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1114, 232103, 'RAÚL CUENCA', 23, 232100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1115, 240101, 'CARABALLEDA', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1116, 240102, 'CARAYACA', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1117, 240103, 'CARUAO', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1118, 240104, 'CATIA LA MAR', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1119, 240105, 'EL JUNKO', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1120, 240106, 'LA GUAIRA', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1121, 240107, 'MACUTO', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1122, 240108, 'MAIQUETÍA', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1123, 240109, 'NAIGUATÁ', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1124, 240110, 'URIMARE', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1125, 240111, 'CARLOS SOUBLETTE', 24, 240100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1126, 250101, 'LOS ROQUES', 25, 250100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1127, 250201, 'LAS AVES', 25, 250200, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1128, 250301, 'LA ORCHILA', 25, 250300, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1129, 260101, 'OTRAS DEPENDENCIAS FEDERALES (P)', 26, 260100, 230, true);
INSERT INTO top_parroquias (id, codigo, descripcion, estado_id, municipio_id, pais_id, activo) VALUES (1130, 270101, 'REGISTRO DE NORMALIZACIÓN (PARROQUIA)', 27, 270100, 230, true);


--
-- TOC entry 2169 (class 0 OID 0)
-- Dependencies: 198
-- Name: top_parroquias_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('top_parroquias_id_seq', 1, false);


--
-- TOC entry 2134 (class 0 OID 1151794)
-- Dependencies: 199
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (2, 1, 'admin', 'pbkdf2_sha256$12000$8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 12345678, 'ADMIN', 'ADMIN', 'admin@gmail.com', '1', NULL, '04161073358', true, NULL, NULL, NULL, NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (4, 4, 'ana', 'pbkdf2_sha256$12000$a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', NULL, NULL, NULL, NULL, '3', NULL, NULL, true, NULL, '2016-11-04 18:21:28-04', '2016-11-04 18:21:28-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (5, 5, 'jlaya', 'pbkdf2_sha256$12000$a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', NULL, NULL, NULL, NULL, '3', NULL, NULL, true, NULL, '2016-11-04 18:26:41-04', '2016-11-04 18:26:41-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (6, 6, 'operador', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 80555454, 'OPERADOR', 'OPERADOR', 'OPERADOR@GMAIL.COM', '2', NULL, '04143255888', true, '', '2016-11-05 11:20:36-04', '2016-11-05 11:20:36-04', 2);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (7, 7, 'operador2', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 80264564, 'OPERADOR2', 'OPERADOR2', 'OPERADOR@GMAIL.COM', '2', NULL, '04143255888', true, '', '2016-11-05 14:35:36-04', '2016-11-05 14:35:36-04', 2);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (8, 8, 'operador3', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 81541564, 'OPERADOR3', 'OPERADOR3', 'OPERADOR@GMAIL.COM', '2', NULL, '04143255888', true, '', '2016-11-05 14:47:25-04', '2016-11-05 14:47:25-04', 2);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (10, 10, 'ralvarado', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 19124123, 'Ruben', 'Alvarado', 'ruben@gmai', '3', NULL, '(0212) 484-6864', true, NULL, '2016-11-08 08:10:28-04', '2016-11-08 08:10:28-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (11, 11, 'ibenitez', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 19144489, 'Isel', 'Benitez', 'isel@gmail', '3', NULL, '(0244) 233-5558', true, NULL, '2016-11-08 08:52:59-04', '2016-11-08 08:52:59-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (9, 9, 'marcuri', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 19363480, 'MARCEL ROBERT', 'ARCURI GOMEZ', 'marcel@bva.org.ve', '3', NULL, '(0242) 384-3848', true, NULL, '2016-11-07 15:28:03-04', '2016-11-07 15:28:03-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (3, 3, 'jsolorzano', 'pbkdf2_sha256$12000$a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 18993867, 'JOSE ', 'SOLORZANO', 'jose@bvaorg.ve', '3', NULL, '(0243) 484-3844', true, NULL, '2016-11-04 16:04:37-04', '2016-11-04 16:04:37-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (12, 12, 'cmeza', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 19404568, 'Carlos', 'Meza', 'carlos@gmail.com', '3', NULL, '(0234) 684-9898', true, NULL, '2016-11-09 11:32:57-04', '2016-11-09 11:32:57-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (13, 13, 'lcalderon', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 18456684, 'Luis', 'Calderon', 'lcalderon@hotmail.com', '3', NULL, '(0234) 486-4864', true, NULL, '2016-11-09 13:25:48-04', '2016-11-09 13:25:48-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (14, 14, 'ocartaya', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 18784848, 'OSCAR ', 'CARTAYA', 'ocartaya@hotmail.com', '3', NULL, '(0231) 313-4646', true, NULL, '2016-11-24 03:30:03-04', '2016-11-24 03:30:03-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (15, 15, 'rgarmendia', 'pbkdf2_sha256$12000$8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 17884984, 'RICARDO', 'GARMENDIA', 'rgarmendia@hotmail.com', '3', NULL, '(0234) 384-8968', true, NULL, '2016-11-24 06:55:51-04', '2016-11-24 06:55:51-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (16, 16, 'fmedina', 'pbkdf2_sha256$12000$a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', NULL, NULL, NULL, NULL, '3', NULL, NULL, true, NULL, '2017-06-07 19:43:29-04', '2017-06-07 19:43:29-04', NULL);
INSERT INTO usuarios (id, codigo, username, password, cedula, first_name, last_name, email, tipo_usuario, cargo, telefono, estatus, picture, fecha_create, fecha_update, user_create_id) VALUES (17, NULL, NULL, 'pbkdf2_sha256$12000$e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2017-11-25 21:28:51-04', '2017-11-25 21:28:51-04', 2);


--
-- TOC entry 2170 (class 0 OID 0)
-- Dependencies: 200
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('usuarios_id_seq', 16, true);


--
-- TOC entry 1982 (class 2606 OID 1151819)
-- Name: pk_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY rel_candidato_eleccion
    ADD CONSTRAINT pk_id PRIMARY KEY (id);


--
-- TOC entry 1984 (class 2606 OID 1151821)
-- Name: pk_id_candidato; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY reg_candidato
    ADD CONSTRAINT pk_id_candidato PRIMARY KEY (id);


--
-- TOC entry 1986 (class 2606 OID 1151823)
-- Name: pk_id_eleccion; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY reg_eleccion
    ADD CONSTRAINT pk_id_eleccion PRIMARY KEY (id);


--
-- TOC entry 1988 (class 2606 OID 1151825)
-- Name: rep_resultados_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY rep_resultados
    ADD CONSTRAINT rep_resultados_key PRIMARY KEY (id);


--
-- TOC entry 1990 (class 2606 OID 1151827)
-- Name: top_ciudades_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY top_ciudades
    ADD CONSTRAINT top_ciudades_key PRIMARY KEY (id);


--
-- TOC entry 1992 (class 2606 OID 1151829)
-- Name: top_estados_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY top_estados
    ADD CONSTRAINT top_estados_key PRIMARY KEY (id);


--
-- TOC entry 1994 (class 2606 OID 1151831)
-- Name: top_municipios_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY top_municipios
    ADD CONSTRAINT top_municipios_key PRIMARY KEY (id);


--
-- TOC entry 1996 (class 2606 OID 1151833)
-- Name: top_paises_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY top_paises
    ADD CONSTRAINT top_paises_key PRIMARY KEY (id);


--
-- TOC entry 1998 (class 2606 OID 1151835)
-- Name: top_parroquias_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY top_parroquias
    ADD CONSTRAINT top_parroquias_key PRIMARY KEY (id);


-- Completed on 2017-11-27 23:21:57 -04

--
-- PostgreSQL database dump complete
--

